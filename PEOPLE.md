# "Cloudflare, we have a problem"

![](image/people.jpg)
![](image/cfblockbothuman.jpg)

```
It's pretty sad when even the hometown paper is behind the greatcloudwall.
C'mon guys. Stop using Cloudflare.
It's endangering the world wide web and you're blocking readers who are protecting
their privacy by using Tor.
```
-- [Mr. Jeff Cliff, BSc](https://shitposter.club/users/jeffcliff)


```
Man-in-the-middle attack is a serious offense.
If you′re using Cloudflare on your website, you won′t get first rank.
You shouldn′t use it if you value visitor′s privacy.
```
 -- [Searxes](https://searxes.eu.org/)


```
There have been many cases of cloudflare denying people access to a website
without the website owner knowing.
I'm a legitimate user increasing my security by using a VPN but Cloudflare mistakenly
decides I'm a bot and denies access.
There have also been cases where Cloudflare just stops allowing anyone to visit a website
simply because they don't think anyone should be able to.

Regardless, that's not my main objection to Cloudflare.
As a "man in the middle", they get access to all of my traffic.
With the SSL certificates they provide, your connection is encrypted to Cloudflare then
decrypted and sent to the server. The lock icon in your browser means absolutely nothing
when a third party is literally getting *all* of your traffic when you connect to that site.
Beyond that, with so many sites using Cloudflare, cross-site tracking becomes a problem.

In addition, with so many websites behind Cloudflare, when it goes down, half the web is down.
The internet was designed to be decentralised and Cloudflare is centralising it.
Half of the internet relying on a single entity is dangerous.
```
-- [Amolith, Ex contributor](https://masto.nixnet.xyz/@Amolith)


```
Think about your visitors.
Do you like it when websites ruin a bit of your experience?
In other words: Would you love to visit a website which forces you
to solve a annoying timewasting puzzle usually after waiting five
seconds which also cost you valuable time?
Awesome! Use Cloudflare!
```
-- [Robin Wils](https://linuxrocks.online/@RMW)


---

![](image/butitsdown.jpg)
![](image/cloudflareinternalerror.jpg)

---

<details>
<summary>_click me_

## News
</summary>


"[Internet wobble caused by Cloudflare glitch](https://www.bbcnewsv2vjtpsuy.onion/news/technology-48841815)" by [BBC](https://www.bbcnewsv2vjtpsuy.onion/)

"[CloudflareのDNSだと「5ちゃんねる」に接続できない？　ネットで話題に](https://www.itmedia.co.jp/news/articles/1908/09/news108.html)" by [itmedia](https://www.itmedia.co.jp/news/)

"[Cloudflare down: Thousands of popular websites affected by brief outage](https://news.sky.com/story/cloudflare-down-thousands-of-popular-websites-affected-by-brief-outage-11755312)" by [Alexander J Martin](https://news.sky.com/)

"[Major websites and services across the internet went down Tuesday because of a hosting-platform outage](https://www.businessinsider.nl/cloudflare-outage-causes-major-websites-across-internet-to-go-down-2019-7/)" by [Antonio Villas-Boas](https://www.businessinsider.com/author/antonio-villas-boas)

"[CloudFlare Outage Takes Down Coinbase, CoinMarketCap and Other Top Crypto Websites](https://www.coindesk.com/cloudflare-outage-takes-down-coinbase-coinmarketcap-and-other-top-crypto-websites)" by [John Biggs](https://www.coindesk.com/author/johncoindesk-com)

"[Millions of websites went down across the internet today after massive Cloudflare outage](https://metro.co.uk/2019/07/02/cloudflare-outage-means-websites-including-detector-10103471/)" by [Jasper Hamill](https://metro.co.uk/author/jasper-hamill/)

"[Cloudflare blames ‘bad software’ deployment for today’s outage](https://techcrunch.com/2019/07/02/a-cloudflare-outage-is-impacting-sites-everywhere/)" by [Brian Heater](https://techcrunch.com/author/brian-heater/)

"[Cloudflare issues affecting numerous sites on Monday AM](https://techcrunch.com/2019/06/24/cloudflare-outage-affecting-numerous-sites-on-monday-am/)" by [Sarah Perez](https://techcrunch.com/author/sarah-perez/)

"[Internet security CEO explained why his company exposed people to harassment, and suggested they should've used fake names](https://web.archive.org/web/20171024040313/http://www.businessinsider.com/cloudflare-ceo-suggests-people-who-report-online-abuse-use-fake-names-2017-5)" by [Julie Bort](http://www.businessinsider.com/author/julie-bort)

"[Cloudflare CEO Terminates Neo-Nazi Site After 'Waking Up in a Bad Mood'](https://www.zerohedge.com/news/2017-08-17/cloudflare-ceo-terminates-neo-nazi-site-after-waking-bad-mood)" by [The_Real_Fly](https://www.zerohedge.com/users/therealfly)

"[Cloudflare CEO on Terminating Service to Neo-Nazi Site: 'The Daily Stormer Are Assholes'](https://gizmodo.com/cloudflare-ceo-on-terminating-service-to-neo-nazi-site-1797915295)" by [Kate Conger](https://kinja.com/conger)

"[The invasion boards that set out to ruin lives](https://boingboing.net/2015/01/19/invasion-boards-set-out-to-rui.html)" by [Jay Allen](https://boingboing.net/author/amaninblack)

"[How One Major Internet Company Helps Serve Up Hate on the Web](https://web.archive.org/web/20190512152916/https://www.propublica.org/article/how-cloudflare-helps-serve-up-hate-on-the-web)" by [Ken Schwencke](https://web.archive.org/web/20190512152916/https://www.propublica.org/people/ken-schwencke)

"[Zaradi hrošča iz Cloudflara več mesecev curljalo](https://slo-tech.com/novice/t694836/p5445996)" by [Slo-Tech](https://slo-tech.com/)

"[CDN企業Cloudflareのバグで、多数のサービスで機密データ流出の可能性](https://www.itmedia.co.jp/news/articles/1702/25/news024.html)" by [佐藤由紀子](https://www.itmedia.co.jp/news/)

"[Everything You Need to Know About Cloudbleed, the Latest Internet Security Disaster](https://gizmodo.com/everything-you-need-to-know-about-cloudbleed-the-lates-1792710616)" by [Adam Clark Estes](https://kinja.com/ace)

"[Why Cloudflare Let an Extremist Stronghold Burn](https://www.wired.com/story/free-speech-issue-cloudflare/)" by [Steven Johnson](https://www.wired.com/author/steven-johnson/)

"[Private crypto keys are accessible to Heartbleed hackers, new data shows](https://arstechnica.com/information-technology/2014/04/private-crypto-keys-are-accessible-to-heartbleed-hackers-new-data-shows/)" by [Megan Geuss](https://arstechnica.com/author/megan-geuss/)

</details>

------

<details>
<summary>_click me_

## Blog
</summary>


(Just don't add Medium.com articles. It's Cloudflared. Whether the content is good or not doesn't matter.)

"[Cloudflare silently deleted my DNS records](https://web.archive.org/save/https://txti.es/cloudflare-deleted-my-dns)" by Cloudflare user

"[Cloudflare is turning off the internet for me](https://blog.dijit.sh/cloudflare-is-turning-off-the-internet-for-me)" by [Jan Harasym](https://blog.dijit.sh/)

"[Can you trust CloudFlare with your personal data?](https://shkspr.mobi/blog/2019/11/can-you-trust-cloudflare-with-your-personal-data/)" by [Terence Eden](https://edent.tel/)

"[WARP is not a VPN for privacy](https://www.piavpnaymodqeuza.onion/blog/2019/09/warp-is-not-a-vpn-for-privacy/)" by [Private Internet Access](https://www.privateinternetaccess.com/)

"[Cloudflare - why the fuss?](https://decentralize.today/decentralization/cloudflare-why-the-fuss)" by [O S Layman](https://decentralize.today/cryptocurrency/blogger/big-h)

"[Cloudflare considered harmful](https://www.devever.net/~hl/cloudflare)"

"[Cloudflare’s WARP ‘VPN’ isn’t private nor safe; Don’t use it](https://blog.kareldonk.com/cloudflares-warp-vpn-isnt-private-nor-safe-dont-use-it/)" by [Karel Donk](https://blog.kareldonk.com/)

"[Does Cloudflare help my WordPress site?](https://seravo.com/blog/does-cloudflare-help-my-wordpress-site/)" by [Seravo](https://seravo.com/)

"[Say no to Cloudflare](https://robinwils.gitlab.io/articles/say-no-to-cloudflare/)" by [Robin Wils](https://robinwils.gitlab.io/contact/)

"[Why I Stopped Using CloudFlare](https://www.reviewhell.com/blog/cloudflare-makes-websites-slower/)" by [Review Hell](https://www.reviewhell.com/)

"[I don’t trust Cloudflare’s 1.1.1.1 App and Warp VPN](https://blog.kareldonk.com/i-dont-trust-cloudflares-1-1-1-1-app-and-warp-vpn/)" by [Karel Donk](https://blog.kareldonk.com/)

"[Don’t Use Cloudflare Because You Impose This on People Who Least Want It](http://techrights.org/2019/02/17/the-cloudflare-trap/)" by [Dr. Roy Schestowitz](http://techrights.org/)

"[All your DNS traffic will be sent to Cloudflare](https://ungleich.ch/en-us/cms/blog/2018/08/04/mozillas-new-dns-resolution-is-dangerous/)" by [ungleich](https://ungleich.ch/)

"[Cloudflare: The bad, the worse and the ugly?](http://webschauder.de/cloudflare-the-bad-the-worse-and-the-ugly/)" by [Alle Beiträge](http://webschauder.de/author/jw/)

"[I don’t trust Cloudflare with IPFS](https://blog.kareldonk.com/i-dont-trust-cloudflare-with-ipfs/)" by [Karel Donk](https://blog.kareldonk.com/)

"[Cloudflare IPFS experiment](https://js.ipfs.io/ipns/QmZJBQBXX98AuTcoR1HBGdbe5Gph74ZBWSgNemBcqPNv1W/cloudflare-IPFS-experiment.html)" by [Joe](https://js.ipfs.io/ipns/QmZJBQBXX98AuTcoR1HBGdbe5Gph74ZBWSgNemBcqPNv1W/index.html)  [[mirror](http://archive.fo/139z1)]

"[CloudFlare: Deutscher Bundestag bezieht schon wieder Internet von US-Anbietern, diesmal für die eigenen Webseiten](https://netzpolitik.org/2015/cloudflare-deutscher-bundestag-bezieht-schon-wieder-internet-von-us-anbietern-diesmal-fuer-die-eigenen-webseiten/)" by [Andre Meister](https://netzpolitik.org/author/andre/)

"[Don't Trust CloudFlare](https://write.lain.haus/thufie/dont-trust-cloudflare)" by [@lunaterra@cyberia.social](https://cyberia.social/@lunaterra)

"[CloudFlare slowed down our site](https://pergento.wordpress.com/2012/02/01/cloudflare-slowed-down-our-site/)" by [Callum](https://pergento.wordpress.com/author/chmac/)

"[Stay away from CloudFlare](http://www.unixsheikh.com/articles/stay-away-from-cloudflare.html)" by [Unix Sheikh](http://www.unixsheikh.com/)

"[Cloudflare and Spamhaus](https://wordtothewise.com/2012/07/cloudflare-and-spamhaus/)" by [laura](https://wordtothewise.com/author/laura/)

"[Support End-to-End Encryption on the Web](https://www.wordfence.com/blog/2017/03/support-end-to-end-encryption/)" by [Mark Maunder](https://www.wordfence.com/)

"[Cloudflare发布针对IPFS的Gateway](https://www.jianshu.com/p/8a9cb8065f4a)" by [幸运排骨虾](https://www.jianshu.com/u/c3c0e9748845)

"[CloudFlareよサヨナラ！ WordPressの表示速度が改善するプラグイン「Photon」](https://rentalhomepage.com/photon/)" by [ENJILOG](https://rentalhomepage.com/)

"[CloudFlareの解除と「Phonton」の導入方法](http://kyoheixxx.com/cloudflare-phonton-5058)" by [kyohei](http://kyoheixxx.com/)

"[The CloudFlare Leak and the Problem of Centralized Authentication](https://www.iovation.com/blog/the-cloudflare-leak-and-the-problem-of-centralized-authentication)" by [iovation Inc.](https://www.iovation.com/)

"[CloudFlare is ruining the internet (for me)](https://www.slashgeek.net/2016/05/17/cloudflare-is-ruining-the-internet-for-me/)" by [slashgeek](https://www.slashgeek.net/)

"[MITM-as-a-Service: The Threat Surface We Didn’t Know We Had](http://daveshackleford.com/?p=1134)" by [Shack](twitter.com/daveshackleford/)

"[Journal CloudFlare au milieu](https://linuxfr.org/users/thibg/journaux/cloudflare-au-milieu)" by [ThibG](https://linuxfr.org/)

"[why you shouldn’t use Cloudflare](https://tech.tiq.cc/2016/01/why-you-shouldnt-use-cloudflare/)" by [tiq](https://tech.tiq.cc/)

"[DNSサーバー「1.1.1.1」が利用できない障害発生中（2018年4月22日）](https://web.archive.org/web/20190815152042/https://did2memo.net/2018/04/22/dns-1-1-1-1-failure-2018-04-22/)" by [did2](https://twitter.com/did2memo)

"[The CloudFlare MITM](https://web.archive.org/web/20160311163431/https://blog.paymium.com/2014/02/19/the-cloudflare-mitm/)" by [David FRANCOIS](https://blog.paymium.com/)

"[Allergique à Cloudflare ? Voici comment vous soigner…](https://korben.info/cloudflare-mitm.html)" by [KORBEN](https://korben.info/)

"[CloudFlare, We Have A Problem](http://cryto.net/~joepie91/blog/2016/07/14/cloudflare-we-have-a-problem/)" by [joepie91](http://cryto.net/~joepie91/)

"[On Cloudflare](https://www.tyil.nl/post/2017/12/17/on-cloudflare/)" by [tyil](https://www.tyil.nl/)

"[Why CloudFlare Is Probably A Honeypot](https://cypherpunk.is/2015/04/02/why-cloudflare-is-probably-a-honeypot/)" by [cypherpunk](https://cypherpunk.is/)

"[iSucker: Big Brother Internet Culture](https://exiledonline.com/isucker-big-brother-internet-culture/)" by [The Exiled](https://exiledonline.com/)

"[The Trouble with CloudFlare](https://blog.torproject.org/trouble-cloudflare)" by [mikeperry](https://blog.torproject.org/users/mikeperry)

"[Growing Cloudflare Menace](http://imhhge4lijqv7jzf.onion/warning.html)"

</details>

------

<details>
<summary>_click me_

## Forum / Wiki
</summary>


"[As long as Gab uses Cloudflare, it's doomed to failure](https://www.reddit.com/r/gab/comments/eet4cr/as_long_as_gab_uses_cloudflare_its_doomed_to/)"

"[Mozilla just lost all its credibility. Cloudflare isn't trustworthy, since it decrypts TLS!](https://www.reddit.com/r/privacytoolsIO/comments/e97thq/mozilla_just_lost_all_its_credibility_cloudflare/)" by [vargasgetulio](https://www.reddit.com/user/vargasgetulio)

"[Cloudflare and the US Intelligence Community](https://www.reddit.com/r/privacy/comments/dmc4cj/cloudflare_and_the_us_intelligence_community/)" by [dhaavi](https://www.reddit.com/user/dhaavi)

"[Some websites not loading pictures after upgrade to Tor Browser 9.0](https://trac.torproject.org/projects/tor/ticket/32238)"

"[Some images from Cloudflare don't load up and a 403 Forbidden is returned](https://trac.torproject.org/projects/tor/ticket/32582)"

"[website not working properly since update](https://trac.torproject.org/projects/tor/ticket/32430)"

"[Cloudflare to MITM your traffic is an extraordinary security and privacy problem.](https://www.reddit.com/r/Bitcoin/comments/e303gh/bitcoincoreorg_expired_ssl_cert/f8zqcf9/)" by [hva32](https://www.reddit.com/user/hva32/)

"[Google's reCAPTCHA fails 100%](https://trac.torproject.org/projects/tor/ticket/23840)"

"[The Great Cloudwall](https://lobste.rs/s/xkwcl3)" by [caioalonso](https://lobste.rs/u/caioalonso)

"[Proposal: Remove Cloudflare from the official Bitcoin Cash website](https://www.reddit.com/r/btc/comments/docdui/proposal_remove_cloudflare_from_the_official/)" by [LeoBeltran](https://www.reddit.com/user/LeoBeltran/)

"[Now that Cloudflare Warp has gone public, is it still the worst thing EVER?](https://www.reddit.com/r/privacytoolsIO/comments/da1lx3/now_that_cloudflare_warp_has_gone_public_is_it/)" by [Anaranovski](https://www.reddit.com/user/Anaranovski)

"[Cloudeflare Captcha](https://forum.palemoon.org/viewtopic.php?f=37&t=22321&p=169114)" by [zdmv09rzbtklezd8d](https://forum.palemoon.org/memberlist.php?mode=viewprofile&u=19075)

"[Secure Connection Failed](https://forum.palemoon.org/viewtopic.php?f=44&t=20845&p=155973)" by [Tomaso](https://forum.palemoon.org/memberlist.php?mode=viewprofile&u=9778)

"[That's not how the web works, sorry. Cheers](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=941394)"

"[Pale Moon developer shows disdain for Tor and people who combat Cloudflare MITM](https://www.reddit.com/r/privacy/comments/cnqwr3/pale_moon_developer_shows_disdain_for_tor_and/)" by [vargasgetulio](https://www.reddit.com/user/vargasgetulio/)

"[...Cloudflare DOES TLS termination, directly having access to all the data in clear.](https://www.reddit.com/r/privacy/comments/ckwlem/cloudflare_does_tls_termination_directly_having/)" by [JustCondition4](https://www.reddit.com/user/JustCondition4/)

"[What makes CloudFlare bad?](https://www.reddit.com/r/privacy/comments/cki0s5/what_makes_cloudflare_bad/)" by [Spadey0](https://www.reddit.com/user/Spadey0/)

"[Cloudflare](https://mlpol.net/mlpol/res/241062.html)" by Anonymous

"[The Great Cloudwall](https://sebsauvage.net/links/?beq2fA)" by [sebsauvage](https://framapiaf.org/@sebsauvage)

"[DNS over HTTPS](http://oxwugzccvk3dk6tj.onion/tech/res/1082196.html)"

"[Cloudflare = CIA](http://oxwugzccvk3dk6tj.onion/tech/res/1032001.html)"

"[Delete Account](https://www.projecthoneypot.org/board/read.php?f=4&i=722&t=722)"

"[Please remove cloudflare](https://www.reddit.com/r/privacytoolsIO/comments/35xg9u/please_remove_cloudflare/)" by [cloudspyha](https://www.reddit.com/user/cloudspyha/)

"[coinkite SSL is shared with shady sites?](https://www.reddit.com/r/Bitcoin/comments/1ynfvt/coinkite_ssl_is_shared_with_shady_sites/)" by [notR1CH](https://www.reddit.com/user/notR1CH/)

"[Cloudflare's mobile application "1.1.1.1: Faster & Safer Internet" keeps a log of your DNS queries for 24 hours, can't be disabled](https://www.reddit.com/r/privacy/comments/9wmjey/cloudflares_mobile_application_1111_faster_safer/)" by [maybenot12](https://www.reddit.com/user/maybenot12/)

"[In other news, Bitpay is completely broken now.](https://www.reddit.com/r/Bitcoin/comments/7n5ofh/in_other_news_bitpay_is_completely_broken_now/)" by [phelix2](https://www.reddit.com/user/phelix2/)

"[Can we stop posting articles from medium?](https://www.reddit.com/r/privacy/comments/bsip31/can_we_stop_posting_articles_from_medium/)" by [FusionTorpedo](https://www.reddit.com/user/FusionTorpedo)

"[How to avoid plain text passwords etc being visible to Cloudflare's MITM proxies](https://www.reddit.com/r/webdev/comments/5ap1le/how_to_avoid_plain_text_passwords_etc_being/)" by [r0ck0](https://www.reddit.com/user/r0ck0/)

"[Thoughts on CloudFlare](https://www.reddit.com/r/privacytoolsIO/comments/cailfa/thoughts_on_cloudflare/)" by [techEnthusiast0](https://www.reddit.com/user/techEnthusiast0/)

"[Is it possible to universally bypass the Cloudflare DDOS-protection? Can add a delay to your daily browsing when automatically clearing cookies](https://www.reddit.com/r/firefox/comments/c7ywt4/is_it_possible_to_universally_bypass_the/)" by [8VBQ-Y5AG-8XU9-567UM](https://www.reddit.com/user/8VBQ-Y5AG-8XU9-567UM)

"[Este Cloudflare atac MITM?](https://forum.softpedia.com/topic/1146988-este-cloudflare-atac-mitm/)" by gladioc

"[How to get rid of CloudFlare](http://answerszuvs3gg2l64e6hmnryudl5zgrmwm3vh65hzszdghblddvfiqd.onion/76323/how-to-get-rid-of-cloudflare)"

"[Cloudflare outage caused by deploying bad regular expression that caused 100% CPU usage worldwide, dropping up to 82% of traffic](https://www.reddit.com/r/sysadmin/comments/c8eymj/cloudflare_outage_caused_by_deploying_bad_regular/)" by [TyroPyro](https://www.reddit.com/user/TyroPyro/)

"[Cloudflare outage AGAIN MAH INTERNETS! Nooo!](https://www.reddit.com/r/sysadmin/comments/c89fa4/cloudflare_outage_again/)" by [Lewis_Browne](https://www.reddit.com/user/Lewis_Browne/)

"[Dear customers of Cloudflare: an appeal regarding Tor](https://news.ycombinator.com/item?id=17750801)" by [IngoBlechschmid](https://news.ycombinator.com/user?id=IngoBlechschmid)

"[Cloudflare CDN suffers route leak; services like Discord and Crunchyroll experience outages all over the world](https://www.reddit.com/r/worldnews/comments/c4n42g/cloudflare_cdn_suffers_route_leak_services_like/)" by [DragonSkyMusic](https://www.reddit.com/user/DragonSkyMusic/)

"[Cloudflare Is Not An Option!](https://www.reddit.com/r/privacy/comments/bhag8s/cloudflare_is_not_an_option/)" by [FVz7Ftt83m](https://www.reddit.com/user/FVz7Ftt83m)

"[All Captchas seem to be broken for me.](https://www.reddit.com/r/CloudFlare/comments/7cqb60/all_captchas_seem_to_be_broken_for_me/)" by [da_sechzga](https://www.reddit.com/user/da_sechzga/)

"[Fastest DNS from Cloudflare + privacy first? Hmmm](https://www.reddit.com/r/privacy/comments/88qqjf/fastest_dns_from_cloudflare_privacy_first_hmmm/)" by [deleted](https://www.reddit.com/r/privacy/comments/88qqjf/fastest_dns_from_cloudflare_privacy_first_hmmm/)

"[Tor says CloudFlare's claim that 94% of requests from Tor are malicious is likely based on flawed methodology, asks for explanation](https://www.reddit.com/r/autotldr/comments/4d21p5/tor_says_cloudflares_claim_that_94_of_requests/)" by [autotldr](https://www.reddit.com/user/autotldr/)

"[Is there a safe download mirror for Monero wallets that doesn't force me to go through this hateful and broken CloudFlare?](https://www.reddit.com/r/TOR/comments/30980u/cant_get_passed_cloudflares_captcha_with/)" by [VedadoAnonimato](https://www.reddit.com/user/VedadoAnonimato/)

"[Can't get passed CloudFlare's CAPTCHA with TorBrowser.](https://www.reddit.com/r/TOR/comments/30980u/cant_get_passed_cloudflares_captcha_with/)" by [VedadoAnonimato](https://www.reddit.com/user/VedadoAnonimato/)

"[Anyone else getting this error - THIS REQUEST HAS BEEN RATE LIMITED](https://www.reddit.com/r/CoinBase/comments/7k8hug/anyone_else_getting_this_error_this_request_has/)" by [jessicadunbar](https://www.reddit.com/user/jessicadunbar/)

"[Cloudflare blocked "my" IP address.](https://www.reddit.com/r/discordapp/comments/bq8977/cloudflare_blocked_my_ip_address_i_use_pia_vpn/)" by [Stillhart](https://www.reddit.com/user/Stillhart/)

"[CloudFlare blocks reddit API requests from Tor](https://www.reddit.com/r/bugs/comments/4wre66/cloudflare_blocks_reddit_api_requests_from_tor/)" by [QuantumBadger](https://www.reddit.com/user/QuantumBadger/)

"[Any way to fix 523 cloudflare error websites (websites that dead with 523 cloudlare error)?](https://www.reddit.com/r/techsupport/comments/byb2g1/any_way_to_fix_523_cloudflare_error_websites/)" by [fasdaa222](https://www.reddit.com/user/fasdaa222/)

"[Ongoing centralization of the internet through CDNs](https://www.reddit.com/r/C_S_T/comments/3gc1ne/ongoing_centralization_of_the_internet_through/)" by [quickdecision](https://www.reddit.com/user/quickdecision/)

"[Padlock icon appears on MitMd Firefox connections](https://redmine.tails.boum.org/code/issues/11629)" by [cypherpunks](https://redmine.tails.boum.org/code/users/808)

"[I'm not sure if I can ELI5 this but I'll try my best to explain.](https://www.reddit.com/r/india/comments/bg3lxh/was_casually_checking_my_logs_this_is_what_is/elk77k3/)" by [Waphire](https://www.reddit.com/user/Waphire)

"[Anonym im Internet - Inhaltsverzeichnis](https://wiki.kairaven.de/open/anon/netzwerk/anet)" by [Kairaven](https://hp.kairaven.de)

"[Cloudflare is MITM](https://forum.golem.de/kommentare/security/ddos-schutz-cloudflare-gewinnt-kampf-um-national-security-letter/cloudflare-is-mitm/106383,4705549,4705549,read.html)" by Nutzer

"[Constant "CloudFlare" IP BLOCK Popups](https://web.archive.org/web/20190119114739/https://forums.malwarebytes.com/topic/209059-constant-cloudflare-ip-block-popups/)" by [craigs](https://forums.malwarebytes.com/profile/39498-craigs/)

"[This Cloudflare horsesh*t. Are they CIA? Seriously, I keep finding cloudflare based problems. No way they are this popular with all these problems.](https://www.reddit.com/r/conspiracy/comments/3cuqgg/this_cloudflare_horsesht_are_they_cia_seriously_i/)" by deleted

"[Issues with corporate censorship and mass surveillance](https://www.torproject.org/projects/tor/ticket/18361)" by [Jacob Appelbaum](https://twitter.com/ioerror)

[How can i get rid of those cloudflare captchas its annoying](https://tor.stackexchange.com/questions/13424/how-can-i-get-rid-of-those-cloudflare-captchas-its-annoying)

[Cloudflares captcha screen insurmountable](https://tor.stackexchange.com/questions/599/cloudflares-captcha-screen-insurmountable)

[Does cloudflare identify tor users actual ip address and other identifying info](https://tor.stackexchange.com/questions/4441/does-cloudflare-identify-tor-users-actual-ip-address-and-other-identifying-info)

[Cloudflare hates tbb v7 not duplicate](https://tor.stackexchange.com/questions/14955/cloudflare-hates-tbb-v7-not-duplicate)

[Cloudflare wont let in even though i solve the captcha correctly](https://tor.stackexchange.com/questions/6045/cloudflare-wont-let-in-even-though-i-solve-the-captcha-correctly)

[Getting cloudflared all of a sudden why](https://tor.stackexchange.com/questions/9330/getting-cloudflared-all-of-a-sudden-why)

[Is a tor or cloudflare update the reason i suddenly getting cloudflared to death](https://tor.stackexchange.com/questions/9465/is-a-tor-or-cloudflare-update-the-reason-i-suddenly-getting-cloudflared-to-death)

[How do enter all cloudflares captchas on one page to access a complex page](https://tor.stackexchange.com/questions/6277/how-do-enter-all-cloudflares-captchas-on-one-page-to-access-a-complex-page)

"[Be careful with CloudFlare](https://www.reddit.com/r/privacy/comments/41cb4k/be_careful_with_cloudflare/)" by [no-idea-for-username](https://www.reddit.com/user/no-idea-for-username)

"[Be wary reporting to Cloudflare](https://www.reddit.com/r/GamerGhazi/comments/2s64fe/be_wary_reporting_to_cloudflare/)" by [athenahollow](https://www.reddit.com/user/athenahollow/)

"[Waterfox needs this(DNS over HTTPS)](https://www.reddit.com/r/waterfox/comments/8p4x1q/waterfox_needs_thisdns_over_https/)" by [0o-0-o0](https://www.reddit.com/user/0o-0-o0)

"[Cloudflare rant](https://www.reddit.com/r/privacy/comments/8ixnfa/cloudflare_rant/)" by [86rd9t7ofy8pguh](https://www.reddit.com/user/86rd9t7ofy8pguh/)

"["Fuck CloudFlare" stickers](https://www.reddit.com/r/TOR/comments/46kn1d/fuck_cloudflare_stickers/)" by [pizzaiolo_](https://www.reddit.com/user/pizzaiolo_/)

"[Ditch Cloudflare - Broken HTTPS/MiTM](https://greysec.net/showthread.php?tid=1256)" by [NO-OP](https://greysec.net/member.php?action=profile&uid=47)

"[Slow load after settng up CloudFlare](https://www.webpagetest.org/forums/showthread.php?tid=14948)" by [Altberger](https://www.webpagetest.org/forums/member.php?action=profile&uid=456680)

"[CloudFlare - What the Daily WTF?](https://what.thedailywtf.com/topic/11168/cloudflare)" by [Daniel Beardsmore](https://what.thedailywtf.com/user/daniel_beardsmore)

"[HTTPSを「借りる」のはやめよう。](https://srad.jp/comment/3349767)" by [Printable is bad.](https://srad.jp/~Printable%20is%20bad./)

"[PSA: Do not trust any website using CloudFlare.](https://www.reddit.com/r/Bitcoin/comments/5w13kw/psa_do_not_trust_any_website_using_cloudflare/)" by [danda](https://www.reddit.com/user/danda/)

"[How do I get rid of cloudflare on Windows 10?](https://answers.microsoft.com/en-us/windows/forum/windows_10-win_cortana/how-do-i-get-rid-of-cloudflare-on-windows-10/bab116b1-5c09-4952-aa18-9d8e8261b705)" by [BillWebster1964](https://answers.microsoft.com/en-us/windows/forum/windows_10-win_cortana/how-do-i-get-rid-of-cloudflare-on-windows-10/en-us/profile/e12a62cc-0c40-4e09-9199-5c5b41f65e2f)

"[Fucking Cloudflare](https://www.reddit.com/r/TOR/comments/351ept/fucking_cloudflare/)" by [deleted](https://www.reddit.com/r/TOR/comments/351ept/fucking_cloudflare/)

"[The CloudFlare MITM](https://www.reddit.com/r/Bitcoin/comments/1yj948/the_cloudflare_mitm/)" by [davout-bc](https://www.reddit.com/user/davout-bc/)

"[Cloudflare as a Security Risk - Support - Whonix Forum](http://forums.whonix.org/t/cloudflare-as-a-security-risk/2162)" by [entr0py](https://forums.whonix.org/u/entr0py)

"[My dad is getting pop-ups from CloudFlare every time he types something into his Mozilla Firefox search/address bar and hits enter.](http://archive.is/XTRlk)" by [Yahoo! user](http://archive.is/XTRlk)

"[Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351)" by [nullius](https://trac.torproject.org/projects/tor/ticket/24351)

"[Issues with corporate censorship and mass surveillance](https://trac.torproject.org/projects/tor/ticket/18361)" by [ioerror](https://trac.torproject.org/projects/tor/query?status=!closed&reporter=ioerror)

"[Padlock icon indicates a secure SSL connection established w MitM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835)" by [Anonymous](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835)

"[The catch you're missing](https://www.reddit.com/r/Wordpress/comments/2zpq2g/cloudflare_free_plan_too_good_to_be_true/cpldjc6/)" by [cqwww](https://www.reddit.com/user/cqwww)

"[Why did Cloudflare go down on July 2, 2019?](https://www.quora.com/Why-did-Cloudflare-go-down-on-July-2-2019)" by quora

"[How likely is it that CloudFlare is an NSA operation?](https://www.quora.com/How-likely-is-it-that-CloudFlare-is-an-NSA-operation/answer/Hamid-Sarfraz)" by quora

"[cloudflare 是如何转发 HTTPS 流量的？](https://www.v2ex.com/t/406759)" by [feast](https://www.v2ex.com/member/feast)

"[Cloudflare – The Asocial](http://asocialfz7ncw5ui.onion/articles/internet/cloudflare.html)" by [ASocial](https://theasocial.github.io/) [[mirror](https://theasocial.github.io/articles/internet/cloudflare.html)]

"[CloudFlare – Sipuliwiki 3](http://nla423n3gyyunhci.onion/index.php?title=CloudFlare)"

"[I keep getting cloudflare blocking access to some websites](https://support.mozilla.org/en-US/questions/1106240)" by [vbbuilt](https://support.mozilla.org/en-US/user/vbbuilt)

"[fuckcloudflare/cloudflare-tor](http://volagitvnzf3o56b.onion/cgit/fuckcloudflare/cloudflare-tor/)"

</details>

------

<details>
<summary>_click me_

## Microsoft GitHub
</summary>


"[CloudFlare rejects my benchmark](https://github.com/jsperf/jsperf.com/issues/523)" by [willydee](https://github.com/willydee)

"[Redirection Error](https://github.com/johngodley/redirection/issues/2186)" by [Jmira323](https://github.com/Jmira323)

"[[BUG] 429 Too Many Requests](https://github.com/npm/cli/issues/836)" by [raphaelyancey](https://github.com/raphaelyancey)

"[Cloudflare challenge loop (again)](https://github.com/codemanki/cloudscraper/issues/285)" by [elpaxel](https://github.com/elpaxel)

"[Handle new cloudflare blocking](https://github.com/krues8dr/lazuli/issues/29)" by [krues8dr](https://github.com/krues8dr)

"[Add cloudflare to blocked services](https://github.com/AdguardTeam/AdGuardHome/pull/1155/commits/d42d475d1b4376055adee3e6181c9a5624af7525)" by [AdguardTeam](https://github.com/AdguardTeam)

"[Cloudflare blocks feed update](https://github.com/QuiteRSS/quiterss/issues/1076)" by [QuiteRSS](https://github.com/QuiteRSS)

"[[Bug] API blocked by CloudFlare](https://github.com/angel-penchev/globaloffensive-predictor/issues/4)" by [angel-penchev](https://github.com/angel-penchev)

"[Cloudflare blocks me when trying to include jQuery](https://github.com/jsperf/jsperf.com/issues/518)" by [jsperf](https://github.com/jsperf)

"[Cloudflare blocks this client library](https://github.com/apixu/apixu-java/issues/1)" by [apixu](https://github.com/apixu)

"[blocked by cloudflare](https://github.com/thinkle/gourmet/issues/885)" by [imesg](https://github.com/imesg)

"[Cloudflare blocked](https://github.com/sinkaroid/Hentai2read-Grabber/issues/1)" by [sinkaroid](https://github.com/sinkaroid)

"[Use a CDN that doesn't use cookies](https://github.com/badges/shields/issues/2986)"

"[Blocked by cloudflare](https://github.com/bitmarket-net/api/issues/24)" by [roquez](https://github.com/roquez)

"[Can't access your API](https://github.com/bitcoincoltd/bitexthai/issues/7)" by [adeelotx](https://github.com/adeelotx)

"[Can't access susper, because of cloudflare](https://github.com/fossasia/susper.com/issues/903)" by [clownfeces](https://github.com/clownfeces)

"[Extension blocked by Cloudflare](https://github.com/bit4woo/knife/issues/11)" by [Neolex-Security](https://github.com/Neolex-Security)

"[Custom DNS option not working !](https://github.com/Jigsaw-Code/Intra/issues/209)" by [Nokia808](https://github.com/Nokia808)

"[Patreon added Cloudflare, blocks get_file_contents](https://github.com/daemionfox/patreon-feed/issues/7)" by [TwistedMexi](https://github.com/TwistedMexi)

"[Why do I have to complete a CAPTCHA?](https://github.com/KyranRana/cloudflare-bypass/issues/119)" by [DrPaw](https://github.com/DrPaw)

"[i found a solution for cloudflare](https://github.com/KyranRana/cloudflare-bypass/issues/116)" by [WAVDEVTEAM](https://github.com/WAVDEVTEAM)

"[Notes on privacy and data collection of Matrix.org](https://gist.github.com/maxidorius/5736fd09c9194b7a6dc03b6b8d7220d0#gistcomment-2963692)" by [maxidorius](https://github.com/maxidorius)

"[The feed stopped working](https://github.com/splitbrain/patreon-rss/issues/4)" by [leandroprz](https://github.com/leandroprz)

"[Tor and Cloudflare](https://github.com/Eloston/ungoogled-chromium/issues/783)" by [nchv](https://github.com/nchv)

"[Stop using Cloudflare to make site https](https://github.com/nownabe/blog.nownabe.com/issues/45)" by [nownabe](https://github.com/nownabe)

"[Stop using Cloudflare](https://github.com/vector-im/riot-web/issues/8691)" by [theel0ja](https://github.com/theel0ja)

"[Stop using CloudFlare as a public resolver](https://github.com/dappnode/DNP_BIND/issues/20)" by [vdo](https://github.com/vdo)

"[Please stop using CloudFlare.](https://github.com/danarel/thinkprivacy/issues/5)"

"[Wire, Please stop using CloudFlare](https://github.com/wireapp/wire-webapp/issues/5716)"

"[What do you think about Cloudflare? PTIO](https://github.com/privacytoolsIO/privacytools.io/issues/374)"

"[What do you think about Cloudflare? PRISM](https://github.com/prism-break/prism-break/issues/1843)"

"[let's talk about our little buddy cloudflare](https://github.com/ghacksuserjs/ghacks-user.js/issues/310)" by [Thorin-Oakenpants](https://github.com/Thorin-Oakenpants)

"[API is currently always returning a 503 error triggered by the Cloudflare protection](https://github.com/ICObench/data-api/issues/7)" by [TiesdeKok](https://github.com/TiesdeKok)

"[Add an option to stop trusting Cloudflare certificate](https://github.com/mozilla-mobile/focus-android/issues/1743)" by [StopMITMInternational](https://github.com/StopMITMInternational)

"[Block Cloudflare MITM Attack](https://github.com/nym-zone/block_cloudflare_mitm_fx)" by [nym-zone](https://github.com/nym-zone)

"[List of Sites on Cloudflare DNS](https://github.com/pirate/sites-using-cloudflare)" by [pirate](https://github.com/pirate)

</details>

------

<details>
<summary>_click me_

## Twitter
</summary>


> Too many to list here. It is IMPOSSIBLE to list them all! [See for yourself](https://twitter.com/search?q=cloudflare&f=live).


"For anyone having issues accessing the repo from the UK you will need to use a vpn as cloudflare is blocking access from the UK for some reason." by @[F2P_Gamer](https://nitter.net/F2P_Gamer/status/1233417591512805376)

"So is everyone ignoring the multiple times Cloudflare has gone offline and taken down a large chunk of the internet with it? Or the hassle it has caused normal users who it suddenly decides are worthy of a block?" by @[Foxhack](https://twitter.com/Foxhack/status/1232758206209777665)

"i've seen one of my client sites get an ip block from cloudflare, just what I need when people start calling unable to browse because their DNS over HTTPS is blocked" by @[dakotathekat](https://twitter.com/dakotathekat/status/1232738497280331782)

"Exactly! cloudflare are a major pita - they just ban huge tracts of ASN - all these sites fronting with cloudflare - do you even realize people can't get to your site anymore?" by @[nthcolumn](https://twitter.com/nthcolumn/status/1232412097436430339)

"I'm still looking for an answer to this question that makes sense: Why do people trust CloudFlare with all your DNS data? With your ISP, at least you have a legal contract. FireFox end-users don't have a contract with CloudFlare. Why is CloudFlare better than your ISP?" by @[JeroenJacobs79](https://twitter.com/JeroenJacobs79/status/1232594190313365504)

"Had to delete the Cloudflare app. The latest update completely broke my phones connectivity." by @[bassthumpa](https://twitter.com/bassthumpa/status/1221446155772538880)

"The Cloudflare 5s redirect protection thingy broke RSS feeds a long time ago" by @[ArtemR](https://twitter.com/ArtemR/status/1219388218065342464)

"I think the site just broke. Getting 502s from Cloudflare back" by @[faker_](https://twitter.com/faker_/status/1204443148749082624)

"I tried Cloudflare dns for a week or so.
Found many things that I use regularly wouldn't work anymore like they were blocked. There theres the "we have anonymous dns but we collect data and share it with a partner" bullshit that makes me wonder what they're really up to." by @[NubsWithGuns](https://twitter.com/NubsWithGuns/status/1232767004966313984)

"As someone who has been IP blocked by Cloudflare for no reason at all, and who has had trouble with them because of their trash service rendering entire swaths of the internet inaccessible, this is bullshit of the stinkiest caliber." by @[Foxhack](https://twitter.com/Foxhack/status/1232380579686932480)

"Unable to login to reply app or support website. Getting Error 1020 Access denied by Cloudflare. Please help" by @[VishwaKk](https://twitter.com/VishwaKk/status/1232759611507167233)

"Hi Rich! I've been unable to get on the forums these past 2 days. I keep getting Cloudflare error 522 whenever I try to load anything. Tried looking tha error up and it might be a problem w/ the site." by @[3nz3r0](https://twitter.com/3nz3r0/status/1232345563724570626)

"why not use dnscrypt-proxy instead of this untrustworthy MiTM called cloudflare" by @[pitchf___](https://twitter.com/pitchf___/status/1232459457201721344)

"Centralizing DNS queries to a privately held US company is a huge step backwards for the security of the Internet. I thought the EFF had more sense than to support this misguided step from mozilla" by @[brett_sheffield](https://twitter.com/brett_sheffield/status/1232421747334549504)

"Well cloudflare definitely do block by ip, it's their thing. I'd never use cloudflare dns anyway - what is the point swapping one spy for another? " by @[nthcolumn](https://twitter.com/nthcolumn/status/1232422055171305474)

"Screw Cloudflare - it spams literally endless captcha if u try break out from even the Tor Browser, into a Browser from another Country... !" by @[BAZ65529105](https://twitter.com/BAZ65529105/status/1232388280143904768)

"The cloudflare captcha challenge when accessing IGIHE mobile website is stressful. Even if when one is an expert, it takes many attempts to get over it, imagine the pain experienced by elderly visitors with basic scroll-and-touch  knowledge." by @[ndahiroho](https://twitter.com/ndahiroho/status/1231173498292363266)

"Hats off to Medium for this example of backwards security. This is a CAPTCHA – a test to validate you are a human and not a program – protecting a JSON API, a URL that sends data for programs to read " by @[jsoverson](https://twitter.com/jsoverson/status/1230935263838429188)

"I keep getting this? what does it mean please?" by @[DarloStriker](https://twitter.com/DarloStriker/status/1231867141722427392)

"Starting to hear clown music every time I get that Cloudflare 522 error page" by @[emarl_](https://twitter.com/emarl_/status/1231300374406918144)

"Yeah getting a 522 error from cloudflare as well. Hmmm." by @[Neurodyne_One](https://twitter.com/Neurodyne_One/status/1231248068034617351)

"lopp.net down ? I have a cloudflare error" by @[orcitis](https://twitter.com/orcitis/status/1231069662613405697)

"Stop using Cloudflare and use your premium money to use a more solid service." by @[DKittycoon](https://twitter.com/DKittycoon/status/1230850744791814144)

"Wow - another risk of Cloudflare's false positives - consumers being denied access to medical information" by @[peter_tonoli](https://twitter.com/peter_tonoli/status/1221699671048867840)

"That wasn't a challenge - that was an outright block, and outright user hostile." by @[peter_tonoli](https://twitter.com/peter_tonoli/status/1222321557873119232)

"Why is your DNS blocking Chinese Gaming mouse company's ?" by @[01000110_010101](https://twitter.com/01000110_010101/status/1231143359810502667)

"Stuck in cyberspace trying to access a Cloudflare hosted site through a Cloudflare VPN and get blocked by Cloudflare" by @[objectivechad](https://twitter.com/objectivechad/status/1232098408137207808)

"There was an issue this weekend, #Cochrane Library blocked by Cloudflare.  It appears to be fixed but check access to Cochrane." by @[CSU_SFX](https://twitter.com/CSU_SFX/status/1229454195092180992)

"Dear npmjs: Please get rid of your active attacker, that means Cloudflare. npm update fails because Cloudflare is actively blocking updates meaning more vulnerable systems out there." by @[MacLemon](https://twitter.com/MacLemon/status/1229367173405118464)

"Cloudflare seems to be blocking viewing this.." by @[HotDiggetyDam](https://twitter.com/HotDiggetyDam/status/1230643765603336194)

"The NSA LOLs about TOR, because all they have to do is sit behind Cloudflare’s TLS termination endpoints." by @[Profpatsch](https://twitter.com/Profpatsch/status/1229463942616973319)

"if you try to view your forum through tor, you are looped through an endless cycle of cloudflare challenges that never lets you get to the actual forum." by @[sziggle](https://twitter.com/sziggle/status/1230595595083091969)

"¿Se puede remediar sin tener que dejar de usar Tor?" by @[diesanromero](https://twitter.com/diesanromero/status/1230266376444575744)

"自分のポートフォリオサイトがError 1016とか出ててCloudflareに問い合わせしようかと思ったら10分ぐらいして勝手に直った。おいおい、Cloudflare、、" by @[okutani_t](https://twitter.com/okutani_t/status/1230474294087442432)

"個人で使ってる Cloudflare、勝手に Origin DNS Error になって勝手に復活した" by @[hassasa3](https://twitter.com/hassasa3/status/1230477819093741569)

"I just made 17 euro to 500 today and now the website is down? What is going on ?" by @[DragonWhammer](https://twitter.com/DragonWhammer/status/1230590795398467586)

"keep getting EM 520 from local newspaper websites, it reports Cloudflare is working but host server error. If I leave my Broadband and use my Mobile signal it works fine." by @[HughGW](https://twitter.com/HughGW/status/1230517382608805891)

"cuz poeple are to dump to properly protect their servers against malicious traffic and just block known tor nodes or even worse, use cloudflare" by @[bus1nessrasta](https://twitter.com/bus1nessrasta/status/1229799855213727744)

"We're getting 403s delivered by Cloudflare (your MITM CDN) when trying to `npm update` our server application. Are you aware of this?" by @[MacLemon](https://twitter.com/MacLemon/status/1229369465537777665)

"I'm not sure if this has been realized, but I think the error we are getting in the middle of raids is because cloudflare wants a captcha from the game?" by @[bobjobob24](https://twitter.com/bobjobob24/status/1226220361857019905)

"Interesting, customer hitting our API from a Lambda function is sometimes prompted with a Cloudflare captcha. Turns out you don't control the origin IP of the Lambda. Malicious actors might use the same IP address, causing the IP to get flagged as having a bad reputation." by @[tomdev](https://twitter.com/tomdev/status/1227198530198163456)

"Patreon has a very nasty Cloudflare CAPTCHA infection" by @[dxgl_org](https://twitter.com/dxgl_org/status/1228030068435357696)

"why is it giving me a cloudflare captcha thing when I try to go to myreadingmanga" by @[muttzone](https://twitter.com/muttzone/status/1228566293323927553)

"npm: why respond on a public issue before telling us?" by @[wesleytodd](https://twitter.com/wesleytodd/status/1229453056086921216)

"Is your promotion of alcohol why you use Cloudflare?" by @[dxgl_org](https://twitter.com/dxgl_org/status/1229506984485048320)

"hi, my wan ip is blocked by cloudflare, now my fitbit and sonos can not connect to theire servers, how can  I remove my wan ip from your blacklist?" by @[nl1wdj](https://twitter.com/nl1wdj/status/1226226530172440577)

"Steam users have threatened to dox me; now kiwifarms, a happy customer of Cloudflare" by @[dxgl_org](https://twitter.com/dxgl_org/status/1228031362915012611)

"Need help" by @[AbeysingheRichi](https://twitter.com/AbeysingheRichi/status/1228751658064207873)

"why am i getting blocked from every device on off---white.com? :(" by @[copbud](https://twitter.com/copbud/status/1228695503476600834)

"Blocked for no reason on my computer when I never opened your site till today? Over zealous much and I have to resort to my mobile just to see you site wow. I suggest figuring out your problem blocking people and denying people at random with cloudflare." by @[TrevorMacewan](https://twitter.com/TrevorMacewan/status/1228848856151904256)

"So npmjs.org broke because they were using HTTP referrers outside the HTTP spec and Cloudflare rate limited them. " by @[g0rdin](https://twitter.com/g0rdin/status/1229536718266814468)

"NPM is rate limiting and people's builds are failing" by @[HNTweets](https://twitter.com/HNTweets/status/1229392562772942848)

"Have been blocked by bstategames Cloudflare protection while playing the game. Just a Popup that wouldn't go away and the option to restart the game. Any idea why?" by @[BlackyWerSonst](https://twitter.com/BlackyWerSonst/status/1226220100610666497)

"Hello, why do I get this screen at some websites?" by @[](https://twitter.com/PatrickVkmp/status/1226230725336293377)

"Why am I keep getting the cloudflare Attention Required Solved Captcha thing over & over & over again?" by @[blackinmind123](https://twitter.com/blackinmind123/status/1226273879078862848)

"#CloudFlare is now hitting the archive.org wayback machine with the same #CAPTCHA as #Tor users, thus censoring history too." by @[namanwtf](https://twitter.com/namanwtf/status/1220993565762805763)

"can't do captcha to prove I'm not a robot because I might be a robot" by @[olihough86](https://twitter.com/olihough86/status/1225420620495757312)

"Cloudflare is blocking your content" by @[Roxann_Minerals](https://twitter.comt/Roxann_Minerals/status/1226990674588897280)

"If your server is behind Cloudflare, you are losing, big time. The vast Internet multitudes see that "check all the crosswalks" captcha, and click away." by @[BobPurvy1](https://twitter.com/BobPurvy1/status/1222337944473755650)

"google recaptcha actually sux sometime they give captcha that can never be solved. And one day i solved like 15 minutes and gave up. All was due to fault of cloudflare which uses dummy google recaptcha." by @[shirshak55](https://twitter.com/shirshak55/status/1224041961125867520)

"Ever heard of CloudFlare? Your DDOS protection is blocking tons of range of IP that legit customers use, which is completely useless for blocking DDOS and preventing users to connect." by @[Fealen3](https://twitter.com/Fealen3/status/1223078522412924929)

"I get blocked by cloudflare a lot, as well as a ludicrous amount of recaptchas (I suspect it is due to ad/JS/cookie-blocking)." by @[tnhh](https://twitter.com/tnhh/status/1219698503649234944)

"Cloudflare having some issues reaching the bouqs flower website because of a security service protecting it from cyber attacks? Clearly not trying anything malicious just need flowers lmao" by @[PlagueBitch](https://twitter.com/PlagueBitch/status/1223292192439119872)

"Europe/Germany getting Cloudflare Error on loading in the menu, and this error here says cant load ingame, bad account id ?" by @[FetteMacke](https://twitter.com/FetteMacke/status/1224076509800955904)

"Cloudflare's rate limiting is the worst thing to script for in a testing sense. Depending on the plan, it could be any combination of values for request limits in certain time frames, then block times vary, or you can send people to js challenges and stuff. ugh." by @[incredincomp](https://twitter.com/incredincomp/status/1219688905093439488)

"See just know you won't fix that error 1000 without deleting cloudflare.... Seriously is cloudflare needed for your site?" by @[mvptrinity](https://twitter.com/mvptrinity/status/1221027782693617664)

"Getting an error from CloudFlare on those archive links." by @[IonotterA](https://twitter.com/IonotterA/status/1221971502104481793)

"Game is down in the same way as last night. Can't get to profile. Gives some cloudflare error and then you're screwed from there" by @[MikkelVilla](https://twitter.com/MikkelVilla/status/1224026651211771906)

"Even unable to login in bungie as it's complaining about error 500 and cloudflare." by @[solidus198306](https://twitter.com/solidus198306/status/1222947361594531842)

"CloudFlare is already the biggest MITM network on earth - and with the percentage of the web that is powered by CF, I'm sure people blindly trust MITM (Or maybe doesn't know any better)." by @[LucasRolff](https://twitter.com/LucasRolff/status/1217747562217779201)

"Does anyone know offhand, if Cloudflare is wrongly flagging your IP as a bot, is there a way to either A) confirm/test that, or B) request it be whitelisted?" by @[fienen](https://twitter.com/fienen/status/1205285537420107777)

"I'm just getting a captcha challenge when I try to log in to Sling. Not a problem on computers, but it causes the Fire Stick and Android apps to straight up fail, because they don't account for the challenge in the app." by @[fienen](https://twitter.com/fienen/status/1205501216555458561)

"CloudFlare Origin DNS Error (Cloudflare cannot resolve the A or CNAME record requested.)" by @[YggdrasilStatus](https://twitter.com/YggdrasilStatus/status/1204742963005403137)

"Still over Tor it's hard not to get a cloudflare captcha... Haven't found a reliable way." by @[j6sp5r](https://twitter.com/j6sp5r/status/1207676342852079618)

"Cloudflare appears to be blocking access to the source site for this article" by @[Escher911](https://twitter.com/Escher911/status/1207742170893041670)

"Cloudflare will happily inflict CAPTCHAs on Tor users, reformat images and otherwise MITM user traffic to websites But when it comes to screening for abusive content" by @[randomoracle](https://twitter.com/randomoracle/status/1207387123742953472)

"Hello, I am writing from Chile. I visit often and shop at the Sam Ash site in the USA. Today I find that it prevents me from accessing the page due to a "ERROR 1020" of CLOUDFLARE please explain how I can solve this to navigate the page and be able to buy again. " by @[IamXavichu](https://twitter.com/IamXavichu/status/1189226368099332098)

"Looking into issues with Cloudflare, stay tuned" by @[ponytownteam](https://twitter.com/ponytownteam/status/1207124820183470080)

"Cloudflare isn't very reliable compared to google" by @[PawzLenaLuLa](https://twitter.com/PawzLenaLuLa/status/1207125640224366592)

"there appears to be an issue with the Cloudflare captcha this morning that's preventing posting to your forum?" by @[Lowe0](https://twitter.com/Lowe0/status/1204778899735040002)

"Did they take down that site that was the subject of the angry TrustPilot review?  If not they are lying through their teeth." by @[dxgl_org](https://twitter.com/dxgl_org/status/1206599735869612032)

"It kept sending me to cloudflare and it said the site was down." by @[TaiKamiya101](https://twitter.com/TaiKamiya101/status/1206998694740004866)

"When I check your site, my browser signs "Error 522" in now. Maybe Cloudflare's network server is down, i think" by @[sallahge69](https://twitter.com/sallahge69/status/1207253735052726272)

"trying to hit your customer support page but getting blocked by cloudflare." by @[JoshReedSchramm](https://twitter.com/JoshReedSchramm/status/1206589401914134528)

"Cloudflare is blocking access to snort from Cuba. We can't download Snort community rules from snort.org " by @[ReneHernndez67](https://twitter.com/ReneHernndez67/status/1204144625390358529)

"Cloudflare your 1.1.1.1 DNS is blocking #PS4 downstream packets. Interrupting all streaming apps on the console. Please, investigative." by @[VanGThieu](https://twitter.com/VanGThieu/status/1203548880639610880)

"Cloudflare is more like the guy who puts a big tent over your house so no one can see inside and places a bouncer at the front door. They are more like the internet mafia than the county clerk." by @[phyzonloop](https://twitter.com/phyzonloop/status/1198608708042919937)

"Cloudflare oh my god get your shit together. Surely you can survive one poke from Google without bringing down half the internet." by @[MoNetProduction](https://twitter.com/MoNetProduction/status/1206280272964210690)

"I've got major issues with Firefox these days and their insane desire for DoH which is just stupid and supports Cloudflare who are an utterly vile company." by @[plambrechtsen](https://twitter.com/plambrechtsen/status/1206491416693575682)

"Dear Cloudflare I'm not a f***ing robot and this time I will not turn of my IPv6 tunnel. :-(" by @[QuuxBerlin](https://twitter.com/QuuxBerlin/status/1203993799288066053)

"CAPTCHAs interrogating us as if we were robots all day?" by @[hyphybyterhymer](https://twitter.com/hyphybyterhymer/status/1200130090354348032)

"I can't seem to download the new installer for FL Studio 20.6, I'm getting an error from CloudFlare saying that the server is down." by @[oksymusic](https://twitter.com/oksymusic/status/1205581584767864833)

"cloudflare Mafia exposed. This would qualify as single point of failure." by @[cotlage](https://twitter.com/cotlage/status/1146059776750522369)

"cloudflare is like a mafia racket SURE IS A NICE WEBPAGE YOU GOT THERE WOULD BE A SHAME IF SOMETHING WERE TO H A P P E N TO IT" by @[toolboxybrown](https://twitter.com/toolboxybrown/status/1158216847973650432)

"I fucking hate Cloudflare" by @[NDarkson984](https://twitter.com/NDarkson984/status/1205553945764073473)

"Cloudflare Not working" by @[shuttle_flash](https://twitter.com/shuttle_flash/status/1204972998370807810)

"I am having trouble accessing websites protected by Cloudflare My IP address that was assigned to me makes use their capture to prove I am human.  Tech support won't help me!  They say I have to get new equipment.  My wife works from home and needs this fixed!" by @[prowler9](https://twitter.com/prowler9/status/1204978832828227584)

"You should stop using Cloudflare." by @[dxgl_org](https://twitter.com/dxgl_org/status/1204901363165081601)

"Cloudflare shenanigans had better have affected just me and not on purpose. Missing reach oportunities over questionable provider choice would be silly." by @[make_post](https://twitter.com/make_post/status/1205271946654564352)

"when you use a rate limit that doesn't allow access ~15 pages in 10 minutes that's asinine." by @[ModestTim](https://twitter.com/ModestTim/status/1199245015681093637)

"As of right now I cant enter to any website that uses Cloudflare, that includes Discord." by @[htfcuddles_](https://twitter.com/htfcuddles_/status/1198258548892950528)

"interacting with any website that uses Google's CAPTCHAs via the Tor browser is dang near impossible these days" by @[conspirator0](https://twitter.com/conspirator0/status/1197685548137295872)

"Google is fairly aggressive in making Tor difficult to use. Ditto for cloudflare." by @[funnymonkey](https://twitter.com/funnymonkey/status/1197706488585818112)

"Just learned that cloudflare blocks a lot of tor exit nodes." by @[sidravic](https://twitter.com/sidravic/status/1197883248883449856)

"Cloudflare are all the networks up right now? Facing issues with Singapore" by @[ankyitm](https://twitter.com/ankyitm/status/1204974596866232320)

"Cloudflare your DNS service (1.1.1.1) is down. Seems like it isn’t resolving most queries on my end." by @[luckyjajj](https://twitter.com/luckyjajj/status/1204916602984189952)

"WTF Cloudflare is down " by @[reginpv3](https://twitter.com/reginpv3/status/1204529772430102528)

"Perfect timing for Cloudflare to go down for maintenance when I'm trying to deal with a DDOS attack..." by @[brussell98](https://twitter.com/brussell98/status/1204527167033544704)

"The problem resolved for me when I switched from Cloudflare's DNS to Suddenlink's defaults, though I'm not entirely sure why." by @[CascadeMage](https://twitter.com/CascadeMage/status/1204321262119505925)

"Cloudflare Hey is WARP down today? I can't use it at all..." by @[ZenIsBestWolf](https://twitter.com/ZenIsBestWolf/status/1204161037613842434)

"After careful measurements my company decided to turn off Cloudflare for our sites about a year ago. We actually saw a non-trivial performance increase in some metrics." by @[jdgregson](https://twitter.com/jdgregson/status/1204582588897267718)

"I found no noticeable enhancement in performance." by @[dxgl_org](https://twitter.com/dxgl_org/status/1204570003179212800)

"Ya, they're consistently tell you where they can be filed and ignored. Cloudflare doesn't care about victims of websites behind its infrastructure." by @[phyzonloop](https://twitter.com/phyzonloop/status/1204569060161343490)

"Using #Tor, the worst user experience comes from dealing with #Cloudflare and #reCAPTCHA." by @[VasilyGrz](https://twitter.com/VasilyGrz/status/1188583129612726272)

"TFW someone from Cloudflare tells you they offer "end-to-end encryption." Your business model is literally MITM'ing enterprise web traffic at scale." by @[toholdaquill](https://twitter.com/toholdaquill/status/1202408279621722114)

"explain why Cloudflare isn't the utopia they want you to believe" by @[dxgl_org](https://twitter.com/dxgl_org/status/1203669736455884801)

"I'm blocked from viewing hypixel.net  bc of cloudflare and was wondering if there's any way you could help?" by @[awyssloaf](https://twitter.com/awyssloaf/status/1204184961647095808)

"Is the Labour Party website using Cloudflare to block access from Ukraine? I will try again when I get to my hotel." by @[stuartbruce](https://twitter.com/stuartbruce/status/1203350830465900544)

"to clarify, i also mean on websites that use cloudflare stuff, like i keep getting asked that almost every time i go to a website" by @[EeveeEuphoria](https://twitter.com/EeveeEuphoria/status/1204293412574420992)

"I keep thinking that CloudFlare is some kind of a weird human subject experiment on Tor users, like how many can be tricked into enabling JS" by @[hackerb0t](https://twitter.com/hackerb0t/status/1203705804064321536)

"Keep getting Errors on images being serverd via CloudFlare proxy, plus a couple of random sites DNS won't resolve, is it an issue on your end?" by @[minecraft_buzz](https://twitter.com/minecraft_buzz/status/1204561304259891201)

"and therefore, we'll bypass your very own security rules so that CloudFlare Inc. can keep all your personal data and metadata safe, because, well, don't you think security is important?" by @[cunnigliu](https://twitter.com/cunnigliu/status/1202979464696737793)

"Does that mean Cloudflare could be throwing away SpamCop reports?  If so SpamCop needs to be notified that Cloudflare is not properly responding to abuse reports." by @[dxgl_org](https://twitter.com/dxgl_org/status/1204521082767781895)

"I'm getting cloudflare captcha when trying to reach" by @[Forcen](https://twitter.com/Forcen/status/1203771208195563522)

"Dashboard is currently down, though nothing on the Status page indicates this, but other users are reporting it too." by @[hammyhavoc](https://twitter.com/hammyhavoc/status/1204528653926834176)

"here we go again. Zoom not working. ERROR 1020 tell your cloudflare thetmy are blocking subscribers " by @[DanceTeacher55](https://twitter.com/DanceTeacher55/status/1203029649498001408)

"Please re-think your use of Google's reCAPTCHA. Just had to fill in 10 quizzes correctly (not joking) in order to login. You block DDoS attempts on your network all day long, are you saying you rely on Google to detect bots??" by @[dmpinder](https://twitter.com/dmpinder/status/1202982157104427008)

"This site isn't working. I just get a cloudflare error." by @[mojojojo](https://twitter.com/mojojojo/status/1203060938070315009)

"The problem I see there is MitM - do you really want them to terminate your TLS connection with your bank, your doctor, etc., in their perfect cloud instance? I do not." by @[BrendanEich](https://twitter.com/BrendanEich/status/1201701763583070209)

"Please tell your official accounts to stop blocking me on Twitter." by @[dxgl_org](https://twitter.com/dxgl_org/status/1197277192809582592)

"I guess you deployed Cloudflare to mitigate NW traffic issues (DDoS ?). But it is not working properly, it is blocking valid customers too, like me. See pic." by @[aerohari](https://twitter.com/aerohari/status/1201743978925674497)

"Cloudflare is currently experiencing significant issues. Therefore, our website and others managed by CloudFlarws DNS are unavailable. " by @[VimlyDigital](https://twitter.com/VimlyDigital/status/1200228062404792320)

"Cloudflare vro when you go down like half of the internet does please stop" by @[ItJustAPsychoEx](https://twitter.com/ItJustAPsychoEx/status/1200227386857598976)

"CloudFlare. Grootste MITM proxy -ever build-.  Continue expansie drift die zorgt voor centralisatie (alles loopt via CloudFlare).  Onder andere door concurrenten uit de markt te prijzen." by @[UID_](https://twitter.com/UID_/status/1199264018365603840)

"Oh, I found out what it was. You guys have CloudFlare human checks on your images. I had to go one of the images hosted on images.g2.com  directly and click the captcha." by @[clintonskakun](https://twitter.com/clintonskakun/status/1200536371364028416)

"Problem of tor is that it’s blocked by a lot of websites, any website "protected" by Cloudflare can’t be accessed from TOR" by @[jusdepatate_](https://twitter.com/jusdepatate_/status/1200689459391533057)

"so i can't access my website or discord, or any other cloudflare protected website i know." by @[twitch0001](https://twitter.com/twitch0001/status/1200227292619902978)

"Cloudflare is down but only in the UK and i am the big angy because i was literally gunna work for an hour but now I can’t access any of my stuff " by @[Beatlinked](https://twitter.com/Beatlinked/status/1200231055552401409)

"I can't access my.parallels.com  ParallelsCares - Cloudflare seems to think you don't exist... can you help? I'd love to take advantage of your Black Friday offer... " by @[jmeisenberg](https://twitter.com/jmeisenberg/status/1200345456913666053)

"Cloudflare is experiencing difficulties. Parts of our website (that are active) are inaccessible to *some users.* This is unfortunately out of our control. " by @[intpts](https://twitter.com/intpts/status/1200240646881239040)

" I just got 3 friends here in Germany to try - and NONE of them can gain access - they get blocked by the same captcha. I'm thinking Cloudflare has Deutsche Telekom blacklisted, or some such." by @[GrayanOne](https://twitter.com/GrayanOne/status/1200057914842587136)

"Just a friendly reminder that if your  Black Friday sales page requires someone to: check a Cloudflare “I’m not a robot” check box  solve a CAPTCHA image it’s probably not worth the discount." by @[CoryJaccino](https://twitter.com/CoryJaccino/status/1199946959110443008)

"the site seems to use Cloudflare which blocks a lot of users from the start, so a large crowd can't open the site even if it's online" by @[adlerweb](https://twitter.com/adlerweb/status/1199966079126310912)

"Day 2 of trying to get my Cloudflare account deleted. They sure don't make it easy, or logic." by @[originalesushi](https://twitter.com/originalesushi/status/1199041528414527495)

"Just don't make me discover my PII in some future breach or I'll dig up my Cloudflare exodus logs and I'll step on the biggest soapbox I can find to cast hell upon you." by @[originalesushi](https://twitter.com/originalesushi/status/1199063754740314115)

"Hi there ! Your cdnjs is blocked by Stormshield firewall IPS." by @[nivuniconu_](https://twitter.com/nivuniconu_/status/1199988064589950977)

"So many people are hoping for word of an actually decisive move from the Not-Board that cloudflare thinks  arseblog's under DDoS attack." by @[fuckmedoris](https://twitter.com/fuckmedoris/status/1200180923649732608)

"Whole thing happened again when I started from the beginning to reset my password, then again when it told me to reactivate my account." by @[JayNBoston](https://twitter.com/JayNBoston/status/1199829882131427333)

"You also send all the traffic to the NSA though Cloudflare — thanks but no thanks!" by @[Mcnst](https://twitter.com/Mcnst/status/1199096502968299520)

"marketo users whose instances are hosted in its San Jose data center experienced a service disruption lasting approximately one hour. The problem appears to be due to connectivity issues between Marketo’s CDN provider Cloudflare and its DC" by @[anoufriev](https://twitter.com/anoufriev/status/1197603324297539585)

"I never understood why everyone trusts cloudflare." by @[CSS_Sporklab](https://twitter.com/CSS_Sporklab/status/1198051101423161344)

"By the way Cloudflare has blocked me on Twitter for criticizing them." by @[dxgl_org](https://twitter.com/dxgl_org/status/1198461938411429888)

"You can buy few BTC and wanting to publish them for sale they blocked my account. I don't know that I was wrong. What do I have to do to recover it? Cloudflare Ray ID" by @[AgusNob74](https://twitter.com/AgusNob74/status/1198430473829199872)

"Is krakenfx under some sort of attack? " by @[EnnePoe](https://twitter.com/EnnePoe/status/1197910384973406210)

"Cloudflare is not exactly a free speech big tech company.  I'm not saying this is a cloudflare issue but they proved they hated free speech when the blocked 8chan." by @[HD283271](https://twitter.com/HD283271/status/1197866246458482688)

"Cloudflare is blocking me from the sight at the moment.  The documents should be available there for download." by @[davenlr](https://twitter.com/davenlr/status/1198086978715013120)

"Cloudflare is blocking access from Cuba, so we cannot update clamav, and even send virus sample to alert you" by @[ReneHernndez67](https://twitter.com/ReneHernndez67/status/1197914945637306369)

"FOr months when I try there is only a CloudFlare error. I miss my  stories." by @[cogdog](https://twitter.com/cogdog/status/1198795213357010944)

"Alert Details: 530 - CloudFlare Origin DNS Error (Cloudflare cannot resolve the A or CNAME record requested.)" by @[YggdrasilStatus](https://twitter.com/YggdrasilStatus/status/1198426879071526918)

"site's offline now.... showing cloudflare errors consistently.... :(" by @[Nessy_Reads](https://twitter.com/Nessy_Reads/status/1198362194188132354)

"if you somehow make it so i don't have to click crosswalks/firehydrants/stoplights on Cloudflare for every site, then my shilling might never cease" by @[evan_van_ness](https://twitter.com/evan_van_ness/status/1198010177238261761)

"Cloudflare You have always been a #horrible corp with #broken #software Go away! Now! " by @[MeerMusik](https://twitter.com/MeerMusik/status/1197632454217076736)

"Things are really starting to centralize on Mozilla and CloudFlare. I don’t like it." by @[DrScriptt](https://twitter.com/DrScriptt/status/1171644483626262529)

"Whenever cloudflare goes down and takes a ton of major sites with it I just bang my head against a wall because that's what happens when you centralize tons of internet services." by @[SIGSYS](https://twitter.com/SIGSYS/status/1178661000276578304)

"CloudFlare's website protection is blocking access to BitChute today. :/ Why not, they block other sites already!" by @[waynecolvin](https://twitter.com/waynecolvin/status/1197684878692667392)

"I've been wondering this myself. I've also considered that maybe these Cloudflare people, have taken away bitchute credentials in order to block them from public view. Maybe I should try to reach them on Tor Browser." by @[AlarikOdinsson](https://twitter.com/AlarikOdinsson/status/1197684550446415872)

"Two months ago, I cancelled my Cloudflare account and told them to delete my data. Today, they've emailed me their new privacy policy because I'm still a customer! Trying to pump their user numbers up before an IPO or just sloppy data protection? Either way, not a great look." by @[edent](https://twitter.com/edent/status/1179492594855874561)

"New blogging from me. Even after you delete your account, CloudFlare insist they have the right to email you per a non-existent section of their privacy policy." by @[edent](https://twitter.com/edent/status/1197781221180411905)

"I’ve had the same experience as you with account deletions." by @[daviddlow](https://twitter.com/daviddlow/status/1197787135526555648)

"Do you know if this info  is on any other sites? Sadly patreon has been taken in on the foolishness that is CloudFlare & blocks tor nodes." by @[DeborahPeasley](https://twitter.com/DeborahPeasley/status/1196465140243320832)

"That's actually right approach – Cloudflare is MitM, the connection is not end-to-end encrypted, so insecure. :P" by @[JakubJirutka](https://twitter.com/JakubJirutka/status/1196863997162733568)

"Submitted a case & it was instantly marked as resolved." by @[ebenson_emily](https://twitter.com/ebenson_emily/status/1197585421028483073)

"hey, I'm using a VPN exiting in the DigitalOcean network. Cannot access this site." by @[AliveDevil95](https://twitter.com/AliveDevil95/status/1196409774323224576)

"umm this cloudflare anti DOS shit blocked me from your site. LOL i know shit all about DDOSing or anything, the closest thing to that for me is going on Tor " by @[Undyingtmlg](https://twitter.com/Undyingtmlg/status/1196811999029743621)

"Meta: Access Denied You don't have permission to access forums.tesla.com on this server. heh cloudflare" by @[charlieXwallace](https://twitter.com/charlieXwallace/status/1196875795794288640)

"Someone kept getting their Cloudflare puzzle wrong and said enough is enough." by @[D_Downs](https://twitter.com/D_Downs/status/1146073021163040768)

"I fail to see how Cloudflare would be any improvement for me, even without DoT." by @[jornbaer](https://twitter.com/jornbaer/status/1184895606373961734)

"don't send my traffic via Cloudflare, where the NSA can definitly read it." by @[jornbaer](https://twitter.com/jornbaer/status/1184896773086744576)

"Cloudflare responses 502  Bad gateway. So,I cannot access my account." by @[Lain_Wired_Net](https://twitter.com/Lain_Wired_Net/status/1195191788153298944)

"hey! I cannot access any of my apps for extra life, says cloudflare has banned me :(" by @[MatthewDaleC](https://twitter.com/MatthewDaleC/status/1190819639263137797)

"was just perusing looking for some info using Tor and your website wouldn’t load due to CloudFlare DDoS protection." by @[PupGhost](https://twitter.com/PupGhost/status/1196098648846819328)

"Is the Tor DNS resolver down? I can't hit it or the favicon. It's okay, I just want to know so I can stop trying to get this working" by @[RenoMcK](https://twitter.com/RenoMcK/status/1195556205927600128)

"This is the page that loads when I try to load KSR :( " by @[NickNafster79](https://twitter.com/NickNafster79/status/1191427322261495814)

"Me: visiting a website with tor traffic Cloudflare: (chuckles) I'm in danger." by @[LinuxKodachi](https://twitter.com/LinuxKodachi/status/1190654141036523520)

"I love Tor, but too many sites are inaccessible when using it, thanks to CDNs imposing captchas on Tor connections, which don’t render properly because they all use Google stalkertech. Artificial creation of false ‘security vs privacy’ dichotomy. Looking at YOU, Cloudflare" by @[MissIG_Geek](https://twitter.com/MissIG_Geek/status/1189186672270819328)

"Not to mention, of course, that Cloudflare is also the prime target for mass surveillance" by @[TheBlueMatt](https://twitter.com/TheBlueMatt/status/1178424585244553216)

"Their dns infrastructure just became a prime target for all intelligence agencies. And I think we can safely assume that NSA / Five Eyes will have, or allready have access to that data. With or without cloudflare's cooperation." by @[HackyScientress](https://twitter.com/HackyScientress/status/1188161788619251712)

"البته اگر حریم خصوصی مهم باشه اصلا توصیه نمیشه CloudFlare استفاده کرد به دلایل زیر" by @[Snbig_](https://twitter.com/Snbig_/status/1171836697694351360)

"bitte kein cloudflare benutzen. niemals jemals. einfach nein. cloudflare ist ein big no no." by @[krizpin](https://twitter.com/krizpin/status/1176757352093106178)

"You getting a Cloudflare error too?" by @[michaeldexter](https://twitter.com/michaeldexter/status/1193326043899158528)

"Heck, I spent an hour re-submitting THIS CFP submission. Turns out Cloudflare blocked any submission containing "cmd.exe"" by @[danielhbohannon](https://twitter.com/danielhbohannon/status/1194978231142645760)

"If you’re in EU, the US can force Cloudflare to spy on you for them. At least your Dutch ISP only had to comply with warrants from a few countries. Worse, Cloudflare is *the* most juicy target for NSA/etc... they’re compromised." by @[TheBlueMatt](https://twitter.com/TheBlueMatt/status/1178029559737913345)

"The cause was identified as an error in an automatic update received from Cloudflare" by @[MCMarket_org](https://twitter.com/MCMarket_org/status/1194954194479792129)

"why do you trust cloudflare more than your ISP?" by @[bmarwell](https://twitter.com/bmarwell/status/1194872808435847168)

"Cloudflare is the largest mitm attack on TLS as we know it, why is this not discussed more?" by @[adrians_reality](https://twitter.com/adrians_reality/status/1187400617402208258)

"Here's what I have, was going to comment on how Cloudflare's SSH services are an MITM on SSH." by @[dxgl_org](https://twitter.com/dxgl_org/status/1187775624322191360)

""We have blocked you from visiting a webpage so contact the webpage owner to resolve the issue".  Ummm…. how do you contact the webpage owner when you have blocked everyone in Singapore from seeing the webpage's "contact us"?" by @[MichaelSmithSG](https://twitter.com/MichaelSmithSG/status/1194515123903164416)

"hi, what's wrong with ASN 12714 that you're blocking on Cloudflare?" by @[cjmaxik](https://twitter.com/cjmaxik/status/1193842746605408256)

"I've tried Cloudflare, but they somehow blocked one of my bank sites, so I went with Google DNS instead." by @[nanianmichaels](https://twitter.com/nanianmichaels/status/1194688408985227264)

"Cloudflare attempted to CENSOR this negative review on Trustpilot." by @[phyzonloop](https://twitter.com/phyzonloop/status/1194458264026660864)

"Ukrainian extremist website Myrotvorets still operates protected by Cloudflare. It contains doxxed info on thousands of people disagreeable to Ukrainian extremists, including #US journalists who visited Donbas. Several persons, who were on this portal, were killed." by @[RusEmbUSA](https://twitter.com/RusEmbUSA/status/1187363092793040901)

"Also, can anyone from UKLabour  or Cloudflare explain why I haven't been able to access the Labour Party website for a day?" by @[BernieSnell](https://twitter.com/BernieSnell/status/1194688395164950528)

"You having a bad day ? I am having voip trouble, tried logging in to your customer-zone and getting cloudflare errors.." by @[4t4ri](https://twitter.com/4t4ri/status/1194613025535381505)

"Looks like Cloudflare is broken - 429 error." by @[RHJOfficial](https://twitter.com/RHJOfficial/status/1192398412911652864)

"I've been trying for almost 1,5 hour to order something in the outlet store. Keep getting "Cloudflare error 524" or "503 Service Unavailable"." by @[JWestra11](https://twitter.com/JWestra11/status/1192416998065627138)

"Don't install Cloudflare's WARP "VPN". This is NOT A VPN by their own admission, and is a privacy MINEFIELD. This is a jaw dropping TOS, and I'm only a few paragraphs in." by @[notdan](https://twitter.com/notdan/status/1178339685795598336)

"Did you know a foreign-owned cloud provider has access to online votes on their way to the digital ballot box? When electors in Canada’s Northwest Territories vote online, their ballots pass through Cloudflare servers and are briefly decrypted while in transit." by @[aleksessex](https://twitter.com/aleksessex/status/1176543023636897792)

"As a protection against denial-of-service attacks you can pay cloud providers to act as a kind of friendly man-in-the-middle. But protection comes in exchange for a high degree of trust: they need access to application-layer data to do things like inject fingerprinting JavaScript" by @[aleksessex](https://twitter.com/aleksessex/status/1176543026853941248)

"That means they have privileged access to see and change your vote. The legitimacy of the election relies on the assumption they won’t. They say the won’t, and so far we’ve seen nothing to contradict that. But how would you find out if they did? How do you know your counted?" by @[aleksessex](https://twitter.com/aleksessex/status/1176543027936018435)

"Consumers simply do not stand a chance in hell these days understanding the technology surrounding them." by @[notdan](https://twitter.com/notdan/status/1178384818352066563)

"can’t checkout online, tried cards from three different banks and always get a cloudflare error after card verification. Any idea?" by @[rossburton](https://twitter.com/rossburton/status/1192808969250717696)

"Hey  Pinterest, you're DNS is down Origin DNS error" by @[DemosthenesPol](https://twitter.com/DemosthenesPol/status/1192831438707548160)

"the one site that has the one show that I like, and is the only site I know of that has it, is giving me the cloudflare bad gateway error " by @[otomequeene](https://twitter.com/otomequeene/status/1193397915844263937)

"I keep getting "Error 1016 Ray ID Origin DNS error" which is a Cloudflare error" by @[7leaguebootdisk](https://twitter.com/7leaguebootdisk/status/1193617137039888385)

"Cloudflare's WARP VPN is now available to everyone. I tried it and it made the internet so slow it was unusable" by @[waddling](https://twitter.com/waddling/status/1177615384616325120)

"Unfortunately I live in Mobile Third World country (Germany) and unfortunately warp seems to slow down or even block my devices instead of improving anything." by @[raupach](https://twitter.com/raupach/status/1189132930486034432)

"I use 1.1.1.1 with warp. But the speed is very slow. I just turned warp off and my speed is about 1000% faster on my iPhone. When will you fix this bug? I payed for unlimited use." by @[tombalfoort](https://twitter.com/tombalfoort/status/1179724521575735296)

"Hi, I'm not sure what you can do, but I've been having issues accessing your site for the last week. I keep getting a 502 error or sometimes "this page is currently offline". Looks like it might be an issue with Cloudflare. I'm using Firefox." by @[MrB0nd07](https://twitter.com/MrB0nd07/status/1193916853325836288)

"Outage today? Getting the "web server reported a gateway time-out" error" by @[knapjack](https://twitter.com/knapjack/status/1194002384273338368)

"Great...now @VPSCheap_NET is using cloudflare to block legitimate customers from accessing the client area. I have no plans to use the Net without the protection of a VPN, so may have to take my virtual servers elsewhere." by @[CFSummers](https://twitter.com/CFSummers/status/1189932851564818432)

"You mean you offload your problems onto legit customers by using cloudflare to block every VPN you can find. Emailing isn't going to help, since I have no intention of surfing w/o one. So will start looking for a company that cares about customer security." by @[CFSummers](https://twitter.com/CFSummers/status/1189941994170269696)

"You actually don't get it - I CAN NOT OPEN A SUPPORT TICKET. I cannot access the client area because you are blocking my VPN's access. Using a VPN IS a part of MY security. Will make sure clients who have servers there & others also know how little you care..." by @[CFSummers](https://twitter.com/CFSummers/status/1189945582699384832)

"is cloudflare happy to allow isps to block websites in pakistan for no reason at all. the whole point of using cloudflare dns was to access blocked websites and since u partnered with ptcl in pakistan the blocking has started again?? r u even aware of this shit?" by @[untitled413](https://twitter.com/untitled413/status/1192110658353061889)

"Since 22nd Oct I was getting "Sorry, you have been blocked" CF message on DO website. No help from DO." by @[zedfoxus](https://twitter.com/zedfoxus/status/1194124693067812864)

"I really wanted to like it but I found so much content being blocked (Tidal for example) it wasn't usable for me. Do you not have any media streaming issues?" by @[chadl2](https://twitter.com/chadl2/status/1193432188923961345)

"I recommend you stop 'just blocking' people for the hell of it.  You've blocked my IP for no reason.  You do realize that these IPs are handed out by the ISP all day long and change often, or are you new to IT?" by @[random_interrup](https://twitter.com/random_interrup/status/1191785034547650561)

"#clownflare is #cancer on the Web. Refusing to run #proprietarysoftware in #javascript on your own computer? BLOCKED!" by @[schestowitz](https://twitter.com/schestowitz/status/1206873758868353026)

"If someone finds themselves wrongly blocked, they can't go to Cloudflare to complain because Cloudflare has blocked them from that site as well.  So every day they sentence thousands of people to Internet purgatory because they got randomly handed an  IP address they blocked." by @[random_interrup](https://twitter.com/random_interrup/status/1191798471914852352)

"Y'all need to dump Cloudflare .  I'm blocked from your site without cause.  ISPs randomly hand out IPs all day and I've inherited one they've blocked.  This garbage directly impacts your business and shows how poor Cloudflare design is." by @[random_interrup](https://twitter.com/random_interrup/status/1191798470585323520)

"I cycle the IP and now I'm in.  If I were a malicous actor I could do the same.  Some security feature.  Now some other guy can be blocked." by @[random_interrup](https://twitter.com/random_interrup/status/1191800765490368513)

"I got some weird “redirecting, checking my browser” message - I thought it was on my end." by @[Cindy_M_McCraw](https://twitter.com/Cindy_M_McCraw/status/1193246807435948032)

"The answer appears to be Cloudflare: the CAPTCHA-wall was theirs, and I'd recently looked at an unrelated site they host that was under their DDoS protection, so I bet that's why I was on the naughty list." by @[xlerb](https://twitter.com/xlerb/status/1189530664606953473)

"CloudFlare is currently f’ed up and I can’t log into my account: the login screen sends me to a captcha which sends me back to a login screen, repeat forever." by @[kstrauser](https://twitter.com/kstrauser/status/1191082449511440384)

"I wonder if Google is funding Cloudflare in exchange for them to put their CAPTCHA on as many users as possible. Would make total sense google need users to train machine learning algorithms. Cloudflare can market it as a service while deceiving webmasters " by @[catgirlsec](https://twitter.com/catgirlsec/status/1192161900810973184)

"Furaffinity Cloudflare protection is so good that even artists can't upload anything" by @[PaigeScribe](https://twitter.com/PaigeScribe/status/1194042797680054272)

"That CF would be a great way to centralize surveillance has been some people’s suspicion for some years now. So I guess that’s confirmed." by @[fugueish](https://twitter.com/fugueish/status/1158093425008230400)

"Hi, we can't log in this morning..." by @[isaacgrover](https://twitter.com/isaacgrover/status/1192430529032810497)

"Your site just went down when I was mid-course, whats the problem? Something with Cloudflare..." by @[Best__Lux](https://twitter.com/Best__Lux/status/1164673967552004096)

"looks like cloudflare are blocking requests. Will need a cool off period before it starts accepting again." by @[TheCryptarch](https://twitter.com/TheCryptarch/status/1164256030039035904)

"Yeah and working with media and websites it's usually a block from cloudflare :D" by @[TheCryptarch](https://twitter.com/TheCryptarch/status/1164255513913286656)

"AWSとCloudFlareやられたらとりあえず発狂する人たち多そう" by @[SHINOHARATTT](https://twitter.com/SHINOHARATTT/status/1164760368473788416)

"AWSやGCPは死んでもなんとかなるけどCloudflareが死んだらサービスが全滅する" by @[LaLN_](https://twitter.com/LaLN_/status/1164763045026918400)

"AWSってこの前も死んでなかったっけって思ったけどcloudflareだった" by @[familiar_ak](https://twitter.com/familiar_ak/status/1164772894288699398)

"I'm getting "500 Internal Server Error cloudflare-nginx" on multiple cloudflare accounts which started at the same time. Any eta?" by @[uxjw](https://twitter.com/uxjw/status/1164264754501697536)

"this issue common? Tried both yesterday and today. EU server maybe? This error has automatically been logged and we are aware of your plight. HTTP Client Error; 500 Internal Server Error ::CLOUDFLARE_ERROR_500S_BOX::" by @[lomakin9](https://twitter.com/lomakin9/status/1164272323400294400)

"Getting a coinspice is down error from cloudflare." by @[Echt_Kain_niaK](https://twitter.com/Echt_Kain_niaK/status/1164555779711688704)

"I'm Getting Error 522. Says im connected to cloudflare(ashburn) but the host worldanvil is erroring." by @[Johntheblack78](https://twitter.com/Johntheblack78/status/1164598224134516737)

"Hey mosh, is your website down? Getting a 1014 error code from Cloudflare." by @[k_pacheco10](https://twitter.com/k_pacheco10/status/1164673639666479105)

"they could probably put into place to know each unique user when going through cloudflare." by @[MrJoshuaPack](https://twitter.com/MrJoshuaPack/status/1113105709221273602)

"eastdakota May I ask why Iranian can't login into Cloudflare anymore? Cloudflare CloudflareHelp" by @[iHMahmoodi](https://twitter.com/iHMahmoodi/status/1163739507285471232)

"I'm not trying to hack you so why are you making Cloudflare sniff my browser?" by @[dxgl_org](https://twitter.com/dxgl_org/status/1164573012294868994)

"For several weeks I am not able to access your site anymore. I always get an error message from cloudflare..." by @[SirJoan](https://twitter.com/SirJoan/status/1164517618180665345)

"Anyone else suddenly experiencing 500 errors from #Cloudflare proxied sites?" by @[warkior](https://twitter.com/warkior/status/1164262039570677760)

"Ah, fair point on getting locked-in with Cloudflare services" by @[pedrolamas](https://twitter.com/pedrolamas/status/1160885585399226368)

"Don't want your traffic going through cloudflare? Download an extension that detects cloudflare use and just don't use those sites." by @[matthkilgore](https://twitter.com/matthkilgore/status/1161216956793442304)

"Don't use cloudflare" by @[ghoul_two](https://twitter.com/ghoul_two/status/1163505950038396928)

"I'll add that I use the darknet for privacy reasons and the fact I don't want to support large corporations (like CloudFlare) on the clearnet. Are either of these illegal?" by @[genericspider](https://twitter.com/genericspider/status/1164021077544247301)

"Don't they use cloudflare servers? No fucking wonder.. Ugh.." by @[Rabbit_Raddish](https://twitter.com/Rabbit_Raddish/status/1164027199793651718)

"Failed to get through the CAPTCHA that Cloudflare's rate-limiting puts in place using a SOCKS proxy into the server" by @[hugo__df](https://twitter.com/hugo__df/status/1060489364034199552)

"God dammit Cloudflare, stop thinking I'm a bot. it's tiring to fill all those recaptchas." by @[MathiasSM](https://twitter.com/MathiasSM/status/1164326427841486850)

"the primary reason i have probably 200 tabs open on my phone (ff stops counting at 100) is that fucking cloudflare keeps cycling me through captcha after captcha and i really, really want to punch through a wall as a result. also its generally all enraging anyway" by @[coffeeshark89](https://twitter.com/coffeeshark89/status/1161372200898838529)

"Hi do you guys know how to make Cloudflare shit stop? We don't have an account but someone is creating accounts with our email domain. I tried a password rest to lock them out but they keep coming." by @[LinLorienelen](https://twitter.com/LinLorienelen/status/1163491558831734784)

"Cloudflare you are drunk! Stop harassing me for using ProtonVPN!" by @[TestaErrico](https://twitter.com/TestaErrico/status/1163395227794771968)

"You getting this too? 500 Internal Server Error" by @[shoden](https://twitter.com/shoden/status/1164261897547501574)

"They block loads of countries outside West for spam traffic. We had that issue constantly from them on our site." by @[stacyherbert](https://twitter.com/stacyherbert/status/1161745226739982342)

"network is being blocked by CloudFlare that’s supporting several SG Govt websites. Called your tech support to simply refresh my IP, and they can’t do that. It’s a simple request!" by @[vincent_tky](https://twitter.com/vincent_tky/status/1163452333331632128)

"Here's one of the least fun problems that I've ever had to investigate: Confluence repeatedly refusing to save changes to a page, claiming that my session had expired, and eventually ending up giving me a Cloudflare "you have been blocked" message. The issue...?" by @[neilstudd](https://twitter.com/neilstudd/status/1163399956746706951)

"Access denied Common cause: A client or browser is blocked by a Cloudflare customer’s Firewall Rules." by @[adspedia](https://twitter.com/adspedia/status/1161297256273907713)

"getuploader.jp 、VPN使ってもCloudFlareのヤツでてきてハゲそう" by @[key_yan0424](https://twitter.com/key_yan0424/status/1163770740006477824)

"De quién fue la "genial" idea de usar esa basura de Cloudflare... Su sitio es inutilizable sin JS, Opera Mini, UC browser Mini, proxy, VPN... Y en caso de ataque no les servirá para nada, usuarios verán la segunda pantalla" by @[monotemoso](https://twitter.com/monotemoso/status/1163581963690811392)

"I legit can't do anything on the page also, i cant log in fromt he page, i cant download it. keeps giving me "Cloudflare Error 1015"" by @[Nickalusss](https://twitter.com/Nickalusss/status/1163943950933094400)

"having trouble linking my Bungie account to Steam. It keeps giving me a Cloudflare error." by @[tophermcpartlin](https://twitter.com/tophermcpartlin/status/1163938429463404545)

"we had downtime for about 5 minutes on our London located website that goes through CloudFlare. No error message from CloudFlare - was there an issue 2 hours ago?" by @[PeterFi24402592](https://twitter.com/PeterFi24402592/status/1163894227216060417)

"Guys, You're down. No connection can be made anymore. Connection fails with error 522 on cloudflare. Is that the consequence of your giuthub cancellation? Millions of WEB sites (like ours) are impacted as they use the fontlibrary and loading times are terrible. Please fix ASAP!" by @[HeavenOfSound4U](https://twitter.com/HeavenOfSound4U/status/1163886188614184961)

"I am getting a Cloudflare Error 500 when trying to hit the Link button for Steam. Help!" by @[josephjaramillo](https://twitter.com/josephjaramillo/status/1163869290086330370)

"Looks like Cloudflare strikes again! Getting site error instead of your article." by @[QanonQurator](https://twitter.com/QanonQurator/status/1163125567513743361)

"Fucked if I know. I was trying to put up the latest column for my patrons, but I'm getting a Cloudflare "gateway timeout" error." by @[DrNerdLove](https://twitter.com/DrNerdLove/status/1163200154792615940)

"#iTWire still blocked by a cancer called #cloudflare Cannot even access the site without proprietary #javascript Stop using CF, people! Stop adding it to sites please!" by @[schestowitz](https://twitter.com/schestowitz/status/1162903811654074369)

"This week #cloudflare does a very fine job proving everything I ever said about it/them (their staff blocked me over it!!) to be TRUE! #surveillance #censorship #internetKopp" by @[schestowitz](https://twitter.com/schestowitz/status/1158746077870395392)

"Laughable, and I'll advise clients and other decent humans away from your services." by @[CalawayTandC](https://twitter.com/CalawayTandC/status/1161306459503771648)

"Cloudflare is having issues #cloudflare is getting too many requests. #websiteproblem" by @[subhaghealth](https://twitter.com/subhaghealth)

""The #uk #PirateParty feed is blocked by Cloudflare," a friend tells me. Surely they should know better. #Cloudflare needs to be avoided. " by @[schestowitz](https://twitter.com/schestowitz/status/1161447079530643456)

"This is really annoying when Cloudflare blocks WordPress REST API PUT requests for #Gutenberg editor (WP0025B rule blocks). You can't save, update or publish your blog post. #block #wordpress #cloudflare #fix #WP0025B" by @[Eftakhairul](https://twitter.com/Eftakhairul/status/1041943169863221248)

"cloudflare unable to access my sites from China unless using VPN. Have you been blocked by the GFW?" by @[FrasSmith](https://twitter.com/FrasSmith/status/57782717152100352)

"I don't want this from #Cloudflare, it is risking being blocked by China. Not just the VPN, but all Cloudflare protected sites." by @[EvolvingViews](https://twitter.com/EvolvingViews/status/1112908186044915712)

"Explain to me how this isn't a MITM attack & proves SSL is busted. CloudFlare promises SSL security" by @[TechHelp](https://twitter.com/TechHelp/status/512953699246014464)

"Bitcointalk.org compromised again in a MiTM attack, CloudFlare used to generate fraudulent SSL..." by @[TechL0G](https://twitter.com/TechL0G/status/407638425433436160)

"1.1.1.1 is not the solution - it is just a commercial MITM attack which any proper security design would have to mitigate." by @[Engbrg](https://twitter.com/Engbrg/status/980691434943606784)

"I firmly believe Cloudflare in the most dangerous thing that happened to the internet this decade. Facebook abuses the data you voluntarily give them; but Cloudflare is a full on MITM attack." by @[peter_szilagyi](https://twitter.com/peter_szilagyi/status/1013786813717196802)

"CloudFlare blocked access to sngpl.ma from Williams (open public wifi) because of suspicious activity. Good thing I have a VPN." by @[dj3vande](https://twitter.com/dj3vande/status/10389334490349568)

"Can't Access Your Favorite Site? Cloudflare Has Been Experiencing Major Outages Almost Everywhere" by @[K4Kats](https://twitter.com/K4Kats/status/1146065947611951105)

"Seems Cloudflare is having ssl issues in the Boston area. If you can’t access my sites please just drive outside of Boston and that should fix it" by @[wesbos](https://twitter.com/wesbos/status/939288672188403712)

"cloudflare just fucking died, can't access discord or a lotta sites bruh" by @[LucasIsYoshi](https://twitter.com/LucasIsYoshi/status/1146053001758552064)

"Yeah, just noticed it’s a cloudflare issue and not discord, as my own sites running on cloudflare aren’t loading - 502 bad gateway." by @[KieranCairns](https://twitter.com/KieranCairns/status/1146053190862946305)

"god fucking damnit, another cloudflare shitshow. the first one was last week and verizon's fault, but this is still infuriating. I can't access most of the websites I usually visit" by @[elli_psis](https://twitter.com/elli_psis/status/1146055591091200000)

"Is CloudFlare down? I can’t even visit it’s website :( Sadly, my server relies on CloudFlare’s services." by @[rickyipcw](https://twitter.com/rickyipcw/status/401360781712687104)

"I can't visit CloudFlare "protected" sites with cookies disabled (is my default browser configuration). There aren't less intrusive methods to "protect" sites?" by @[lito_ordes](https://twitter.com/lito_ordes/status/1143822332894560256)

"I can't visit www.army.mil and Archive Today with Cloudflare DNS It returns zero results when DNS querying for Archive Today or any domains under *.mil Google DNS works though for some reason" by @[wongmjane](https://twitter.com/wongmjane/status/1102446734993551360)

"But the site works fine through Google DNS" by @[wongmjane](https://twitter.com/wongmjane/status/1102448803150389248)

"Let's harass Tor users, MITM all our customers, and call ourselves a "security" company." by @[taoeffect](https://twitter.com/taoeffect/status/716135815366086656)

"The number one MITM just got bigger: "CloudFlare is used by 73.9% of all the websites whose reverse proxy service we know. This is 6.8% of all websites."" by @[modrobert](https://twitter.com/modrobert/status/997007129641996289)

"too bad there's a cloudflare mitm that targets tor users with captchas" by @[femmetasm](https://twitter.com/femmetasm/status/816708201869623296)

"I bet Cloudlfare (Cloudbleed) triggers most of those captchas. If you really want to know how much the web depends on this MITM service, try using Tor for a while. BTW: Are the webmasters using Cloudflare fully aware what they are doing to their visitors?" by @[modrobert](https://twitter.com/modrobert/status/940146857564655616)

"I registered an account last night and was sent an activation but the activate link (or any request to account.tfl.gov.uk ) produces a Cloudflare error "This website is using a security service to protect itself from online attacks. Ray ID"" by @[TouchingVirus](https://twitter.com/TouchingVirus/status/1160123539678257152)

"Turns out Cloudflare's https redirection is messing with http-01 challenge. Always use nginx's own https redirection." by @[6r33z3](https://twitter.com/6r33z3/status/1159482334980268033)

"I keep getting a Cloudflare error screen when attempting to load" by @[dtjohnso](https://twitter.com/dtjohnso/status/1160209031891431425)

"is fa down again? I'm getting a gateway error from cloudflare." by @[JayriAvieock](https://twitter.com/JayriAvieock/status/1160267801296351234)

"It’s script kiddies, human error, or just a Cloudflare outage." by @[notgrubles](https://twitter.com/notgrubles/status/1160574312442675200)

"Maybe Cloudflare should shut off business with facebook for allowing this manifesto being posted. Naw... cloudflare would never cut off one of the hands that feed them by applying equal censorship. #CloudflareHypocrisy" by @[TheCypressGang](https://twitter.com/TheCypressGang/status/1160292182064082944)

"Cloudflareみたいな会社が、偉い人間の感情でこういうことするの本当に良くないから(止|辞)めてね" by @[sekai67](https://twitter.com/sekai67/status/1160385439154331648)

"I'm getting 502 Cloudflare Issues. Is there any updates on how long this is going to take?" by @[Shadowclouddr](https://twitter.com/Shadowclouddr/status/1160328555059916802)

"Sorry to be a pest, but Cloudflare says the host for the site isn't responding!" by @[HannahCaruso3](https://twitter.com/HannahCaruso3/status/1160326868807143424)

"Just letting you know you've issued a certificate to a crypto scam site" by @[SickBeard_Log](https://twitter.com/SickBeard_Log/status/1160312871164628993)

"Cloudflare's CDN isnt a DNS smartass. Cloudflare's only mission is to MITM the whole internet. " by @[6JY6ZQVYS4p7YrL](https://twitter.com/6JY6ZQVYS4p7YrL/status/1158307042924408832)

"While you try and MITM the whole fucking internet." by @[6JY6ZQVYS4p7YrL](https://twitter.com/6JY6ZQVYS4p7YrL/status/1156332140713402368)

"Cloudflare acts as a MITM. In other words, the client connects via HTTPS to their servers, not to the actual website, and then they connect to the actual server. They can read all traffic since encryption is "terminated" at their node." by @[OtomaiShirona](https://twitter.com/OtomaiShirona/status/1159033822903787525)

"speaking of crapflare, yet another one... the 2nd one I've seen since this prior tweet. Maybe it is me... but in the end vendors lose out and I move on..." by @[brettPowerline](https://twitter.com/brettPowerline/status/1159219980921126914)

"would you want your kids to be victims on your own infrastructure? #Hypocrisy #cybercrime" by @[phyzonloop](https://twitter.com/phyzonloop/status/1160000377418387457)

"You blocked 5ch.net , which includes Japan's strongest liberal community /poverty/ (kenmo) board. You think you blocked Japan's alt-right website, but it's not actually quite that simple." by @[joinus_yaruo](https://twitter.com/joinus_yaruo/status/1159678887275614209)

"Having trouble with the haveibeenpwned API. Using v3 with an API key and sending requests under the rate limit but still getting blocked by Cloudflare. Anyone else having similar problems?" by @[averagesecguy](https://twitter.com/averagesecguy/status/1159491954503086080)

" can't login to Patreon. When I try to, it tells me to verify by clicking the link sent to my mail. After I click, I see " 403 Forbidden " and " cloudflare". This must have something to do with you. I am not attacking the site obviously, why am I blocked?" by @[pleionea](https://twitter.com/pleionea/status/1159955321634402305)

"Cloudflare blocking access to the site from France. So much for net neutrality..." by @[jfenal](https://twitter.com/jfenal/status/1159704817675177984)

"Cloudflareの1.1.1.1ってpoliticalな云々でblockingするとこだったっけ？と" by @[hornet_mk2](https://twitter.com/hornet_mk2/status/1159799146800242691)

"率直に言って Cloudflare DNS が特定のドメインの名前解決を阻害し続けるならば、今後それを使うことはなくなる。" by @[soltia48](https://twitter.com/soltia48/status/1159657171266617344)

"Hey remember when you dickheads forwarded my name, email address, and IP number to 8chan after we sent you a report about the child pornography ring they were hosting?" by @[FoldableHuman](https://twitter.com/FoldableHuman/status/1158234905395904512)

"Reading this thread makes me think it's straight up dangerous to have any sort of contact with Cloudflare if you have any sort of morals or scruples. I'd say it's best to report websites hosted by them to the authorities." by @[WenSchw](https://twitter.com/WenSchw/status/1159957822626357248)

"Is cloudflare trying to shut you guys down too? " by @[youngandaspire](https://twitter.com/youngandaspire/status/1159958784569049089)

"CloudflareのDNSだと「5ちゃんねる」に接続できない？　ネットで話題に  … "Cloudflareは今回、8chanの現運営者が一時的に管理していた5ちゃんねるに対しても措置を講じたとみられる"" by @[catnap707](https://twitter.com/catnap707/status/1159963775178436608)

"People did not know cloudflare sucks? You ever try to browse websites from a Tor connection to a cloudflare hosted site? (Granted they eventually stopped putting people in captcha jail)" by @[Pic0o](https://twitter.com/Pic0o/status/1158531041033936897)

"Hm.. Pakistan govt has signed up enterprise plan with Cloudflare to block it's army website for India IP addresses. " by @[njnrn](https://twitter.com/njnrn/status/1158445893948866560)

"Hey, i have some trouble with your website Cloudflare say i have no access...." by @[Poke7z](https://twitter.com/Poke7z/status/1159209207511625729)

"I have a bad gateway error (502) with the Cloudflare server being in Warsaw." by @[kbartek05](https://twitter.com/kbartek05/status/1159174662955380736)

"Hey there #errbot devs. It appears that errbot  is throwing a Cloudflare error: Error 1014 CNAME Cross-User Banned" by @[n8felton](https://twitter.com/n8felton/status/1158087249126469632)

"Dear FJ, the Cloudflare error in white page means that the owner of the site has blocked Indian connections from accessing it." by @[Aamer_Sha](https://twitter.com/Aamer_Sha/status/1158083077425512449)

"You alright? For a second there was a cloudflare error. " by @[RainbowKappaMC](https://twitter.com/RainbowKappaMC/status/1157259941461266432)

"is your website ok? getting a Error 522 via cloudflare" by @[CatherineWeird](https://twitter.com/CatherineWeird/status/1157006526781038592)

"Getting a 521 error on cloudflare for the past 30 minutes. fyi." by @[taffy_nay](https://twitter.com/taffy_nay/status/1157004487854370816)

"i placed an Order, and after paying with Paypal i was redirected to a Cloudflare Error-Page. My Paypal has the debit but there is no E-Mail Order Confirmation or Order No at your Website." by @[Dryteak](https://twitter.com/Dryteak/status/1156957781892706307)

"Hello i have a Problem with the Website gt-mp.net  i can go on the Site it gives me an Cloudflare Error! I just want to Download the Client is there way that i get a directly Download Link?" by @[imxylox](https://twitter.com/imxylox/status/1156942014065389569)

"What's up with Cloudflare? 1. Error while attempting to log in (credentials are correct -- cookies wiped) 2. When logging in using incognito, it works but redirects to.. the path to a SVG logo that isn't there? 3. Many errors on the main page after clearing out all site data " by @[CoolApps45](https://twitter.com/CoolApps45/status/1156712382099591173)

"In truth Cloudflare's commitment to freedom on the 'net is clearly not true given Cloudflare's blocking of anyone who tries to connect to one of their client sites from a #tor exit node. It's not about freedom. I doubt it's about any principles at all." by @[DeborahPeasley](https://twitter.com/DeborahPeasley/status/1158124739933745152)

"If anything, Cloudflare is demonstrably against free access to information. Their entire business model comes down to locking off the Internet to all but the biggest tech giants (Googlebot), and denying any other Mom&Pop co’s in the name of “DDoS protection.”" by @[petebray](https://twitter.com/petebray/status/1158067340388401152)

"I remember cloudflare's "safety lead" blocking a bunch of people for pointing out that their abuse process is (was) extremely unsafe." by @[sschinke](https://twitter.com/sschinke/status/1158177997960040448)

"Well cloudflare is one of the worst companies out there. Their entire business model is made up of restricting access to content (ie machine learning to only let Google and other big co bots through, but denying all small players)." by @[petebray](https://twitter.com/petebray/status/1157874402588889088)

"Finding it difficult to access petition. Error 502. No idea if deliberate or just technical." by @[NDWissner](https://twitter.com/NDWissner/status/1157214234209656832)

"/r/privacy: "...cloudflare does tls termination, directly having access to all the data in clear." " by @[cryptoretreat](https://twitter.com/cryptoretreat/status/1157143398630846464)

"Why is my IP blocked please? Error 1006" by @[sea_bouy](https://twitter.com/sea_bouy/status/1157617308522749953)

"He’ll use a bullshit “free speech” argument that doesn’t apply to his business and then block you if you push back." by @[slpng_giants](https://twitter.com/slpng_giants/status/1157794700432220161)

"Matthew Prince blocked me after I called that out." by @[theMetz](https://twitter.com/theMetz/status/1157845329082900481)

"Cloudflare blocking sydney-ipsec-1b." by @[sendai](https://twitter.com/sendai/status/1156792041617022983)

"why is my cloudflare down i can't connect to any games" by @[__SeeKerZ__](https://twitter.com/__SeeKerZ__/status/1156710401616297989)

"first was cloudflare now you can't even open more than 3 tabs at once or you get rate limited cmon ESEA" by @[jmc_025](https://twitter.com/jmc_025/status/1156735060210520064)

"Getting a Cloudflare error..." by @[thehotiron](https://twitter.com/thehotiron/status/1155079783711170562)

"I cannot get into the Witcher 3 section to download mods. Getting a cloudflare error. Are you aware?" by @[iSpaceNate](https://twitter.com/iSpaceNate/status/1154810492961153024)

"Día dos... El Cloudflare sigue dando error 1016, han pasado el ticket a los expertos en servidor....seguiremos informando." by @[softwartive](https://twitter.com/softwartive/status/1154769944434892801)

"extremely cool how cloudflare goes out of their way to try and make the web unusable for people who browse through tor" by @[elisohl](https://twitter.com/elisohl/status/1154494785865994240)

"Use #tor for one day and see how many #recaptcha|s you have to pass on sites hosted on cloudflare (so around half the internet?) because of "unusual behaviour from your IP". This is crazy!" by @[JayVii_de](https://twitter.com/JayVii_de/status/1153918724828815362)

"قبلاً دیده بودم که سایتهایی که از cloudflare استفاده میکنند، recaptcha نشون بدن اما اینکه کلاً کاربران tor رو بلاک کنند رو ندیده بودم 
اینم یک جور تحریمه ولی خب دیگه منحصر به ایران نیست " by @[Hamed](https://twitter.com/Hamed/status/1153210455944511488)

"now lm just getting cloudflare errors." by @[Auroken](https://twitter.com/Auroken/status/1155182329494691841)

"Something called Cloudflare keeps coming up to say it isn't available." by @[SACNSNew](https://twitter.com/SACNSNew/status/1154798137258795008)

"Cloudflare Would you please answer this problem. " by @[botrader2019](https://twitter.com/botrader2019/status/1155151942793846784)

"cloudflare is blocking adsense login for websites" by @[joecioffi](https://twitter.com/joecioffi/status/1153459742192349185)

"I'm trying to report a bug on your pro forum but I'm being blocked by your CloudFlare spam filter for (I assume) posting PHP code." by @[amielucha](https://twitter.com/amielucha/status/1152224097893572610)

"It's often a deadend when you hit a CloudFlare site using Tor on mobile (OnionBrowser). This is why we can't have privacy. Don't block Tor!" by @[mrphs](https://twitter.com/mrphs/status/874324472379166726)

"Hi, I've been blocked from the website by cloudflare with a 1020 error? Is there a way to fix this?" by @[isasangtae](https://twitter.com/isasangtae/status/1152409567709450240)

"Hey Cloudflare, your 1111resolver isn't responding to queries from my box at ovh, what's up? I'm in the 192.95.9.0/24 block. I guess I have to use Google for now" by @[dpedu](https://twitter.com/dpedu/status/1151603868574220288)

"Their abuse form is a joke and results in no action." by @[phyzonloop](https://twitter.com/phyzonloop/status/1151831447554134016)

"It's scary to see how many websites are relaying on CloudFlare, and yet they go down pretty much frequently. They control the internet." by @[OctolusNET](https://twitter.com/OctolusNET/status/1146056667424907265)

"Recent #outages from Cloudflare and Google are a stark reminder that the internet is increasingly centralized and easily broken." by @[CurtailInc](https://twitter.com/CurtailInc/status/1151613547283009536)

"Cloudflare is down more often than it's up on Charter / Spectrum. Oddly enough the DNS servers I shared a photo of work just fine & they too are anycast just like cloudflare." by @[IanKoHoman](https://twitter.com/IanKoHoman/status/1151627555524489217)

"why'd you do this Cloudflare?" by @[prith_august](https://twitter.com/prith_august/status/1151117385824215040)

"LMAO it's another fucking cloudflare thing, how. fucking. incompetent can you be??? what the actual fuck?? LOL" by @[literallypoo](https://twitter.com/literallypoo/status/1151286341168242688)

"both their status pages are all green fucking amazing why do people even use cloudflare i get it but at the same time it's so dumb i'd understand it if they were proper about this and 1) properly reported downtime and 2) properly handled user data but they don't" by @[osk______](https://twitter.com/osk______/status/1151287059514048513)

"Ask why Cloudflare keeps on using Google’s reCAPTCHA when they can do their own. " by @[rom](https://twitter.com/rom/status/1151423727629828096)

"Looks like a Cloudflare issue. This is why I don't trust Cloudflare. So many people use it, which puts them into a position of being a gateway to information. You have to go through them to get to your favorite site. The perfect climate for a marxist censorship takeover." by @[Scarasyte](https://twitter.com/Scarasyte/status/1151604200754884609)

"i will sooooo block the sh*t out of TRR if mozilla makes it the default. to hell with whatever high value web properties cloudflare co-hosts it with. the internet is a _distributed_ system. dns is a _distributed_ database (and also autonomous.) FSCK centralization by decree." by @[paulvixie](https://twitter.com/paulvixie/status/1026302589757837312)

"why are you blocking spinquark?" by @[jhthurman](https://twitter.com/jhthurman/status/1150773700842725382)

"did you know you’re being blocked by Cloudflare?" by @[jhthurman](https://twitter.com/jhthurman/status/1150775195503865857)

"Hey Cloudflare why all of a sudden are you challenging Google when they go to spider our sites sitemap_index.xml is google now a suspicious party now." by @[delawarewebhost](https://twitter.com/delawarewebhost/status/1150411571996160002)

"You see, when the secure connection gets to Cloudflare, they do what is called SSL termination -- that means they end the secure connection so they can do a man-in-the-middle (MITM) attack. So any website you enter private information on, that uses Cloudflare, they get that info." by @[cqwww](https://twitter.com/cqwww/status/1146117149297016832)

"List of websites that used to block tor with #Cloudflare and don't seem to block tor anymore #DontBlockTor" by @[shiromarieke](https://twitter.com/shiromarieke/status/724677543832522752)

"This morning. Tell me again how #Cloudflare aren't having issues" by @[PAHarrisonpics](https://twitter.com/PAHarrisonpics/status/1149596711871893504)

"Cloudflare has gone down around the world and vast numbers of websites have gone down with it" by @[PrimeTechSuppor](https://twitter.com/PrimeTechSuppor/status/1146101523119783937)

"Cloudflare was down. Half the Internet was down. IoTeX blockchain wasn't." by @[qevanguo](https://twitter.com/qevanguo/status/1146067288916783104)

"Cloudflare was down causing many others to be down too" by @[jhyu](https://twitter.com/jhyu/status/1149497658718822400)

"If you'd asked me back then how we'd work around issues raised in the paper, I'd have said 'IPv6' and not "in the future we'll all just agree that Cloudflare gets to MitM half the Internet"" by @[arbedout](https://twitter.com/arbedout/status/1146069113023991808)

"調査するとどうやら521エラーはcloudflareが障害を引き起こしているのが、原因だとわかりました。 こちらでは、対処ができないみたいなので早く復旧してくれることの祈ります" by @[yassan_channel](https://twitter.com/yassan_channel/status/1149288589219356677)

"Cloudflareの障害で痛い目に遭ったのでCDNも分散させる" by @[dragon6887](https://twitter.com/dragon6887/status/1148188924478545921)

"Cloudflareが障害起こした後から超安定しなくなった。" by @[InkoHX](https://twitter.com/InkoHX/status/1147426975281082368)

"アメリカのCloudflareという会社がダウンしたようで、画像が送られないという障害が発生したようですね。" by @[hiiro33](https://twitter.com/hiiro33/status/1147408061444718592)

"障害があった いずれもCloudflareを使っていたことがドメイン検索でわかります" by @[midori50000000](https://twitter.com/midori50000000/status/1147155939268501504)

"なんというか、CloudFlareにfacebookにさくら、障害話が毎日続いて楽しい（楽しくない）ことに。" by @[Bredtn_1et](https://twitter.com/Bredtn_1et/status/1146699062264995841)

"Well, looks like Cloudflare is experiencing yet another worldwide outage. This happened precisely as I was sending out a huge mass email to my entire Medfuseio community, so they all ended up getting directed to a website with a 521 error. " by @[iotmpls](https://twitter.com/iotmpls/status/1149013609390989314)

"Cloudflare is returning error code 521 for every request to the IPSW API :(" by @[portuj](https://twitter.com/portuj/status/1148997592128401408)

"Just as a point of information, Cloudflare is giving me a 521, telling me that the gab.ai  isn't responding." by @[FrancisRoyCA](https://twitter.com/FrancisRoyCA/status/1146747891387641858)

"Is it possible to read your articles on patreon/subscribestar so that I wouldn’t have to visit cloudflare and solve endless google captchas?" by @[mezriss](https://twitter.com/mezriss/status/1146689544579964928)

"One of the #Cloudflare outages included Down Detector, which tracks outages, meaning people couldn’t even see if the website they wanted to visit is working properly." by @[mrgeffitas](https://twitter.com/mrgeffitas/status/1146352778547216384)

"Error 1006 is displayed and I can not browse the web I do not understand English because I am Japanese What should I do?" by @[Bliss87817873](https://twitter.com/Bliss87817873/status/1148856109970538496)

"Cloudflare is someting going on? i can't access most of the websites and discord seems not accessible too." by @[Sebastien_Baka](https://twitter.com/Sebastien_Baka/status/1148715450374733825)

"cloudflare sucks, i hope people stop using them." by @[uguuCyuubi](https://twitter.com/uguuCyuubi/status/1146054595262726144)

"Told you not to use this piece of shift." by @[schestowitz](https://twitter.com/schestowitz/status/1146073119897063424)

"Cloudflare outage highlights the internet's fragility" by @[benstricker](https://twitter.com/benstricker/status/1148724389229342721)

"We wanted an internet that works during nuclear wars. Instead we powered all websites by Cloudflare." by @[muneeb](https://twitter.com/muneeb/status/1146075756197482496)

"I suspect it's like why I called them a villain because the way they announced they were going to do it involved a financial partnership with CloudFlare." by @[nyquildotorg](https://twitter.com/nyquildotorg/status/1147206155367661569)

"I'm not sure how Mozilla DOH works, but if they are just teaming up with CloudFlare or Google, how does DOH enhances privacy? Why would CF or Google getting your entire DNS history, be "better" than your ISP getting it?" by @[JeroenJacobs79](https://twitter.com/JeroenJacobs79/status/1147141410791002113)

"No more lobstering for me. I decided to add ips-v4  in my gateway and reject any traffic from/to their networks." by @[empatogen](https://twitter.com/empatogen/status/1146454813137391616)

"Don't panic, don't panic! just Cloudflare going down.... wtf... ok yea let's panic." by @[RiseofLex88](https://twitter.com/RiseofLex88/status/1146052613747572738)

"502! Lots of websites are down duo to Cloudflare is down. Websites like Skillshare, Udemy, Upwork, and many more sites. According to this site, it’s 9.6% of the web is most likely down right now." by @[WittrupMatilde](https://twitter.com/WittrupMatilde/status/1146055654487949312)

"Cloudflare is currently down, including the Cloudflare DoH server that is the default DoH server for Firefox, if you enabled it." by @[PowerDNS_Bert](https://twitter.com/PowerDNS_Bert/status/1146057034430738432)

"Cloudflare is pretty trash, most of the time the only thing that works is the captcha, and thats rarely. Never used it on my own stuff, never will." by @[ObscenityIB](https://twitter.com/ObscenityIB/status/1146161299199410176)

"I just got a captcha starting a search on @crunchbase (Cloudflare name was on the page). It merely got me to press and hold a big button for 1s! Can't replicate for a pic now but both far less tedious and time consuming than this ml bs." by @[RoryDHughes](https://twitter.com/RoryDHughes/status/1148699678785118210)

"Based on Cloudflare? A company that is US based and would allow intelligence agencies with nearly no effort to spy on all users at once? This is no enhancement. Just use a local recursive resolver like unbound if you don't like the blocking of your ISP." by @[RealityAbsorber](https://twitter.com/RealityAbsorber/status/1147877776768294921)

"Getting 'bad gateway' on your sites....unable to access anything, and on a deadline....uhm...." by @[RelianceJobs](https://twitter.com/RelianceJobs/status/1146057910323011585)

"I have confirmed that it is not just me. Others are having some Cloudflare problems today as well." by @[TirsohCartoons](https://twitter.com/TirsohCartoons/status/1146063343582498817)

"Cloudflare is having problems across the board." by @[SesameShibane](https://twitter.com/SesameShibane/status/1146071855545180160)

"Its funny how we take a distributed, resilient, network like the Internet and immediately bolt on a single point of failure." by @[vulgrin](https://twitter.com/vulgrin/status/1146058169782820864)

"wanted to check if cloudflare is just down for me, then realized the down checker website is hosted on cloudflare" by @[soulchildpls](https://twitter.com/soulchildpls/status/1146055235305013250)

"No, that would be a terrible single point of failure." by @[pwaring](https://twitter.com/pwaring/status/1146061396376842243)

"the answer to you cloudflare issue is simple: Stop using it." by @[TanaPyre](https://twitter.com/TanaPyre/status/1146109881738334208)

"If everyone could stop using shit ass Cloudflare so giant sections of the web wouldn’t go down every time they have an issue, that would be great. " by @[rob_neu](https://twitter.com/rob_neu/status/1146108992734879744)

"Where 1 day later, pretty much half of the worlds, widely available and most used communications platforms and devices just stop working." by @[The1stStroke](https://twitter.com/The1stStroke/status/1146472721200635906)

"Is Cloudflare an extortion racket now? I have to enter a captcha to view every single webpage" by @[protocollie](https://twitter.com/protocollie/status/1151114888602894337)

"Going to a website and seeing the cloudflare ddos protection CAPTCHA means you've stumbled upon some good shit" by @[spikhykaran](https://twitter.com/spikhykaran/status/1150749661906391041)

"So recently Patreon moved behind Cloudflare. Now my patreon-feed library doesn't work because the setup Cloudflare has for Patreon requires CAPTCHA before it will let you through, and that fucks up with the feed." by @[daemionfox](https://twitter.com/daemionfox/status/1149090123432255489)

"I think we don't hate the creators of Captcha enough. 15 captchas later I still can't retrieve a single API key" by @[MSF_Jarvis](https://twitter.com/MSF_Jarvis/status/1150074959986749440)

"Why is half the internet down this week? Facebook, WhatsApp, twitter, Cloudflare..." by @[elie2222](https://twitter.com/elie2222/status/1146524774673395713)

"Why is the internet a shit storm right now. Cloudflare, twitch api, facebook, Instagram, and twitter dms have had outages in the last couple days" by @[SterlingZubel](https://twitter.com/SterlingZubel/status/1146521935720808449)

"Cloudflare is dead, that's why nothing works" by @[Kameeleon_](https://twitter.com/Kameeleon_/status/1146516471280996353)

"Do people just not know how to run a server or.....? Why is everyone bunnyhopping off of stuff like Amazon and CloudFlare? You can just but your own equipment and run a server and.....ugh my soul" by @[GeekyFoxy](https://twitter.com/GeekyFoxy/status/1146496719368196098)

"Users were unable to access cryptocurrency service websites as the servers could no longer be accessed due to a technical error on Cloudflare. The problem was confirmed by Cloudflare in its blog, citing that the issue stemmed from bad software." by @[cryptosexp](https://twitter.com/cryptosexp/status/1146537903448891403)

"Well that explained why I couldn’t access some websites today - Cloudflare outage takes down a large portion of the internet " by @[copyranting](https://twitter.com/copyranting/status/1146485524120649728)

"Earlier this week, Ecessa experienced the pain of the #Cloudflare global #outage when we couldn't access some cloud-based apps. It pointed out how dependent we all are on our SaaS providers and their infrastructures." by @[Ecessa](https://twitter.com/Ecessa/status/1146450399098023937)

"Recent outages reveal that many services rely heavily on CloudFlare as their single point of access. How can we prevent CDNs from becoming all-encompassing centralized layers of control over the Web?" by @[jhamberg](https://twitter.com/jhamberg/status/1146371917332537344)

"Not been able to access some of your favourite sities? Today's Internet wobble was caused by Cloudflare glitch" by @[SolsoftGroup](https://twitter.com/SolsoftGroup/status/1146321219668647936)

"Our DNS service CloudFlare went down for 30 minutes so we couldn't access anything." by @[Orangopus](https://twitter.com/Orangopus/status/1146162772855730177)

"Cloudflare glitch affects access to websites" by @[digitaljournal](https://twitter.com/digitaljournal/status/1146134957208801280)

"This morning’s outage demonstrated nicely why having CloudFlare at as your domain registrar is a bad idea. If you want to redirect your sites during a major CF outage, you’re stuck without access to one’s dashboard." by @[drjjw](https://twitter.com/drjjw/status/1146134626592841728)

"Cloudflare is down so Firefox browser users might have a problem resolving some sites. It’s called privacy vs performance trade-off, and today it hits all users at the same time." by @[lukOlejnik](https://twitter.com/lukOlejnik/status/1146059459401138176)

"Typical illustration of how bad the internet is centralized nowadays: Cloudflare is down, most websites & services being disrupted by this." by @[x0rz](https://twitter.com/x0rz/status/1146058998010957826)

"why couldn't this #cloudflare outage affect any of the websites I use for work?? " by @[steffyj17](https://twitter.com/steffyj17/status/1146069270503317504)

"One day AWS and CloudFlare are going to have issues on the same day and we're all just going to go home because nothing will be working" by @[living_syn](https://twitter.com/living_syn/status/1146059384214024195)

"one weird consequence of doing all my personal browsing through tor is that i never notice when cloudflare goes down because those sites are never available for me in the first place" by @[elisohl](https://twitter.com/elisohl/status/1146552703167995906)

"No, I’m not blocking the site cloudflare is down. Close ticket." by @[lnxdork](https://twitter.com/lnxdork/status/1146106818319532038)

"The website blocked me and I need to make a payment. Can you DM me a link or email addy to do that? Or if you need Cloudflare’s ID info... pls share?" by @[dpwac](https://twitter.com/dpwac/status/1146150147069480962)

"OTOH if Firefox insists on using Cloudflare and Cloudflare, as soon as Cloudflare is down/blocked/whatever, I'm screwed." by @[cunnigliu](https://twitter.com/cunnigliu/status/1146114730240684035)

"that's why I don't like Cloudflare. Not only this but also that my IP has been blocked by them in the past thanks to fucking SpamHaus." by @[actual_mishaor](https://twitter.com/actual_mishaor/status/1146065433528639489)

"I was scraping a website and it stopped working... thought I was blocked... nope Cloudflare is down " by @[LoriKarikari](https://twitter.com/LoriKarikari/status/1146059734287437826)

"hey folks just FYI a cloudflare outage has blocked many people from reaching Discord." by @[K5SQL](https://twitter.com/K5SQL/status/1146058488553959425)

"why are alot of websites blocked by you I cant do anymore can you plz stop blocking these websites cuz it's really not fair to the people who made them or the people who love to get on the website!!" by @[nopestopplayin](https://twitter.com/nopestopplayin/status/1146056740938477572)

"#Cloudflare parece tener problemas (caído) y se llevó media internet entre ello (exchanges sufriendo, wallets, block explorers, etc)." by @[gerardoetaboada](https://twitter.com/gerardoetaboada/status/1146093658795651073)

"Cloudflare parece tener problemas (caído) y se llevó media internet con ella (exchanges sufriendo, wallets, block explorers). " by @[BtcAndres](https://twitter.com/BtcAndres/status/1146065400586620929)

"Could you make it so that when Cloudflare goes down, discord (overlay) doesn't block half my screen again? After restarting I still had text on my screen. " by @[GiveMeTheCheese](https://twitter.com/GiveMeTheCheese/status/1146062709596704769)

"More issues with Cloudflare and lots of high profile sites go offline. Stop using this stupid MITM-service and help out and re-decentralize the web instead. I might as well block their networks completely and stop using services that’s behind Cloudflare. " by @[empatogen](https://twitter.com/empatogen/status/1146059519237087233)

"Cloudflare is down and large swathes of the web are unreachable." by @[moftasa](https://twitter.com/moftasa/status/1146059475771510786)

"Looks like many crypto websites are down because their CDN/DDoS protection provider Cloudflare is down (exchanges, block explorers, etc) Perfect demonstration of single point of failures" by @[JulienThevenard](https://twitter.com/JulienThevenard/status/1146056362553561089)

"please, please, PLEASE stop using #captcha, it's a flaming PoS - I can't even log into #Cloudflare today because I use firefox and block dodgy ads, 24 failed attempts at your captcha! FFS" by @[s_mcleod](https://twitter.com/s_mcleod/status/1145634527227736064)

"OH, he did not. Cloudflare cloud services have had some issues (like being down) resulting in all that erratic behavior. It's been global." by @[EngOnDemand](https://twitter.com/EngOnDemand/status/1146563784649170944)

"With more and more outages(#Cloudflare #googledown), #404 page is becoming important." by @[sendilkumarn](https://twitter.com/sendilkumarn/status/1146563151716069387)

"Hahaha dificil de saber Cloudflare es un servicio taaan extenso y con tantas réplicas de data, que mejor no ilusionarse hehehe" by @[williamromero](https://twitter.com/williamromero/status/1146561718845345792)

"With the Cloudflare fail yesterday which resulted in Coinbase and Kraken being down, another centralized point of failure became obvious" by @[crypto_reddit](https://twitter.com/crypto_reddit/status/1146560931511590919)

"Did you site crash and you use Cloudflare? A bad software deployment took down websites on Tuesday" by @[esellercafe](https://twitter.com/esellercafe/status/1146560002209062925)

"Yeah many websites are not opening and showing the same for me too" by @[AbhradipAchary6](https://twitter.com/AbhradipAchary6/status/1146081664222633984)

"DownDetector down as another Cloudflare outage affects services across the web" by @[verge](https://twitter.com/verge/status/1146080932321005569)

"Hope CloudFlare will lose a bit of influence worldwide." by @[Tsigorf](https://twitter.com/Tsigorf/status/1146063328810192897)

"I still get the Captcha but when enter the code after solving it, there is no "submit" button. Cloudflare . . . is not my friend." by @[DeborahPeasley](https://twitter.com/DeborahPeasley/status/1138905377112776704)

"Entire internet has a case of the Mondays. Cloudflare took a shit, people are harassing a 10 year old... Gonna just go back home and go to bed" by @[axamili7](https://twitter.com/axamili7/status/1143187004273901570)

"Cloudflare is Having an Outage Affecting Sites Everywhere" by @[BleepinComputer](https://twitter.com/BleepinComputer/status/1143133510103961600)

"Could be that you've been given a bad IP address from your ISP and cloudflare is blocking you." by @[JimSwiftDC](https://twitter.com/JimSwiftDC/status/1143110180789477377)

"we saw no problems ourselves but some people were affected by a Cloudflare issue depending on the cloudflare node you were connecting to." by @[romancart](https://twitter.com/romancart/status/1143456148068163584)

"Cloudflare fucking died today dude, can't wait for it too happen again" by @[JohnnSJon](https://twitter.com/JohnnSJon/status/1143172597292494848)

"cloudflare is a fuckin piece of sHIT AHHHHH" by @[actualaalice](https://twitter.com/actualaalice/status/1143139718189735936)

"So hum... Cloudflare is taking a massive shit atm, Discord is down, SLOBS is down..." by @[EDL6six6](https://twitter.com/EDL6six6/status/1143132819943763968)

"For many users, web developers etc, Cloudflare is down! Reports from all over the world, especially Europe!" by @[GadgTecs](https://twitter.com/GadgTecs/status/1143116061333688326)

"#cloudflare right now - When you have all your websites on Cloudflare and it goes offline" by @[Arm4x](https://twitter.com/Arm4x/status/1143115059947872257)

"Pingdom is down #cloudflare" by @[Zabavnov](https://twitter.com/Zabavnov/status/1143125171215261698)

"Definition of irony? :) At least my site uptime stats won't be taking a hit... #Cloudflare #Outage" by @[DataVenia](https://twitter.com/DataVenia/status/1143115885592350721)

"#CloudFlare outage that impacted 1.6 million websites yesterday." by @[EpikDotCom](https://twitter.com/EpikDotCom/status/1143574073462722560)

"Well since cloudflare and stuff got taken down, my isp went down massive hit over UK. And man my phones and ps4 was refusing connect and i fucking yelling my mum face say talktalk done some shit my connection and more and now this anger i have going to far now with this :/" by @[ChisdealHD](https://twitter.com/ChisdealHD/status/1143189134170841090)

"Its not just discord. Cloudflare went to shit and its affecting many websites." by @[DigiKoshi](https://twitter.com/DigiKoshi/status/1143149977822289920)

"me: .. wait why do i care discord is dead" by @[BeepSterr](https://twitter.com/BeepSterr/status/1143133485504389121)

"it's cloudflare fucking shit up again, yay" by @[brxndos](https://twitter.com/brxndos/status/1143139619690577922)

"Cloudflare is down and it's affecting everything. Can't work on my crypto project shit because discord is down so can't communicate with team. Very frustrating indeed." by @[realjunsonchan](https://twitter.com/realjunsonchan/status/1143151053103345664)

"how let you know i'm human ? where the CAPTCHA feature" by @[shibwei](https://twitter.com/shibwei/status/1141200007455838208)

"Hey Cloudflare I don't mind going through a human check once in a while, but having me do it EVERY time I go to a new website or reload one is ridiculous. This is bad UX." by @[JohnnyFDK](https://twitter.com/JohnnyFDK/status/1138373924079767554)

"Aha, ik kreeg deze morgen bij doorklik van google naar velt een cloudflare captcha" by @[Beatever](https://twitter.com/Beatever/status/1143119544384479232)

"Trying to register a U2F token is broken as Cloudflare are serving a CAPTCHA on your API." by @[R1CH_TL](https://twitter.com/R1CH_TL/status/1136251124602888192)

"Hi, I programmatically access to API from Azure. Last month, it was successful, but from this month, it requires CAPTCHA and blocks non-human access. Did you change policy?" by @[hiroto_nihilist](https://twitter.com/hiroto_nihilist/status/1137227605348720640)

"we are trying to call deliveroo test api but we get an error from 'CloudFlare' - Please complete the security check to access -- can u please look into this issue .. this is now stopping sending a signal to 'sync_status' .. appreciate your help" by @[aria_zanganeh](https://twitter.com/aria_zanganeh/status/1143275297501900801)

"Yesterday there was cloudflare failure that prevented access to tonnes of different services (like Discord, Crunchyroll...)." by @[KevinLi43937741](https://twitter.com/KevinLi43937741/status/1143389357748834304)

"Mobile and PC users can't access #Discord due to Cloudflare outage" by @[realnewsgeek](https://twitter.com/realnewsgeek/status/1143156457598259201)

"Apologies if you couldn't access our website yesterday for a few hours, some of our customers were affected by the Cloudflare outage" by @[ConstructTeam](https://twitter.com/ConstructTeam/status/1143443595623653376)

"Due to global issues with cloudflare some visitors will not be able to access the site and there might be interference with using the site." by @[SellMyAppNow](https://twitter.com/SellMyAppNow/status/1143134686996246528)

"No it's worldwide - you won't be able to access many websites due to the routing issues with Cloudflare." by @[BitKhoi](https://twitter.com/BitKhoi/status/1143133265836072962)

"#Cloudflare is down - half of the internet simply died" by @[SpottySTC](https://twitter.com/SpottySTC/status/1143132257466511360)

"Good gosh, those Cloudflare CAPTCHA are annoying as **** when using #Tor. My solution is to not use those sites. How are guys dealing with those?" by @[SageHack](https://twitter.com/SageHack/status/1140931557278015489)

"Unfortunately Cloudflare are having issues affecting 50% of their clients." by @[Devstars](https://twitter.com/Devstars/status/1143117388734455808)

"Cloudflare is down in some regions due to a BGP leak." by @[GossiTheDog](https://twitter.com/GossiTheDog/status/1143130969307799553)

"cloudflare, possibly the largest internet/networking company in the world doesn't have any kind of automated visibility on downtimes. does that not concern anyone? half their network is down and their status page says "all systems OK!"" by @[ppy](https://twitter.com/ppy/status/1143108563428003841)

"So Cloudflare is down? Which makes Discord, VRChat, Osu! , Crunchyroll ,ect  PARTY!" by @[NakagawaNatsix](https://twitter.com/NakagawaNatsix/status/1143132937824587776)

"A broader look at the total traffic impacted by the Cloudflare route leak - fifteen percent of traffic. Pretty big chunk." by @[TrisClarkARGuy](https://twitter.com/TrisClarkARGuy/status/1143671870211973120)

"Why are you lying to people like that Discord your team wasn't working on a fix because it was a cloudflare routing leak." by @[jdbohrman](https://twitter.com/jdbohrman/status/1143675097800171523)

"Why does the CopyrightOffice use Cloudflare?" by @[davidclowery](https://twitter.com/davidclowery/status/1142285340801208320)

"So Cloudflare is offering an NTP service... Why do I care?" by @[tmclaughbos](https://twitter.com/tmclaughbos/status/1142258889447993345)

"Hello why the Sayat site crashes and can you solve the problem as soon as possible" by @[QQCC0](https://twitter.com/QQCC0/status/1142178490390908938)

"So a friend of mine just had their parents receive an email from K*wiFarms telling them: "it looks like your stupid spawn is finally going to kill itself. When is the funeral so we can celebrate?"" by @[loudpenitent](https://twitter.com/loudpenitent/status/1141756398260277248)

"I'm unable to connect to Discord. It looks like a Cloudflare server is having trouble reaching the gateway, resulting in 522 error codes in the ws connection" by @[rsWhosdr](https://twitter.com/rsWhosdr/status/1140466980052512770)

"since you use cloudflare I've been unable to use the api at all as cloudflare blocks my server ip's with a 403 :/" by @[intranix](https://twitter.com/intranix/status/1138488279844687872)

"I'm unable to reach cloudflare here in NL and from my server in FR. A quick twitter search also shows some other people are experiencing it, but there's nothing on the status page?" by @[arunesh90](https://twitter.com/arunesh90/status/1138081519422115841)

"I hear a lot of people say nice things about Cloudflare. You know what I never hear them mention? They never mention how Cloudflare pounds your inbox with 3-4 spam emails a day. Just one company. 3-4. A day." by @[grimmtooth](https://twitter.com/grimmtooth/status/1133724162713935872)

"Appears Cloudflare is blocking DNS requests for your WiFi calling" by @[freestylecorsa](https://twitter.com/freestylecorsa/status/1140320959892787200)

"Don't use Cloudflare." by @[AnarchaWitch](https://twitter.com/AnarchaWitch/status/1119761568600862720)

"CLOUDFLARE FUCK YOU" by @[Rires_Magica](https://twitter.com/Rires_Magica/status/1143129998905053185)

"What the fuck is Cloudflare? Sites we visit recurrently spend hours checking browser and you with landscape face waiting, looking like a pregnant to give birth to your recurring site. CloudFlare fixes it, there's bag! This is not security, it is hopelessness!" by @[ReysDosVersos](https://twitter.com/ReysDosVersos/status/1131984678251040768)

"I can’t access your website for support, Cloudflare shows error message. Some websites are opening, others are not." by @[Marcel_Perform](https://twitter.com/Marcel_Perform/status/1138707296605876224)

"I use TOR and CloudFlare tends to unilaterally block Tor exit nodes. It's a problematic security strategy, but CloudFlare seems unwilling to reevaluate." by @[DeborahPeasley](https://twitter.com/DeborahPeasley/status/1138906244641644544)

"I can't seem to access your site. It's been like this for a few days now. I just keep getting the cloudflare page." by @[TacticalGrace_](https://twitter.com/TacticalGrace_/status/1138407485642039298)

"I've ranted about this before, but I will again - Web devs: stop putting your APIs behind Cloudflare! Cloudflare's main selling point is to prevent automated access to web sites, and automated access is *literally the point of an API!* Think, for pity's sake!" by @[Mister_Flarpy](https://twitter.com/Mister_Flarpy/status/1137708123689553921)

"Can't access to article almost likely that the captcha/cloudflare is blocking it" by @[yajount](https://twitter.com/yajount/status/1134586590272118784)

"Your cloudflare DDOS software is blocking site entry" by @[PDAcquaviva](https://twitter.com/PDAcquaviva/status/1136341633920884736)

"Seems the same is happening to me (Norway) - it seems something on Cloudflare is blocking Scandinavian IPs for your site?" by @[Junkfoodjunkie](https://twitter.com/Junkfoodjunkie/status/1136477534076526593)

"Cloudflare not promoting fare trade. The last policy is blocking mobile websites like technology online stores to avoid posible purchases" by @[AndyAgraciano](https://twitter.com/AndyAgraciano/status/1138223592687263744)

"If you submit too fast cloudflare will block you." by @[TomMcCourt_](https://twitter.com/TomMcCourt_/status/1135899828729319424)

"Letsencrypt DNS 方式更新证书，托管在 Cloudflare 的 DNS 更改后一直无效。以为是国内网络原因，后来发现赶上了 Cloudflare 的 DNS 故障" by @[yanchuan](https://twitter.com/yanchuan/status/1135824141624332290)

"Wer glaubt, amerikanische Konzerne wie #Google oder #Facebook wären die größten Dantengangster, der kennt #Cloudflare noch nicht." by @[ASteinBS](https://twitter.com/ASteinBS/status/1135807583871221760)

"*shakes fist furiously at Cloudflare* Stop screwing up our software!" by @[Supermathie](https://twitter.com/Supermathie/status/1133250297760014336)

"It is time to call out Cloudflare and stop using their services. " by @[dittigas](https://twitter.com/dittigas/status/1131112858413076482)

"El mundo se va a acabar. Ayer se cayó Google y hoy hay problemas con Cloudflare" by @[Cangreja533](https://twitter.com/Cangreja533/status/1135678809687154689)

"if anyone is having problems with the latest version of twitter misbehaving on iOS, it’s the Cloudflare DNS app that’s the culprit. Remove the app, problem solved." by @[AhsanDeliri](https://twitter.com/AhsanDeliri/status/1137162786621075456)

"our sites are having issues, loading your error 502, and even the Enterprise Support interface is giving us problems, so we cannot even create a ticket." by @[andevco](https://twitter.com/andevco/status/1135682386853842944)

"Решил я как-то сделать выходную ноду Tor. Ну чтобы помочь людям анонимизировать свой трафик, а для себя возможность просматривать HTTP трафик. Меня через 4 часа заблокировали абсолютно на всех сайтах, где стоит CloudFlare. Все такие не стоит ставить exit-node Tor дома." by @[gashish1n](https://twitter.com/gashish1n/status/1134906045774647296)

"BTW hat Cloudflare einen riesigen Nachteil: Ist bei Verwendung von Tor durch unlösbare Captchas unbrauchbar. Man kann die Seite nicht anonym abrufen. Kann nach dieser DDOS Attacke zwar erwünscht sein, jedoch als Tor Nutzer, der einfach anonym ins Netz will, ziemlich ärgerlich." by @[schurlbaer](https://twitter.com/schurlbaer/status/1134208379650527233)

"من اومدم مال یکیو بدم حالا، این cloudflare اونقد به torم گیر داد و سایته نیومد که بیخیال شدم :)" by @[_alitou](https://twitter.com/_alitou/status/1132019070235287553)

"hmmm, when using brave in their AnonTorNetwork private window, accessing a Cloudflare site is often google (javascript) #captcha'd." by @[phy0x](https://twitter.com/phy0x/status/1131322715741315072)

"cloudflare AND google having issues right now apparently" by @[widdr](https://twitter.com/widdr/status/1135315287308263429)

"Can't access to article almost likely that the captcha/cloudflare is blocking it" by @[yajount](https://twitter.com/yajount/status/1134586590272118784)

"Can’t access Destiny 2 from the UK and then when I try and check the status on help.bungie.net  I’m greeted with a 522 CloudFlare page....you guys got some Vex loose in your servers?" by @[JustYeenThings](https://twitter.com/JustYeenThings/status/1135158050778730497)

"shit's broke from Canada, from VPN working. Cloudflare issues?" by @[_RobertLowe](https://twitter.com/_RobertLowe/status/1135263884447768577)

"cloudflare having a hiccup or why are a lot of websites very very slow right now" by @[faint_visions](https://twitter.com/faint_visions/status/1135258338336358401)

"Issue discovered. The Cloudflare DNS (1.1.1.1) seems to make it fail. Why would a DNS service block file download is a curious issue." by @[bozhobg](https://twitter.com/bozhobg/status/1134767961103917056)

"What’s next, people start trusting a company like Cloudflare to VPN their DNS traffic?" by @[reykfloeter](https://twitter.com/reykfloeter/status/1134432061710839809)

"I lost faith in firefox when they sold out their DNS resolution to Cloudflare" by @[peter_szilagyi](https://twitter.com/peter_szilagyi/status/1134356975729426432)

"That’s censorship. Please … don’t." by @[kickino](https://twitter.com/kickino/status/1133919945639702528)

"I'm having an issue with the site lagging, and your cloudflare keeps appearing with unable to connect to host." by @[SprazzoMedia](https://twitter.com/SprazzoMedia/status/1133466450667233286)

"Cloudflare's head of abuse, has me blocked. So much for transparency or freedom of speech, huh Cloudflare?" by @[phyzonloop](https://twitter.com/phyzonloop/status/1133772177931677702)

"Dear cloudflare: Would you mind explaining to me why you subscribed me to your newsletter without my consent? You also handed over my email addresses to MailChimp without my consent." by @[MacLemon](https://twitter.com/MacLemon/status/1131976581780348928)

"Oh interesting, that happened with one of my email addresses, too." by @[rixxtr](https://twitter.com/rixxtr/status/1132060363229487104)

"Has anyone else had issues with #cloudflare not responding to 'abuse' and #spam complaints? I'm beginning to conclude that they actually encourage it with their technology - which is a pity." by @[thismikebray](https://twitter.com/thismikebray/status/1133748067734347776)

"Cloudflare is nothing you should trust.  Also, By using a different DNS than your ISP's default, you basically give away you data to 2 different companies instead of 1." by @[6JY6ZQVYS4p7YrL](https://twitter.com/6JY6ZQVYS4p7YrL/status/1132391054165061632)

"Good read on why to avoid Cloudflare, And its privacy concerns." by @[6JY6ZQVYS4p7YrL](https://twitter.com/6JY6ZQVYS4p7YrL/status/1132027230111064065)

"It appears to be really hard to get support from you guys and when I joined the community for support you locked my account." by @[CCreativeAus](https://twitter.com/CCreativeAus/status/1133686391014023169)

"Kodi Users Blocked By Cloudflare, Showing No Sources" by @[KodiNewsTips](https://twitter.com/KodiNewsTips/status/1115518849149849601)

"One of our servers is 403 blocked by cloudflare when trying to apt-get the deb repo (configured as per instructions), it's asking for a captcha which the headless server can't complete!" by @[lanosk](https://twitter.com/lanosk/status/1134277049676288000)

"Cloudflare are quite the hypocrites when it comes to transparency and free speech. If you say anything they don't like about their lack of enforcement of their own policies, they ban or block you." by @[phyzonloop](https://twitter.com/phyzonloop/status/1133355175056424960)

"I cannot access the website, it just keeps saying there is an error between Cloudflare and origin web server timed out" by @[RBNTWRK](https://twitter.com/RBNTWRK/status/1132293336478507009)

"Ever since web server admins happily signed up for cloudflare, over 70% of all HTTPS traffic is effectively MITM'ed as far as the browser user is concerned." by @[modrobert](https://twitter.com/modrobert/status/1132193371949273095)

"I can't access Medium today This is being reported by Cloudflare #boring" by @[eucinico](https://twitter.com/eucinico/status/1133040911838056448)

"So, is the site under DDOS attack or is there another reason why Cloudflare security seems to be at actual max and i'm unable to login?" by @[PeliOptic](https://twitter.com/PeliOptic/status/1132350901858066433)

"Are we down in New Zealand? Can't access Cloudflare Location: Auckland? Help!" by @[rugar8](https://twitter.com/rugar8/status/1132850429690126337)

"Cloudflare is shit bro, change your protection." by @[CyrilB4](https://twitter.com/CyrilB4/status/1132745548635824129)

"ずっとCoinDeskがTorで見れないなーって思ってたんだけどこれCloudflareのServer-Side Excludeで弾いてる可能性高いな" by @[CheenaBlog](https://twitter.com/CheenaBlog/status/979482743066198016)

"Been unable to access the site for almost 48 hours now" by @[Remusion](https://twitter.com/Remusion/status/1131683151951147008)

"Anyone else seeing a Cloudflare outage in Au? Many, many sites showing 502 or 522 errors." by @[tomrlacey](https://twitter.com/tomrlacey/status/1128623954279653376)

"Suddenly getting 522 errors all over our site. Works OK if we disable Cloudflare cache." by @[DataCentreOps](https://twitter.com/DataCentreOps/status/1131210939657838592)

"Sir, your amazing site is down for ~week (giving me Cloudflare error 522 every time I try to open it)" by @[btwosoft](https://twitter.com/btwosoft/status/1131846550521561088)

"I have been trying to contact you via warranty claim but I keep getting blocked by cloudflare both at work and at home. I got a bounce when I try to email customer service. Are you just not supporting customers anymore? How do I contact you?" by @[werdina](https://twitter.com/werdina/status/1116325929490571267)

"Got double-billed for a domain last week and now got a cancellation email even though I didn't cancel anything." by @[markmcerqueira](https://twitter.com/markmcerqueira/status/1115346365150679040)

"I received an email saying I signed up for an account but never did. I’ve never even heard of you before I got that email." by @[Diego_Galaviz_](https://twitter.com/Diego_Galaviz_/status/1087955729422696449)

"Cloudflare is blocking some BiteBTC services due to fake claim." by @[bitebtccom](https://twitter.com/bitebtccom/status/1131558073590530054)

"copyright.gov  is hypocritical. They claim to care about copyrights yet use infringement-friendly Cloudflare to hide their website." by @[dxgl_org](https://twitter.com/dxgl_org/status/1117239332480401409)

"Cloudflare is the NSA (effectively), so both proposals are about equal in their merits, but at least by routing traffic through the NSA we reduce the number of hops, increase efficiency, and are more upfront and transparent with users about it." by @[taoeffect](https://twitter.com/taoeffect/status/1075252240179326976)

"Our legal team has just filed complaints, but apparently they're more likely to act if contacted by multiple people." by @[t_blom](https://twitter.com/t_blom/status/1129419501479571458)

"cloudflare blacklisted me and now I cant access most of the internet on my PC.... gonna have to reset everything tomorrow so I can get a new ip.... :( so annoying." by @[Sven986](https://twitter.com/Sven986/status/1118382870324764673)

"One thing that I take from this that it is a privacy invasion from a European point of view, while many Americans see it as an improvement. That makes a lot if sense, I trust my local ISP more than Cloudflare or any US entity." by @[reykfloeter](https://twitter.com/reykfloeter/status/1075115187223564290)

"This has to be a fucking joke eastdakota. Yesterday was the year anniversary that AssemblyFour was kicked from Cloudflare, we received this e-mail from a sales person who did no research at all and when I called was quite shocked to hear we were an escort directory." by @[Zemmiph0bia](https://twitter.com/Zemmiph0bia/status/1118968501408944128)

"Cloudflare employees don't know about their own policies or past." by @[](https://twitter.com/ThisIsMissEm/status/1118989557960249344)

"I know US-based people who cheer for Cloudflare DoH and centralized DNS. I can understand that from their point of view if it becomes the lesser evil. But we don’t have the same Internet everywhere. What is good for them is bad for us and Mozilla should respect all their users." by @[reykfloeter](https://twitter.com/reykfloeter/status/1075115190067347457)

"Matthew Prince, CEO of Cloudflare, has given me ZERO reasons to ever trust him over the years. I have used Cloudflare for their analytics tools which are very nice, but do not and will not place all my eggs in their basket. Nor should anyone." by @[b_mcnett](https://twitter.com/b_mcnett/status/1075274228579098624)

"personally I don't trust Cloudflare at all either. Fortunately i'm capable of routing through an ISP i do sorta trust, and can run my own DNS infra. i think we americans need to understand centralization on the net is a bad thing." by @[nomadlogicLA](https://twitter.com/nomadlogicLA/status/1075116546727456768)

"Cloudflare or their hosting provider could shut it down on a whim. Also advertising based monetization is dangerous for freedom of speech, because the advertisers can lobby to block certain content." by @[purehategraphs](https://twitter.com/purehategraphs/status/1117860637172875264)

"One example where decentralized systems win: decentralized file storage and delivery is *more* efficient than centralized systems." by @[SCBuergel](https://twitter.com/SCBuergel/status/1122542763885649922)

"Cloudflare not working, as usual. Websites constantly down. Another reason why people shouldn't use centralized web serivces." by @[HiddenRefuge](https://twitter.com/HiddenRefuge/status/498472682581938176)

"but because we’ve centralized the Internet so much, companies like Cloudflare, Google, and Facebook become arbiters of who speaks" by @[Pinboard](https://twitter.com/Pinboard/status/897983409552740352)

"I dumped Cloudflare years ago. I would get messages saying, "Cloudflare fine, site down," then i would go to the backend and disable Cloudflare, and guess what? Suddenly the site was accessible again! Happened several times on two different sites!" by @[The__Pigman](https://twitter.com/The__Pigman/status/1146208287408287744)

"my site is suddenly gone -- what can I do to determine what has happened and retrieve the configuration??" by @[lordscarlet](https://twitter.com/lordscarlet/status/1046785164792205314)

"cloudflare had a site hosted on your free plan for years and it was suddenly deleted. no changes to the hosting. had to re-add it." by @[derivativeburke](https://twitter.com/derivativeburke/status/903755267053117440)

"Suddenly required to pass a CAPTCHA when visiting my web site." by @[ColinTheMathmo](https://twitter.com/ColinTheMathmo/status/858708744003358720)

"I am suddenly confronted with an Error 1104, CNAME Cross-User Banned. " by @[WBommel](https://twitter.com/WBommel/status/834865647339331588)

"Yes, my site is suddenly down with the same error as well." by @[grempe](https://twitter.com/grempe/status/834856452296630272)

"the Internet giants make decisions that affect elections, political speech, violent protests—and are answerable only to themselves" by @[Pinboard](https://twitter.com/Pinboard/status/897984010562949120)

"Yes, clownflare, the guys who also harassed Tor users visiting their clients pages with unsolvable captchas." by @[nblr](https://twitter.com/nblr/status/1011513078641459202)

"annoying part was cloudflare" by @[AltichaDev](https://twitter.com/AltichaDev/status/1113892225472192512)

"A Cloudflare issue caused this for me. Check your firewall settings. Super annoying!" by @[zackkatz](https://twitter.com/zackkatz/status/1110375889978548224)

"Oh. Captchas get new meaning! They should change "prove you're not a robot", to "prove you're a human"." by @[majek04](https://twitter.com/majek04/status/790835167317360640)

"I keep being unable to login to your site. It's throwing this annoying error. Credentials are correct for sure." by @[tsopicland](https://twitter.com/tsopicland/status/1111368061368631297)

"The error 502s I keep getting when using your site are getting annoying. Cloudflare says its a host issue - "Server reported a bad gateway" Please sort this out!" by @[mrfuzzy](https://twitter.com/mrfuzzy/status/1099438055755313152)

"I have been unable to access your MiceChat site due to this annoying Cloudflare blocking software. My Router is secure. So, why the case: Region? Your site, which is not a private community, under ADA, should be fully accessible." by @[RonaldVining](https://twitter.com/RonaldVining/status/1131076297243762689)

"cloudflare unable to resolve DNS query? be wary, this could be a DNS spoof attack" by @[ggmesh](https://twitter.com/ggmesh/status/1127870305022533633)

"What is going on w/Cloudflare invading my computer and saying I have to do CAPTCHA on numerous everyday websites I visit? Malware??" by @[faiburns](https://twitter.com/faiburns/status/1121514383807406080)

"Please stop using google captcha." by @[umurgdk](https://twitter.com/umurgdk/status/1120983976468828160)

"Might be pretty hard doing research when entering a captcha every page on google or anything that has cloudflare." by @[s7nsins](https://twitter.com/s7nsins/status/1129380342068940800)

"I think its related to the Cloudflare bug. A bunch of IPs were blacklisted for no reason, and it's preventing me from queuing into any kind of Draft Pick." by @[Novarch_](https://twitter.com/Novarch_/status/1101586706317365248)

"インドネシアで遅い原因はこいつだ" by @[redsnow_](https://twitter.com/redsnow_/status/1131767554165354496)

"you've recently flagged a website of mine as a suspect of phishing and then banned my Cloudflare account. Website is now fixed but I cannot submit a request for review since my account is banned." by @[alexbratsos](https://twitter.com/alexbratsos/status/1128647036964880386)

"Cloudflare banned our domain seemingly under pressure from mass-reporting . This sucks." by @[EaterDoritos](https://twitter.com/EaterDoritos/status/1128334847452299265)

"Cloudflare Error 1005. Can i be unbanned? Did i do something wrong?" by @[CutieJenny6](https://twitter.com/CutieJenny6/status/1126825887083704321)

"I really blame Cloudflare for companies now choosing to ”police” content they don’t like." by @[N8I](https://twitter.com/N8I/status/1130508378952757248)

"Just got several emails about "Traffic acceleration report [...] using Argo Smart Routing". The lack of "Cloudflare" in the "From" makes them hard(er) to identify" by @[limenet](https://twitter.com/limenet/status/1093028491359150080)

"Got an email about “crypto week” from Cloudflare and my first instinct was to report spam." by @[atmattb](https://twitter.com/atmattb/status/1041735938622279680)

"I've already started moving all my clients off Cloudflare." by @[davidzack](https://twitter.com/davidzack/status/1130192242973372417)

"To see a 522 Error is to know CLOUDFLARE is involved in Internet Traffic on Networks.. CLOUDFLARE handles 10% of all Internet Traffic" by @[AVIVA_Films](https://twitter.com/AVIVA_Films/status/1130662977416421382)

"Why I can’t get access to the powerscore website now?" by @[Amber09729770](https://twitter.com/Amber09729770/status/1130308958428647424)

"WHY DOES A PROGRAM CALLED CLOUDFLARE BLOCK ACCESS TO website??? IT DOES IT ON ALL BROWERS I USE. CENSORESHIP IN FULL ACTION." by @[taku96597029](https://twitter.com/taku96597029/status/1130205454976503814)

"Trying to get some work done, but fucking Cloudflare keeps getting in the way. Don't fucking lie to me, the issue is not the website, but your shitty servers. #CloudflareSucks" by @[mevangelista93](https://twitter.com/mevangelista93/status/1129225416672026624)

"Posting private data without consent it's not a Cloudflare problem, it's violating the law and police should work on that." by @[ISbitnev](https://twitter.com/ISbitnev/status/1102526831121584128)

"i think they are too big, and i don't like some of their practices about some other stuff. they would recommend using their ddos protection all the time, that protection forces me to allow js on each" by @[mikrfe](https://twitter.com/mikrfe/status/1130071356626886656)

"I can’t seem to access Notion in my browser: I get a 522 error from CloudFlare" by @[samstarling](https://twitter.com/samstarling/status/1126801647118766080)

"they are being blacklisted on any websites or services running on Cloudflare and at minimum need to complete a recaptcha check to access a website and at worst can't access any Cloudflare run sites and services at all." by @[betadesh](https://twitter.com/betadesh/status/1118451988369485824)

"can't access any websites due to cloudflare blocking me. I can't even access the cloudflare website to put in a support ticket" by @[Hidd3nKnight](https://twitter.com/Hidd3nKnight/status/1118450454499774464)

"I have this infinite Cloudflare captcha for streamlabs OBS no other website seems to be affected. Anyone else had this issue? or if anyone knows a fix. Its been fine, but randomly today its decide to do it." by @[Bigxpz](https://twitter.com/Bigxpz/status/1101489854209294341)

"someone created an account for me and wanted to run my website on CloudFare." by @[fs0c131y](https://twitter.com/fs0c131y/status/1130090448092049408)

"your Cloudflare setup is brocken. Every page I visit I either being forced to solve a captcha or it doesn’t load at all." by @[111110100](https://twitter.com/111110100/status/1128826984883036163)

"Are your services down? i'm getting nothing but 502 Bad gateway whenever I try and browse to a site that is using your service." by @[agent_flameTV](https://twitter.com/agent_flameTV/status/1146055687249760257)

"what is going on i'm getting 502 Error from a ton of sites I browse" by @[shitcanget](https://twitter.com/shitcanget/status/1146055336715083777)

"hi, while trying to browse the website for PeopleFootwear - encountered multiple SSL certificate problems including this one issued by CloudFlare to myshopify.com " by @[dms666](https://twitter.com/dms666/status/1138875300962623489)

"You do know that Cloudflare acts as a proxy? They can be the causing those 502 errors and slow loading times. They aren't perfect. If you're connected to a (shitty) wifi network that can cause slow load times too. Deactivate Cloudflare next time before jumping to conclusions!" by @[StopTheComplain](https://twitter.com/StopTheComplain/status/1100022334189170688)

"I can’t even load your website, it’s not working idk why." by @[Shahbaz28149872](https://twitter.com/Shahbaz28149872/status/1086070566342160385)

"Cloudflare how do we get you to stop showing us the "I AM NOT A ROBOT" screen?" by @[GD_EdRob](https://twitter.com/GD_EdRob/status/1127864910979878912)

"Cloudflare page would tell it explicitly; "Hey, we think you're a robot, we don't serve your kind in this corner of the web"" by @[JessicaCGlenn](https://twitter.com/JessicaCGlenn/status/1111259638333243392)

"Check into Cloudflare it seems to be forcing blatant censorship in part by blocking access to sites and information on the internet!" by @[rickadam0](https://twitter.com/rickadam0/status/1128934188084711425)

"I'm hearing my friends got doxed by 8ch/GG because of a gross mishandling by CloudFlare ?! Are you kidding me?!!" by @[dirtbagboyfren](https://twitter.com/dirtbagboyfren/status/554668717972287488)

"Short story: CF passes on all the info you submit in their abuse form (incl. name, email) to the website owner." by @[iglvzx](https://twitter.com/iglvzx/status/554689456670650368)

"There is no excuse for even considering the possibility of routing all user traffic through Cloudflare. What a phenomenally dangerous idea. There are many better options." by @[taoeffect](https://twitter.com/taoeffect/status/1075053653961072640)

"If you report abusive content on that particular site you will get doxed. It's not safe to report that site." by @[dxgl_org](https://twitter.com/dxgl_org/status/1108405275709620224)

"Gotta wonder why, when innocent players are being banned and harmful ones can just as easily be avoided / blacklisted later, nothing is being done and Cloudflare is still in use despite it going rogue..." by @[Zephyrpocalypse](https://twitter.com/Zephyrpocalypse/status/1118512904582701057)

"Is there any way to take this cloudflare ban off my ip address . I have whitelisted it . Tried reseting my router i have tried everything i have it the last week and it is still not lifted" by @[CarolineHand8](https://twitter.com/CarolineHand8/status/1120729470799962117)

"...but it seems that there is another bug causing your cloudflare to IP ban you soooooo Me: ..." by @[phoenixprime1](https://twitter.com/phoenixprime1/status/1118476497344442369)

"CloudFlareからIP Banされたらネットの何割か使えなくなりそう" by @[Lispict](https://twitter.com/Lispict/status/1118674366995165184)

"Cloudflare, if I use Tor do you still read the resulting so-called abuse report? And why does the head of "abuse" block critics?" by @[dxgl_org](https://twitter.com/dxgl_org/status/1093280991270092800)

"CF are an outlier with *ALL* vile sites behind them and make the abuse process as hard as possible." by @[plambrechtsen](https://twitter.com/plambrechtsen/status/1125483870667792387)

"My problem is *created by* Cloudflare's protection of violent extremists and not solved by giving more Cloudflare for me." by @[lizthegrey](https://twitter.com/lizthegrey/status/1127946041058263042)

"Cloudflare problems for south america – hundreds of websites down" by @[Kush_Shailendra](https://twitter.com/Kush_Shailendra/status/1128371698305204224)

"Javascript is off by default because of trust issues with a website Meanwhile Cloudflare demands I turn on Javascript because of trust issues" by @[steelhoof](https://twitter.com/steelhoof/status/1128105553299775488)

"a lot of people are getting capcha and challenge on every sites with Cloudflare since sunday without proper reason." by @[Ahks_u](https://twitter.com/Ahks_u/status/1127828742947180544)

"It seems like our applications getting blocked by your CloudFlare firewall." by @[kooicia](https://twitter.com/kooicia/status/1128610811235061760)

"Past experience shows Cloudflare treats Tor network traffic as suspect and I have to answer captchas to load webpages." by @[joshburker](https://twitter.com/joshburker/status/1126942827118772224)

"I keep getting a message from Cloudflare saying I'm blocked from the site. It started a few days ago after clicking a Google search result." by @[ThisSteveGuy](https://twitter.com/ThisSteveGuy/status/1128427079849119744)

"I got blocked by Cloudflare's CEO and "abuse" head for asking similar questions." by @[dxgl_org](https://twitter.com/dxgl_org/status/1128041430985912320)

"Still not usable in China, the whole world except China!" by @[iWyvern](https://twitter.com/iWyvern/status/1125708586779389953)

"I'm trying to evaluate Hubspot for our CRM solution, but our IP address seems to be blocked by CloudFlare" by @[sitchaba](https://twitter.com/sitchaba/status/1125403797939990529)

"cloudflare blocking u from playing friend...seems like they cucked you as well..." by @[Unhearddoc999](https://twitter.com/Unhearddoc999/status/1118506406133415938)

"Why don’t you explain blocking domains? You’re pretty good at blocking access to websites." by @[JimInHeaven](https://twitter.com/JimInHeaven/status/1127598018197430278)

"half the sites i visit require me to do a captcha, and then half the resources fail to load even after i'm successful." by @[adamjogrady](https://twitter.com/adamjogrady/status/1127766600428343297)

"I have an issue where CloudFlare is blocking access to HubSpot ... it is from one IP only (my work IP)" by @[sitchaba](https://twitter.com/sitchaba/status/1125825108050890754)

"Cloudflare WAF blocking real Yandex and Bing bots, starting from today" by @[Nktzz](https://twitter.com/Nktzz/status/1128335777879011330)

"Cloudflare is not an option. You should not use it if you value your privacy." by @[larrybeck_](https://twitter.com/larrybeck_/status/1120180809955254272)

"Cloudflare can fuck right off with this sudden "Confirm you're not a robot" page that's popping up everyfuckingwhere. Sort your malconfigured shit out." by @[anarchic_teapot](https://twitter.com/anarchic_teapot/status/1127677511838707713)

"Resulta que la web m'ha bloquejat per activitat sospitosa Quin ensurt!" by @[bartomeumiro](https://twitter.com/bartomeumiro/status/1127517268152287233)

"They block anyone who challenges them on their lack of morals or ethics. All while claiming transparency and protection of free speech." by @[phyzonloop](https://twitter.com/phyzonloop/status/1126950283580456960)

"Cloudflare by design has to MITM your TLS connection, #cloudbleed can happen again." by @[kraih](https://twitter.com/kraih/status/835918495250460672)

"How about actually focus on protecting your website and not rely on free cloudflare to cover your ass" by @[organise](https://twitter.com/organise/status/1127363885282017280)

"Use #1dot1dot1dot1, and you WILL be spied on." by @[brettglass](https://twitter.com/brettglass/status/1126521626768609281)

"Cloudflare has been crippling our web site speed and has been removed. Things should be noticeably better from today." by @[ModSquadAU](https://twitter.com/ModSquadAU/status/1126617699591737345)

"If you are actually deleting all of the information you have on me then how are you able to prevent me from signing up with the same email?" by @[real_ate](https://twitter.com/real_ate/status/1126415124095471617)

"Cloudflare keeps banning me temporarily... just from browser activity. Is there anything I can do to prevent this?" by @[marcgoujon](https://twitter.com/marcgoujon/status/1120726019407638528)

"Your firewall rules prevent our office IP to visit (Cloudflare 1020, Access denied). Can you help us out?" by @[WietseWind](https://twitter.com/WietseWind/status/1115176560204627969)

"Cloudflare deleted my domain config I lost all my DNS settings no wonder why my emails were not working." by @[stefan_eady](https://twitter.com/stefan_eady/status/1126033791267426304)

"If you file a complaint, it’s sent to the folks hosting Kiwifarms & they give the complaint directly to Kiwifarms to dox victims further." by @[remembrancermx](https://twitter.com/remembrancermx/status/1010329041235148802)

"We're about to find out if #Cloudflare keeps your #IPAddress anywhere." by @[shadesmaclean](https://twitter.com/shadesmaclean/status/1125516351966027776)

"Cloudflare, if you don't know this; we have seriously misplaced our trust in you." by @[ADoug](https://twitter.com/ADoug/status/1125266988966739968)

"Brittany has been killing my inbox with spam. I politely ask her to remove me from your marketing list and she replies ... blaming "marketing" and inviting me to an event. Two days later, Keenan emails me, inviting me to the same event." by @[johnzilch](https://twitter.com/johnzilch/status/1126898232913739781)

"I politely replied that I don't have purchasing authority or reason to use your products. Then he added me to your mailing list. Stop this insanity now." by @[danfuhry](https://twitter.com/danfuhry/status/1001868888789995520)

"Hi, I get spamed by an Cloudflare employee with the same "Broadening possibilities with Cloudflare"-Mail every day at noon. Can this stop immediately please?" by @[BooVeMan](https://twitter.com/BooVeMan/status/1070606946728902656)

"Cloudflare sends spam so many times. I said "Don't do it" but they don't care." by @[christine_nakaj](https://twitter.com/christine_nakaj/status/1035430229609656322)

"Cloudflare are intentionally rubbish at dealing with abuse. It's no mistake they don't validate emails, it's no mistake their AUP is one paragraph. Everyone needs to stop thinking of them as some benign DDoS provider as none of the other companies have this problem." by @[plambrechtsen](https://twitter.com/plambrechtsen/status/1124792607542890496)

"Outrageous to use SSL MITM 4 a healthcare site. Horrendous for fintech." by @[paulmillr](https://twitter.com/paulmillr/status/834980791440535552)

"Which means, you are in fact blocking Tor. Finger pointing to the TLS MITM service you use is just pathetic." by @[MacLemon](https://twitter.com/MacLemon/status/793937442789355520)

"Cloudflare is doing TLS/SSL termination (MITM) at scale." by @[cqwww](https://twitter.com/cqwww/status/1128666788789510146)

"Cloudflare can, as it does a man-in-the-middle (MITM) attack on all of its users, even behind SSL/TLS as it does SSL Termination -- so user's that think they have a secure connection with a website, in fact, don't." by @[cqwww](https://twitter.com/cqwww/status/1128054203291537408)

"That is not good enough as Cloudflare exposes us and violates our privacy with their MITM spying technology that feeds the Big Brother network that is watching our every move globally." by @[larrybeck_](https://twitter.com/larrybeck_/status/1121258213540880384)

"If you truly care about your users & you are so concerned about privacy and having a decentralized Internet, then you will do what’s right" by @[larrybeck_](https://twitter.com/larrybeck_/status/1121412414115917826)

"A lot of us VPN users don't enjoy helping google train computer vision and fingerprinting bots. Or spending 20 minutes being told we don't know what crosswalks are." by @[digiwombat](https://twitter.com/digiwombat/status/1108480942417436672)

"We are trusting one secretive, private company with censoring the web. We’re essentially centralising a decentralised system once again." by @[steve228uk](https://twitter.com/steve228uk/status/1061949645113511939)

"They dox anyone who complains. They are the world's top spambot host." by @[HelloAndrew](https://twitter.com/HelloAndrew/status/897260208845500416)

"Chiming in here, of course, but Cloudflare ARE censoring people." by @[misslolahunt](https://twitter.com/misslolahunt/status/1106745032722137088)

"Just as a warning to others. Because if you fill out a cloudflare complaint, it’ll dox anyone else to the site." by @[Bridaguy](https://twitter.com/Bridaguy/status/915003769280172037)

" I do not appreciate "Cloud flare" unilaterally censoring conservative sites I choose to visit and comment on" by @[Suchindranath](https://twitter.com/Suchindranath/status/1086253299064438784)

"Cloudflare's CEO and abuse head block me over arguing about their support of abusive websites. Trolls fear transparency." by @[dxgl_org](https://twitter.com/dxgl_org/status/1123622959124549632)

"Good luck, Cloudflare loves spammers and other rogue actors using their infrastructure." by @[phyzonloop](https://twitter.com/phyzonloop/status/1156047693418045440)

"Yeah, same thing. It happens on every Cloudflare protected site I try to visit. Because of this, I cannot access raspberrypi.org  and alternativeto.net  (both sites I have tried to visit recently)." by @[twosecslater](https://twitter.com/twosecslater/status/1155949306911715328)

"Defenders are screwed. Plus users are screwed as they lose all control of privacy to companies like cloudflare." by @[hrbrmstr](https://twitter.com/hrbrmstr/status/1121180307091271680)

"CloudFlare is breaking everything." by @[sukarodo](https://twitter.com/sukarodo/status/1124283685304512512)

"New, let CloudFlare MITM your SSH . With an install and a some small .ssh/config changes you can forget all your SSH belong to them. " by @[dontlook](https://twitter.com/dontlook/status/1067408174007881728)

"Thought my Website was a Phishing Suspect, Cloudflare decided to force a Nasty Don't Enter notice on my E-store domain. No notifications to me at all!" by @[modaitalysuits](https://twitter.com/modaitalysuits/status/1122986421109682176)

"RSS Feed is behind the Cloudflare DDoS protection. My RSS Reader is self hosted, so it is impossible for it to retrieve any feeds" by @[GarcaMan2](https://twitter.com/GarcaMan2/status/1121234101657321474)

"Mais comment osez-vous parler de "respect des données personnelles"? Votre site est bourré de traceur en tout genre! Pire vous routez vos utilisateurs via cloudflare aux USA!" by @[Armdias](https://twitter.com/Armdias/status/1120957817693847557)

"Avoid and ask any website you use why they are using them and to stop ASAP." by @[larrybeck_](https://twitter.com/larrybeck_/status/1120840099112026112)

"I hope other IT leaders with buying power will boycott Cloudflare until they stop protecting the armpit of the internet." by @[lost_packets](https://twitter.com/lost_packets/status/1117785522154483719)

"Companies should boycott Cloudflare immediately." by @[DanRayburn](https://twitter.com/DanRayburn/status/1073785965586669568)

"#Cloudflare is blocking me again. Asks me to tackle a challenge that requires #javascript to get past, basically making my surfing a lot less convenient, far less secure, and making the Web a much more dangerous place. Webmasters, boycott CF." by @[schestowitz](https://twitter.com/schestowitz/status/1066085547561680897)

"Folks don't forget to add CloudFlare to your list of providers to boycott!" by @[miftik](https://twitter.com/miftik/status/591684333620109313)

"Security: You might want to avoid sites with #Cloudflare" by @[schestowitz](https://twitter.com/schestowitz/status/835529104007004164)

"Zero Cloudflare, our site scores a 98% on pagespeed.. Really cant see any benefit to using cloudflare at all" by @[WESHUK](https://twitter.com/WESHUK/status/1120817837499650048)

"I read a description of CloudFlare as a "man-in-the-middle attack on the entire internet" and now I'm just a LIIIIIITLE bit conflicted." by @[ianbeck](https://twitter.com/ianbeck/status/1120814430239907840)

"don't be fooled, Cloudflare is Evil and misleading, it is down only for some, the others are blocked by security rules" by @[cassyan128](https://twitter.com/cassyan128/status/1117577021297692673)

"It's why I will never, ever, chose Cloudflare (and have actively recommended business to not do so either)." by @[GregWildSmith](https://twitter.com/GregWildSmith/status/1107869665051717632)

"Cloudflare IMHO remains a major threat to #privacy. Were they to turn evil or be compromised, with the technical ability to snoop TLS encrypted traffic on essentially all top websites, we'd be toast." by @[V_Gibello](https://twitter.com/V_Gibello/status/1100649328765485058)

"Cloud flare you overdoing everything. Now i need to do a CAPTCHA for every page i visit. It surely changes how we use the web." by @[Kemboidickson](https://twitter.com/Kemboidickson/status/1115317470078930951)

"IP's are being blacklisted and denied access to any site using Cloudflare." by @[purplepopoto](https://twitter.com/purplepopoto/status/1118415845296762880)

"My IP got flagged for spam cause I tried logging into VRChat when they were having issues. Now I keep getting captchas for everything" by @[Fleurfurr](https://twitter.com/Fleurfurr/status/1118385992938020867)

"only to find out that my ip was blacklisted becuz of the cloudflare thing and when i used my vpn i couldnt leave my home world or join my friends." by @[NeitherVr](https://twitter.com/NeitherVr/status/1118475648849330176)

"Let's get Blacklisted on Cloudflare, Ty Vrc Devs." by @[ultracori](https://twitter.com/ultracori/status/1118485147911958528)

"the equivalent of a Flash-required splash page 20 years ago. It makes you look stupid." by @[skry](https://twitter.com/skry/status/1096832011631681537)

"You say you're in IT but don't acknowledge the SPOF issue in cloudflare (right after privacy issues and Tor users problems to access your content)?" by @[Idfollowher](https://twitter.com/Idfollowher/status/1094339678516449282)

"I can't even reach the "contact us' without cloudflare banning my IP because of 'rate limit'." by @[SirIsaacIn1905](https://twitter.com/SirIsaacIn1905/status/1091368707484311552)

"Mind telling me why everything your apart of thinks im a Robot?" by @[BrewManChew_](https://twitter.com/BrewManChew_/status/1118618347174858753)

"I've always referred to CloudFlare as the 'global passive adversary ya momma warned you about'." by @[NetworkString](https://twitter.com/NetworkString/status/1120073249189179392)

"They'll have to monetize data eventually, likely with an unnoticed tweak to their ToS." by @[phyzonloop](https://twitter.com/phyzonloop/status/1119703561087148032)

"#Cloudflare increases its MITM'ing of the web with a free #VPN for mobile. Best to avoid IMO." by @[sweepthenet](https://twitter.com/sweepthenet/status/1113086245159256066)

"Not playing the game is the best thing for anyone who wants to use the internet without being blocked." by @[Ares_CVR](https://twitter.com/Ares_CVR/status/1118429720515891200)

"VRChatのようなゲームはCAPTCHAフォームを受け取ることができないので、ブラックリストに捕まった人々は事実上ゲームをプレイすることができません。" by @[Meowkyway222](https://twitter.com/Meowkyway222/status/1118427292676476929)

"今日からなんか各地のサイトでCloudflareとかいうページが表示されてcaptchaのチェックをやたら求められるようになった" by @[yagamo54](https://twitter.com/yagamo54/status/1118115780984311809)

"クライアントがバグって意図せず攻撃者のような大量アクセスしてCloudflare大激怒でVRCユーザーのIPアドレスがブラックリスト入りってことかね" by @[bironist](https://twitter.com/bironist/status/1118474120960757760)

"バグが原因でCloudflare使ってるWebサービス全体にIPブロックされてる" by @[JO2PEG](https://twitter.com/JO2PEG/status/1118506271651401728)

"What’s going on with this cloud flare thing it’s locked me out of all website" by @[Xiverta](https://twitter.com/Xiverta/status/1118417249222529024)

"Cloudflare run DoH and do Man-In-the-middle of the century" by @[FernandoGont](https://twitter.com/FernandoGont/status/1106469002807779330)

"It's not a fully-encrypted experience. In fact, Cloudflare becomes, by definition, a Man In The Middle (MITM) -- and at mass scale!" by @[FernandoGont](https://twitter.com/FernandoGont/status/1077984986144354304)

"I guess #cloudflare's business model is #spam (and man-in-the-middle attacks on #tls)" by @[christophe0o](https://twitter.com/christophe0o/status/1069295953478336517)

"journalist was murdered in #Kiev. His home address was doxxed by the Ukrainian website, protected by Cloudflare" by @[RusEmbUSA](https://twitter.com/RusEmbUSA/status/1118235522168758272)

"Everybody remain silent about US-based extremist website 'Peacekeeper’ protected by Cloudflare with ‘high moral standards'" by @[RusEmbUSApress](https://twitter.com/RusEmbUSApress/status/1045882620289196032)

"They've doxxed someone who reported pedoporn in the past so I'm not expecting miracles" by @[jpetazzo](https://twitter.com/jpetazzo/status/897554355368456193)

"Hi! Attached is the screen shot of my mobile browser when I try to access fitbit.com . Due to this, the app does not sync as it cannot get past this CloudFlare captcha." by @[rahul7280](https://twitter.com/rahul7280/status/935288633375916032)

"I reported a website for hosting illegal content. CloudFlare doxxed me to Brennan. Brennan doxxed me to 8chan" by @[petercoffin](https://twitter.com/petercoffin/status/554728016480182272)

"Cloudflare who are free speech to the point of doing nothing. You can't safely report valid abuse to them without it being forwarded to the hosts and you getting doxxed. Then cloudflare wash hands." by @[plambrechtsen](https://twitter.com/plambrechtsen/status/1125980039814606849)

"Our users are actually customers, and we look out for them and strive to satisfy them. Unlike the lying #Clownflare, which wants to be an MITM." by @[brettglass](https://twitter.com/brettglass/status/1118543682385399809)

"You buy the "free" service with your sites traffic.a.k.a mitm the heck out of you." by @[cireumayn1](https://twitter.com/cireumayn1/status/1114673721355251712)

"Who the hell is cloudflare and why do they block access" by @[TheCubanEdge](https://twitter.com/TheCubanEdge/status/1120513042704404480)

"Why should we trust cloudflare to MITM all our traffic as a VPN when there are such bad security practices in place with existing products?" by @[GvilleComputer](https://twitter.com/GvilleComputer/status/1113046802566131713)

"So, you are actually putting the trust in a commercial provider that is already in a position to find out very private details about you?" by @[h3artbl33d](https://twitter.com/h3artbl33d/status/1112763751353200642)

"Cloudflare seems to be protecting it a bit too much" by @[spitf1r3](https://twitter.com/spitf1r3/status/1104908643017940992)

"all web traffic is routed through CloudFlare - which is an adversary to privacy (MitM by design)" by @[h3artbl33d](https://twitter.com/h3artbl33d/status/1111982937707298816)

"Aren't Cloudflare themselves the biggest MITM around?" by @[auspicacious](https://twitter.com/auspicacious/status/1111500313259724803)

"This Cloudflare error is popping up on a ton of sites all of a sudden. It doesn't seem to be malware or anything..." by @[Eltalite](https://twitter.com/Eltalite/status/1118011155509682176)

"When #Cloudflare and #Google prevent you from watching a government's parliamentary debates. Does that count as interfering with democratic processes?" by @[STP_KITT](https://twitter.com/STP_KITT/status/1113442207619358720)

"I MADE A BILL PAYMENT BUGGING IT OUT AND IT DOUBLE CHARGED ME CAUSE OF IT." by @[The_FoxGod](https://twitter.com/The_FoxGod/status/1118579858437578752)

"People find themselves met with a brick wall of endless captcha requests any time they try to go to a cloudflare-protected website, and they can't even submit a ticket to Cloudflare about it because they're protected, obviously. " by @[RiDakuYazumi](https://twitter.com/RiDakuYazumi/status/1118512842959929344)

"Blind people simply can't pass them. Your test is failing at telling humans & computers apart." by @[axelsimon](https://twitter.com/axelsimon/status/1094303636350529536)

"My IP was blocked by Cloudflare because I can't access a variety of websites without the CAPTCHA page showing up. I also can't play RuneLiteClient anymore." by @[MohamedMounib3](https://twitter.com/MohamedMounib3/status/1119654593867075586)

"Hey ProtonVPN some of your servers in PT are blocked by cloudflare" by @[brecke](https://twitter.com/brecke/status/1118896499121897474)

"Your support of Cloudflare's spyware and lies demonstrates that, again, you're either misguided or malevolent." by @[brettglass](https://twitter.com/brettglass/status/1119720600363261953)

"It wants to be a 'man in the middle' between you and the Internet, and between Web sites and the Internet." by @[brettglass](https://twitter.com/brettglass/status/1119721137263534081)

"I don't know why I was banned from access government website." by @[abidavid1199](https://twitter.com/abidavid1199/status/1119734124821057536)

"You've officially banned my country via CloudFlare's firewall. I honestly, sincerely hope your company goes bankrupt because this is pure bs." by @[ii0wii](https://twitter.com/ii0wii/status/1119520320703143941)

"I had a friend who got banned by cloudflare" by @[WhisperMute](https://twitter.com/WhisperMute/status/1118946685324070914)

"Hey guess what! i got ip banned by Cloudflare Woohoo!" by @[cearealkillas](https://twitter.com/cearealkillas/status/1118626896143511552)

"vrchat fucked up and ended up having bunch people banned from cloudflare so no one could login i cant even watch anime" by @[LoliGoddess_VR](https://twitter.com/LoliGoddess_VR/status/1118424623392202752)

"Imagine being so incompetent at networking that you get your users flagged for spam from cloudflare, you guys are a joke" by @[SilverTheFlat](https://twitter.com/SilverTheFlat/status/1118203335973244934)

"Cloudflare is trash, has always been trash, and will continue to be trash." by @[justkelly_ok](https://twitter.com/justkelly_ok/status/897519768323768320)

"Is it okay to block millions of people from the Internet because it's your business model? #cloudflare #humanrights" by @[torproject](https://twitter.com/torproject/status/717794462387322885)

"#CloudFlare classic from last year's defcon. Remember, most of CloudFlare's competitors don't block #Tor." by @[torproject](https://twitter.com/torproject/status/759445026711580672)

"You probably got blacklisted by Cloudflare like a good amount of people" by @[Mertvyi_](https://twitter.com/Mertvyi_/status/1118550942394109957)

"Only started today. I am definitely not a robot!" by @[TheReal_blue166](https://twitter.com/TheReal_blue166/status/1087778908324458496)

"Ridiculous CAPTCHA usage, Cloudflare Edition; 'not a robot' to refresh the API key for an account that's already locked behind credentials + MFA, and requiring the password." by @[sindarina](https://twitter.com/sindarina/status/1042085625032257536)

"I spent my day telling Cloudflare every 5 min on every site that i'm not a robot." by @[morhac](https://twitter.com/morhac/status/1017409748482719744)

"Dear Cloudflare stop killing our jobs  #Cloudflare #CloudflareDown" by @[Lipan_in](https://twitter.com/Lipan_in/status/1146060555632799747)

"Hey cloudflare, can you stop being fucking retarded and work properly, so Discord can also work properly, assholes" by @[lelguy21](https://twitter.com/lelguy21/stataus/1146060826924609543)

"People are being blacklisted by cloudflare, and the fact that it's literally all down to the vrc devs, this will end with a lawsuit" by @[Retr0Fighter](https://twitter.com/Retr0Fighter/status/1118487667094765569)

"This little VRChat bug is banning people from actual millions of websites with very few convenient fixes." by @[RiDakuYazumi](https://twitter.com/RiDakuYazumi/status/1118514780652879872)

"DO NOT GO ON THE VRCHAT WEBSITE THEY WILL ALSO BLOCK YOUR IP" by @[ECW117](https://twitter.com/ECW117/status/1118477598202507265)

"Im so damn annoyed with this Cloudflare stuff" by @[styloVR](https://twitter.com/styloVR/status/1118545393158119425)

"Gonna quit VRChat for a bit to avoid a cloudflare ban. This isn't gonna end pretty for the dev team at all." by @[iamblssm](https://twitter.com/iamblssm/status/1118621746326675457)

"Cloudflare like to act as champions of free speech, but as soon as protections for free speech disappear, they all but rolled over and censored that speech." by @[ThisIsMissEm](https://twitter.com/ThisIsMissEm/status/1118390074557775872)

"I am stuck at an infinite loading screen trying to load the game. This has also caused my IP to be flagged by Cloudflare" by @[Etna_AD](https://twitter.com/Etna_AD/status/1118393520077901824)

"GOT FLAGGED FROM CLOUDFLARE FOR PLAYING/STREAMING VRChat SO NO STREAM TODAY" by @[odomfire](https://twitter.com/odomfire/status/1118263523002519553)

"How do I fix the cloudfare from popping up. This has made it Impossible for me to even stream." by @[RealTypey](https://twitter.com/RealTypey/status/1118272161607749632)

"ISPs don't sell customer DNS data. Cloudflare is fearmongering in an effort to garner DNS traffic to spy on and sell its VPN" by @[brettglass](https://twitter.com/brettglass/status/1118202225715752960)

"You're lying about your paid product, too. VPNs don't protect privacy, because their exit nodes are easy for governments to monitor." by @[brettglass](https://twitter.com/brettglass/status/1118271101665255432)

"can you unflag my ip please. its actually stoping me from doing anything" by @[churchillvr](https://twitter.com/churchillvr/status/1118164231860432896)

"So much for transparency. If they have nothing to hide, why block me?" by @[phyzonloop](https://twitter.com/phyzonloop/status/1118229040777826305)

"Seems like your Cloudflare waf rule is blocking me!" by @[JobinNixHive](https://twitter.com/JobinNixHive/status/1118242827731914754)

"Tu veux nous informer d'un problème? Ok, mais donne tes données personnelles avant." by @[fladna9](https://twitter.com/fladna9/status/1037585196802101249)

"Your Cloudflare config is being screwy & blocking my wife’s IP (which has been the same IP for over a year) which is screwing up her stream." by @[alexanderhanff](https://twitter.com/alexanderhanff/status/1115660493669191680)

"Cloudflare's WAF blocking Googlebot. Is this accurate? Even this morning I woke up and noticed another page dropped" by @[madhacks](https://twitter.com/madhacks/status/1114577907114856448)

"I'm super tired of websites deciding what I want, or indeed who I am, based on my current geolocation." by @[storyneedle](https://twitter.com/storyneedle/status/1084821216597135360)

"it seems you're running Cloudflare and I can't access your API anymore since I need to solve some Cloudflare captcha from my server" by @[PaulWebSec](https://twitter.com/PaulWebSec/status/1074408829041950720)

"I can't access the article. Getting an error from Cloudflare." by @[QuestionsAndTea](https://twitter.com/QuestionsAndTea/status/1083389572812337158)

"I am in internet cafe which mainly used as gaming facility. There's no malicious activity since we can't. But Unfortunately somehow I can't access all cloudflare website. I can't access support page." by @[cloudscoid](https://twitter.com/cloudscoid/status/1088332993888845825)

"I can't access your blog in the United States using my PC. CloudFlare return Error 1020. My mobile in the same network have no issue though." by @[tam403](https://twitter.com/tam403/status/1098796403856306176)

"I can't access the website. Maybe BroadwayWorld banned my IP from accessing the website?" by @[AnthonyeQuality](https://twitter.com/AnthonyeQuality/status/1100355528411164673)

"Hey I can't seem to access the site, getting a cloudflare 522 error" by @[nemo2966](https://twitter.com/nemo2966/status/1100530567999823877)

"Unfortunately inaccessible from this IP. Cloudflare's captchas block not just bots but human beings. :(" by @[ErgoOne](https://twitter.com/ErgoOne/status/698513443314241537)

"you're not protecting your customers this way but making them lose money. go figure another way to block bots instead of human" by @[dtdw](https://twitter.com/dtdw/status/541462915434287104)

"I can't access your site. I'm getting a cloudflare error 552 connection timed out" by @[Abdul201388](https://twitter.com/Abdul201388/status/1101851412961611777)

"I’m fucking tired of cloudflare and their nonsense grandstanding about DoH." by @[JumpyBirdBird](https://twitter.com/JumpyBirdBird/status/1065536618667270145)

"i’m getting tired of cloud flare’s captcha on every single site" by @[riyadhalnur](https://twitter.com/riyadhalnur/status/729191160917319680)

"Hate captcha. Hate Cloud Flare." by @[EconomicMayhem](https://twitter.com/EconomicMayhem/status/723348151181164544)

"So nice of cloud flare to use a captcha provider that doesn’t appear to work inside China, thus completely blocking sites." by @[albertren](https://twitter.com/albertren/status/716233184833196032)

"eww I got cloudflarecaptcha'd again… too lazy to search for a good pic tho, so I'll just pretend I didn't click link for once" by @[yuki_the_maven](https://twitter.com/yuki_the_maven/status/708468683430039552)

"I'm having trouble getting to your site, everytime i try to get to it, cloud flare seems to block me" by @[slurpeerun](https://twitter.com/slurpeerun/status/1067652451270967296)

"You have Cloudflare on your website and you're blocking access from entire countries?" by @[dec23k](https://twitter.com/dec23k/status/1111961153742213121)

"hey Cloudflare, why do you suck so much? you've blocked my ability to use streamlabs while streaming." by @[Sam41gaming](https://twitter.com/Sam41gaming/status/1101413577796153344)

"#Cloudflare Owns the encryption keys to the servers that collected the data from the 2018 voting machines." by @[MaggieMaeMooCow](https://twitter.com/MaggieMaeMooCow/status/1062775200192770048)

"What about stop using stupid Cloudflare!" by @[x61sh](https://twitter.com/x61sh/status/1116808798897868800)

"I don’t want to select pictures with a car plate to read a fuckin article on web" by @[umurgdk](https://twitter.com/umurgdk/status/1116649566416162818)

"クラウドフレアっていうやつにアクセス拒否くらってる模様" by @[notmoneybutrust](https://twitter.com/notmoneybutrust/status/1114273590944210944)

"If you use 1.1.1.1, cloudflare will be able to snoop on what you do on the Internet." by @[neirbowj](https://twitter.com/neirbowj/status/1116545712668516352)

" Cloudflare blocks a bunch of people without giving you any choice in the matter. If you're using cloudflare you are allowing them to censor who hits your site." by @[jeffcliff1](https://twitter.com/jeffcliff1/status/1108142374776176640)

"It breaks a lot of things for me, like images stop loading on Twitter or some apps straight up don’t work." by @[ScottishBakery](https://twitter.com/ScottishBakery/status/1116673794855272449)

"Not just you. CloudFlare got interrupted a few times today." by @[guyothersome](https://twitter.com/guyothersome/status/1116471262132436994)

"1.1.1.1 prevents some apps from working, in particular I have problems connecting with the app" by @[SCRWD](https://twitter.com/SCRWD/status/1116291854759354371)

"First, you create a problem. Then, you pretend to solve that problem." by @[switch_d](https://twitter.com/switch_d/status/1116039082977533952)

"Why are you blocking listeners in Thailand? You’re blocking countries at the CloudFlare level. I’ve tried multiple IPs from multiple providers here. It’s discriminatory to block based on country of origin." by @[smacintyre](https://twitter.com/smacintyre/status/1087543097574154240)

"Cloudflare DNS is flaking out ugh" by @[sleepingkyoto](https://twitter.com/sleepingkyoto/status/1115014195286646784)

"Tell eastdakota it's not nice to block me. He said in his YouTube interviews that #censorship is bad." by @[phyzonloop](https://twitter.com/phyzonloop/status/1067177577519898626)

"They want to be a single point of failure once again and they've already failed once" by @[wombatush](https://twitter.com/wombatush/status/1114464442794643461)

"This special treatment is inexcusable" by @[saleemrash1d](https://twitter.com/saleemrash1d/status/1114289465760075776)

"You are KILLING the #internet and aiding #surveillance agenda of #GAFAM and others" by @[Dr. Roy](https://twitter.com/schestowitz/status/1067299259593175040)

"The log file from your iOS app shows that you do track personal information" by @[holydevil](https://twitter.com/holydevil/status/1112769739045158912)

"Which part of the privacy policy allows you to share data with marketing crap" by @[thexpaw](https://twitter.com/thexpaw/status/1108424723233419264)

"Can you explain why I am blocked from accessing your site?" by @[Susan_Larson_TN](https://twitter.com/Susan_Larson_TN/status/1110207775311912966)

"Did my questions and comments hit a little too close to home so you BLOCKED ME?" by @[phyzonloop](https://twitter.com/phyzonloop/status/1109133085336047617)

"Why is your site blocking me?" by @[flarn2006](https://twitter.com/flarn2006/status/1107837140359094273)

"It's pretty bad as half the internet is behind Cloudflare." by @[Skyfusion89](https://twitter.com/Skyfusion89/status/1101600562355859456)

"Cloudflare has me blocked." by @[JacobyDave](https://twitter.com/JacobyDave/status/1095411772851474434)

"too much of the web runs through CF & Google captcha, issues i raised before CF can launch MITM attacks, etc" by @[_Ashtar_Ventura](https://twitter.com/_Ashtar_Ventura/status/1045328755273723904)

"People really ought to stop using/trusting cloudflare. it's a disgusting centralized morally compromised CDN." by @[drwdal](https://twitter.com/drwdal/status/1113171715234902018)

"anyone who uses cloudflare as an authority is promoting corporatization and effectively introducing an opportunity for MITM" by @[drwdal](https://twitter.com/drwdal/status/1113172243981496320)

"Please stop using Cloudflare's CDN. They blocked my IP" by @[actual_mishaor](https://twitter.com/actual_mishaor/status/1112285872311934976)

"Cloudflare is a threat to the internet, and makes it harder for visitors of your website to protect their right to privacy." by @[Mr. Jeff](https://twitter.com/jeffcliff1/status/1105832648096862208)

"WTF! Just stop! Patreon Cloudflare and many others..." by @[umurgdk](https://twitter.com/umurgdk/status/1113323739221962752)

"I could not access the email because of cloudflare." by @[DouglasLindquis](https://twitter.com/DouglasLindquis/status/1115990146011344896)

"I have tried but API cannot be used because of Cloudflare" by @[miningpoolstats](https://twitter.com/miningpoolstats/status/1113810320710098944)

"Akamai states that 0.2% of Tor exit nodes had malicious requests, yet CloudFlare claims 94%" by @[musalbas](https://twitter.com/musalbas/status/717316725973389313)

"I'm starting to think that #CloudFlare is post-irony with their non-functional or just broken captchas" by @[ioerror](https://twitter.com/ioerror/status/715168927639343109)

"Why trust Cloudflare so much? I'm very wary of them handling so much Internet traffic." by @[ampajaro](https://twitter.com/ampajaro/status/1113508899397734401)

"You do not pay them anything. If you are not the customer, you are the product." by @[brettglass](https://twitter.com/brettglass/status/1119661919265116160)

"I don't trust a CDN provider that much. Simple as that. It's too big of an ask. This is why I will not use cloudflare." by @[Otto42](https://twitter.com/Otto42/status/1099863930640449536)

"There is no reason why people should trust you more than their ISP of choice." by @[pchapuis](https://twitter.com/pchapuis/status/1065601554131107840)

"It gets even better. SOLVE ALL THE CAPTCHAS" by @[metabubble](https://twitter.com/metabubble/status/715206027793338368)

"CloudFlare is gathering the web and centralizes it, I don't see any good aspect in this" by @[fuolpit](https://twitter.com/fuolpit/status/715942023564550144)

"At work, a very large client had major issues due to cloudflare misconfig. Don't use CloudFlare." by @[Dr. Roy](https://twitter.com/schestowitz/status/1036861493155962881)

"Fuck me CloudFlare Captchas are just impossibly difficult. Yet to succeed tonight … are you trying to block people as well" by @[badlydrawnrob](https://twitter.com/badlydrawnrob/status/602964063308636160)

"So you intermittently block me from viewing sites on your platform, and refuse to even address my questions about it." by @[2012_04_28](https://twitter.com/2012_04_28/status/314180120518750209)

"Go away cloudflare, don't block me from my own website!" by @[liamgooding](https://twitter.com/liamgooding/status/115736397645746176)

"You successful manage to block me, and fail to display why" by @[kaareal](https://twitter.com/kaareal/status/42885552793595904)

"Requested a refund. You guys refuse to do so. Trying to call goes to voice mail and support by email give me lame excuses." by @[thenitai](https://twitter.com/thenitai/status/1116433441598451712)

"genau von denen bin ich wenig begeistert..." by @[r00tthebox](https://twitter.com/r00tthebox/status/1115627988924751873)

"Ist übrigens die IP-Adresse von Cloudflare für die Homepage der JusoSchweiz, die ihr das blockiert." by @[sacovo](https://twitter.com/sacovo/status/1093541275326910464)

"Dass CloudFlare selbst TOR blockiert, ist denen wohl nicht aufgefallen." by @[kamikazow](https://twitter.com/kamikazow/status/570943630539689984)

"Cloudflare blockiert TOR-Exit-Nodes." by @[schild202](https://twitter.com/schild202/status/557770010450726912)

"Sites on Cloudflare blocked me upon browsing. Please fix." by @[lino_almojuela](https://twitter.com/lino_almojuela/status/742721270861553665)

"Achievement Unlocked! Cloudflare has blocked me. Oh, whatever shall I do???" by @[thisisgab2](https://twitter.com/thisisgab2/status/1062518618087481345)

"It's blocked by Cloudflare for me" by @[motters](https://twitter.com/motters/status/723902453939339264)

"My access to all cloudflare websites seem to be blocked." by @[MrDHat](https://twitter.com/MrDHat/status/980777153116569600)

"CloudFlare doesn’t like me… I have been ‘blocked’ by CloudFlare…" by @[bryanbrake](https://twitter.com/bryanbrake/status/706958626154393601)

"CloudFlare would not and won’t let me connect to & open that link." by @[ZEPHYoRUS](https://twitter.com/ZEPHYoRUS/status/1085240244872597504)

"Torをホワイトリストに追加していてもブロックしてくるCloudflare" by @[CheenaBlog](https://twitter.com/CheenaBlog/status/1042413641449005056)

"CloudFlareのCDNをウイルスバスターがブロックしちゃって見れない" by @[_iro](https://twitter.com/_iro/status/734991531724017664)

"Error 520と表示されてページが見れない。" by @[halpas_blog](https://twitter.com/halpas_blog/status/843401976280829953)

"ここ数日ずっとCloudFlareのerror 522で見れない。" by @[usunekoserv_pub](https://twitter.com/usunekoserv_pub/status/689752546991181824)

"IT Cell found a way to secure their website: they banned French IPs" by @[fs0c131y](https://twitter.com/fs0c131y/status/1110989388295467008)

"Cloudflare thinks I'm a robot and is blocking the most annoying preflight requests without letting me do a captcha." by @[enjalot](https://twitter.com/enjalot/status/1107085704591347712)

"It's disappointing that you use CloudFlare to reject some visitors to your website." by @[DeborahPeasley](https://twitter.com/DeborahPeasley/status/1087758565891166208)

"How can you reject abuse complaints? My abuse / theft report about one your clients was rejected because I am not a #cloudflare customer." by @[Mitchell_Miller](https://twitter.com/Mitchell_Miller/status/1064605521272033285)

"Solving captcha puzzles for Google every single time due to your Cloudflare protection just ruins the whole experience." by @[chowdhuryrahul](https://twitter.com/chowdhuryrahul/status/1115132766603972609)

"I am unable to login to android app, tried using credentials, reinstalling app, nothing worked." by @[prasadthombre](https://twitter.com/prasadthombre/status/1112950318537625600)

"Why you shouldn't use CloudFlare...they are run by a degenerate asshole who gave out personal information to GG, used to dox peeps" by @[CALMicC](https://twitter.com/CALMicC/status/555810642993876992)

"Cloudflare has traditionally thrown CAPTCHAs aggressively enough at VPN users to degrade the browsing experience." by @[mbarnath](https://twitter.com/mbarnath/status/1112969137389617152)

"Very few websites aren't using it meaning the majority of warp traffic is easily decrypted." by @[nathanielrsuchy](https://twitter.com/nathanielrsuchy/status/1113120866672525314)

"Cloudflare captcha is annoying af" by @[YourAnonNews](https://twitter.com/YourAnonNews/status/762340768845598720)

"What’s up with the forced #CloudFlare captcha page today?" by @[Aybara](https://twitter.com/Aybara/status/1113462534172946432)

"CAPTCHA protest outside CloudFlare rightscon party!" by @[ageis](https://twitter.com/ageis/status/715353166666027008)

"The truth behind Google Captcha, Analytics, CloudFlare is Covert Espionage and Mass Surveillance." by @[Casey_Comendant](https://twitter.com/Casey_Comendant/status/1116417797007921152)

"#Cloudflare is always the most frustrating part of my day. #dontblocktor" by @[jbrooks_](https://twitter.com/jbrooks_/status/715185447886938112)

"C'mon CloudFlare... I really don't feel like training your AI today" by @[trevorpaglen](https://twitter.com/trevorpaglen/status/728330578781712384)

"3 hours and counting. #Cloudflare is even worse in less developed countries. #dontblocktor" by @[jbrooks_](https://twitter.com/jbrooks_/status/715232157023608832)

"Tor CloudFlare Captcha #TellASadStoryin3Words" by @[YrB1rd](https://twitter.com/YrB1rd/status/834539628791209984)

"CloudFlare is such trash, is there anything comparable that DOESN'T suck???" by @[radix42](https://twitter.com/radix42/status/834543824047190016)

"Your blog post wasn't worth the cloudflare captcha i solved to read it" by @[agentdero](https://twitter.com/agentdero/status/707293903418822656)

"Cloudflare's aggressive use of google captcha basically means the trade-off of using a vpn is training google's self-driving cars." by @[frnsys](https://twitter.com/frnsys/status/1072504815152959488)

"cfc;dr = cloudflare captcha; didn't read." by @[mrphs](https://twitter.com/mrphs/status/710867596959227904)

"Each time I fill out a Cloudflare captcha I grow slightly more suspicious that I may in fact be a robot..." by @[projectgus](https://twitter.com/projectgus/status/979152953297715202)

"Select grass, copy-paste text, click "not a robot", select storefronts. Not getting better." by @[shiromarieke](https://twitter.com/shiromarieke/status/785557109962317825)

"Getting the Cloudflare captcha for everything today!" by @[TheHodge](https://twitter.com/TheHodge/status/1009091924639322114)

"Tor user in Iran can't read content behind CloudFlare" by @[attractr](https://twitter.com/attractr/status/717790178786586625)

"I gave up after the text captcha step." by @[jordansissel](https://twitter.com/jordansissel/status/692508531455582209)

"Why don't you just commit to only using Tor Browser for all of next week and see what it's like for yourself?" by @[hdevalence](https://twitter.com/hdevalence/status/698967506057347072)

"Every time I have to do an image Captcha it's a dead giveaway that Cloudflare services are used." by @[StateExempt](https://twitter.com/StateExempt/status/1111613302914908160)

"I just encountered a site with a fake #Cloudfail #CAPTCHA dialog to trick people reflexively clicking on it." by @[Nin_99](https://twitter.com/Nin_99/status/1112174288323383299)

"You're making the use of your site pretty difficult for #TOR users." by @[Sebastian Bürgel](https://twitter.com/SCBuergel/status/1114492792875700225)

"What, CLOUDFLARE? The company known for doxxing people reporting hate sites for ToS violations?!" by @[Abstruse](https://twitter.com/Abstruse/status/897966838029717505)

"our office IP always show one step captcha when access on cloudfare web. why this happen?" by @[dhaphie09](https://twitter.com/dhaphie09/status/1087710026989326341)

"I have to enter a captcha to get on any of the sites you protect." by @[gtklocker](https://twitter.com/gtklocker/status/1057760320809299975)

"Why on earth would a sane "user" use Cloudflare. Does the DNS get a captcha?" by @[cabshack](https://twitter.com/cabshack/status/982762341371609088)

"Why am I suddenly seeing you bullshit CAPTCHA tests when I'm going to websites I've been going to for years?" by @[Lex_the_Tex](https://twitter.com/Lex_the_Tex/status/948169434081964032)

"Can someone at Cloudflare explain to me why your default behavior is to present a captcha to anyone accessing over Tor?" by @[_djmoch](https://twitter.com/_djmoch/status/902309405865250816)

"Cloudflare used to assist in doxxing and are unlikely to help, even with direct threats." by @[justkelly_ok](https://twitter.com/justkelly_ok/status/898237338559565824)

"Here's a harassment forum mocking a doxxing target for asking Cloudflare to help her." by @[justkelly_ok](https://twitter.com/justkelly_ok/status/898239323245432832)

"I dislike Cloudflare they always block my VPN which I use to keep ads and trackers at bay" by @[nonesuchzach](https://twitter.com/nonesuchzach/status/782238922009042948)

"Ugh, anoying services. You block me because i use a VPN, WHY?" by @[_ruis_](https://twitter.com/_ruis_/status/827105441264054272)

""#Cloudflare is the Great Decentralised Fire Wall. I can circumvent GFW, but still can't read anything because of GDFW." —a Chinese activist" by @[isislovecruft](https://twitter.com/isislovecruft/status/697429362568597505)

"I get captcha loops, supposedly wrong answers, very very frequently. I don't log every Ray, but surely you" by @[thomaskerin](https://twitter.com/thomaskerin/status/697432585618726912)

"You now seemingly have every single IP block for PIA's VPN service in the US at least temporarily blocked via Cloudflare" by @[theonlymattlach](https://twitter.com/theonlymattlach/status/892201755035013121)

"Plz stop using Cloudflare.They block VPN and proxies ment to enhance our privacy." by @[claw137](https://twitter.com/claw137/status/892397825245536257)

"threatening to harass and dox my partners because I told them to remove our address" by @[ZJemptv](https://twitter.com/ZJemptv/status/898299709634248704)

"The people behind cloudflare used to make network honeypots before starting cloudflare. I'd think twice before using this service." by @[twiet](https://twitter.com/twiet/status/980798126800982017)

"Your cloudflare-hosted site blocks my VPN so I can't even buy your VPN while using a competitor." by @[focusaurus](https://twitter.com/focusaurus/status/942968457909456898)

"Most use Cloudflare+CAPTCHAs to filter." by @[r0h1n](https://twitter.com/r0h1n/status/778788564519845888)

"TorGuard uses CloudFlare They are #antiprivacy and block #VPN users!" by @[SPC_Bitcoin](https://twitter.com/SPC_Bitcoin/status/702699563342106625)

"One large company (CloudFlare) deciding who can visit what." by @[OhNoItsFusl](https://twitter.com/OhNoItsFusl/status/680144362773319681)

"I am not a robot. TURN OFF YOUR FUCKING #CLOUDFLARE BULLSHIT. #CloudflareIsEvil" by @[AnalSystemist](https://twitter.com/AnalSystemist/status/810912761035313154)

"So CloudFlare you block my surf because I'm behind a vpn protection?" by @[manuzful](https://twitter.com/manuzful/status/677474770867351553)

"In hindsight, I'm an idiot for not taking the decrypt/recrypt into account" by @[Firesphere](https://twitter.com/Firesphere/status/756693331762184192)

"any dev: to see how obscene they are, imagine getting captchas going onto github, twitter, Facebook" by @[thomaskerin](https://twitter.com/thomaskerin/status/753911221091434496)

"What if you're unable to use them? What if you're banned from using their services?" by @[Zemmiph0bia](https://twitter.com/Zemmiph0bia/status/1119398811674112000)

"Is anyone else unable to load images in Twitter’s iOS app while using Cloudflare’s 1.1.1.1 app?" by @[jabberwik](https://twitter.com/jabberwik/status/1116485375936139265)

"My hunch is the same for any company that wants to centralize traffic. Too much power on their hands. Today's people or techies *may* have good intentions, tomorrow's managers or owners may not." by @[lt3vaio](https://twitter.com/lt3vaio/status/1053512588913528833)

"unable to load our documentation website." by @[_notnathaniel](https://twitter.com/_notnathaniel/status/1116136910064242688)

"Currently I am unable to access the Cloudflare Support portal" by @[lemmertwitt](https://twitter.com/lemmertwitt/status/1116087286708547592)

"You people realize that CloudFlare exists only because you people are unable to ween yourself from 20 year old Apache technology." by @[ErrataRob](https://twitter.com/ErrataRob/status/546916521402855425)

"Good morning, we are unable to pull data from FirstCryptoBank due to the addition of cloudflare to their API. All of the coins listed on FirstCryptoBank, have the wrong value." by @[profitbotpro](https://twitter.com/profitbotpro/status/1064855654249177090)

"You send every HTTPS request from my browser to Cloudflare. They decrypt that HTTPS traffic." by @[jamesconroyfinn](https://twitter.com/jamesconroyfinn/status/811276047085957120)

"Never heard of cloudflare until network connectivity denied me" by @[1nzimande](https://twitter.com/1nzimande/status/690223994038030337)

"Access denied. What have you done, the CloudFlare cyberpolice are now out to get me!" by @[nyarth](https://twitter.com/nyarth/status/287603063668228096)

"You differentiate humans from bots." "Well fuck that." by @[magi093](https://twitter.com/magi093/status/1097480616050860032)

"Does anyone know if Saudi Arabia likes to filter Cloudflare?" by @[ralphholz](https://twitter.com/ralphholz/status/859565090395144192)

"Really wish you didn't block my IP with Cloud Flare. I'd join and support if I could." by @[DeborahPeasley](https://twitter.com/DeborahPeasley/status/801186753746903041)

"Get your shit together. Stop treating all Tor users like criminals." by @[slashdashdot](https://twitter.com/slashdashdot/status/715639099130908674)

"Link uses Cloud Flare to block IPs of those of us who browse anonymously" by @[DeborahPeasley](https://twitter.com/DeborahPeasley/status/568445705020420096)

"I was just replying to a MySQL support thread on SpigotMC and cloudflare's "you have been blocked" came up. lol, tryin to be helpful and getting the boot instead." by @[Floris](https://twitter.com/Floris/status/921170022021545984)

"Someone tell me how to bypass cloud flare so I can add..." by @[Shh_Its_Raghav](https://twitter.com/Shh_Its_Raghav/status/967888191201185793)

"Today I learned that Cloudflare is doing more to censor the internet than the Great Firewall of China" by @[pabloduboue](https://twitter.com/pabloduboue/status/682394633641549829)

"If you're a daily Tor user like me, you should get used to contacting the services you use and ask them to #DontBlockTor" by @[yawnbox](https://twitter.com/yawnbox/status/847182068853428225)

"Uses Cloudflare as MITM, blocks torproject, uses Google Adsense and Analytics to track you. This service is bullshit lies!" by @[MacLemon](https://twitter.com/MacLemon/status/793935353568694272)

"Please unblock my IP address. I’m having to enter CAPTCHA for every website I visit" by @[itixx](https://twitter.com/itixx/status/974463968269033472)

"Receiving a blocked ip error from cloudflare, why is that happening, can you unblock this ip? We never entered any wrong data" by @[entequak](https://twitter.com/entequak/status/906125031599722496)

"Can #cloudflare fix their mail.... All mail from that domain is flagged as spam by gmail" by @[miekg](https://twitter.com/miekg/status/712769672408612864)

"Please, unblock me on x. My ip is: x. It's because i'm using proxy." by @[S60Team](https://twitter.com/S60Team/status/789740374810095616)

"You aren't making anything any easier. Unblock direct IP access for a day, will you?" by @[simAlity](https://twitter.com/simAlity/status/789544967261736960)

"It seems your site is behind Cloudflare and is blocking Tor. Could you unblock it?" by @[qbi](https://twitter.com/qbi/status/761688391075434497)

"Please unblock your site, and don't use Cloudflare #dontblocktor" by @[motters](https://twitter.com/motters/status/747523232039866368)

"One more stepというのが出た Cloudflareってなんやねん" by @[zofeee](https://twitter.com/zofeee/status/1114998054770302976)

"Even though its your website, you are still restricted on how big a file you can upload to it if you use CloudFlare." by @[MajorLinux](https://twitter.com/MajorLinux/status/1088454705515134977)

"I am tired of this stupid Cloudflare Human Verification. Even though I am logged in and been using Agar.io  without any issue why does this human verification comes in annoying way that I have to verify in each and every gaming session? Has robot grown so much?" by @[mysterychemical](https://twitter.com/mysterychemical/status/1059297971387408385)

"Why am I being blocked all the time ? I am NOT a robot !" by @[JorgSchaper](https://twitter.com/JorgSchaper/status/697465639238246400)

"People are increasingly getting tired of little tyrants like you attempting to take away their freedoms." by @[Flavius_Amalek](https://twitter.com/Flavius_Amalek/status/1062372493946769408)

"Cannot access this site with TOR due to cloudflare. i got tired after having to do 20 verifications. No story's worth that kinda nonsense." by @[TheOnlyHapax](https://twitter.com/TheOnlyHapax/status/1045901955393097728)

"I am getting your "one more step attention required" problem when i try to access certain websites. I did not have this problem yesterday, and i dont know what might cause it." by @[edwin_demker](https://twitter.com/edwin_demker/status/1087764376411664384)

"Tor Browser experience: CloudFlare captchas all day, every day. Getting so tired of this. The "world wide web" is totally unusable over Tor." by @[attractr](https://twitter.com/attractr/status/763773075628388353)

"Captchas keep failing, so I am on attempt number 7 or 8 to access the website I need to reach today." by @[attractr](https://twitter.com/attractr/status/763773381967765504)

"Really tired of CloudFlare and having to constantly delete my cookies." by @[_Aqua_Buddha_](https://twitter.com/_Aqua_Buddha_/status/156124049213374464)

"I am *really* tired of convincing Cloudflare I'm not a robot. I wish they were a bit smarter about remembering." by @[_fconti](https://twitter.com/_fconti/status/920119467631435777)

"I've seen the "Cloudflare got hacked update your password" thing about 6 times now and I'm tired" by @[kipptune](https://twitter.com/kipptune/status/835139810956197888)

"Cloudflare got rekt and many sites including Discord is down so ur bored, hot, tired, and missing ur friends pls talk to me I am slowly dying but not that slowly" by @[Savage_ssb](https://twitter.com/Savage_ssb/status/1013266673678389249)

"Oh I'm tired of seeing this WordPress / CloudFlare / website error message.... :-(" by @[wfryer](https://twitter.com/wfryer/status/556854625044025344)

"you can't read Cloudflare's anti-torproject blog post in Tor Browser because of their own blocking" by @[hdevalence](https://twitter.com/hdevalence/status/715324308860567553)

"I HATE CLOUDFLARE GET RID OF IT ASAP!!!" by @[justinledvina](https://twitter.com/justinledvina/status/235959064646017025)

"I've had a issue for a while now where I get a box on OBS which says "One more step. Please complete the security check to access http://streamlabs.com " I went to your website and completed it with 2 different browsers but still the same thing." by @[NemeZiLive](https://twitter.com/NemeZiLive/status/1101399417024843777)

"Is it normal for attempts to open your site, to be met with One more step +capcha test? or a BS speedbump?" by @[TotsTeeter](https://twitter.com/TotsTeeter/status/1061732565185261568)

"Dear CloudFlare, You've some devs who're good people, but as a company you're ruining the Internet. Fuck you." by @[isislovecruft](https://twitter.com/isislovecruft/status/716383968799682560)

"The VRChat client keep requesting API and get 403 Forbidden because it cannot pass Cloudflare check (since VRChat client is actually robot, not human)." by @[YFangzhou](https://twitter.com/YFangzhou/status/1118506991217889281)

"Before blaming torproject for _your_ censorship—where's your data showing "abusive" traffic from Tor is higher than from non-Tor?" by @[isislovecruft](https://twitter.com/isislovecruft/status/716691471450169344)

"Oh fuck, for no apparent reason my IP provider gives me an IP that CloudFlare think it's a bot/spam, but I can't renew the IP." by @[EGaaraAE](https://twitter.com/EGaaraAE/status/957916041048358912)

"yes, they are a real pain in the neck when u use torproject on a daily basis." by @[chgans](https://twitter.com/chgans/status/585339647888269312)

"One more step Please complete the security check to access... I’m gonna blow my brains" by @[ahmMohs](https://twitter.com/ahmMohs/status/1045282363805249536)

"uh thats gonna b a cmd+w for me" by @[reifitaputs](https://twitter.com/reifitaputs/status/1036555891829403648)

"最近、「One more step」という画面がよく出てくるようになって、まともにネットが使えないんだけど、それはカンボジアまたはこの辺の地域の中国人ハッカー共が仕事してるってことでいいのかな" by @[sanchoku_iruma](https://twitter.com/sanchoku_iruma/status/1031794606692691968)

"How to have me leave your site. "One more step" Forcing users that us vpns to jump through hoops is lousy customer service." by @[nomadcat](https://twitter.com/nomadcat/status/1023585890512371712)

"A chaque fois que je veux consulter un site il y'a ça : Remplir un captcha" by @[B_Maroc](https://twitter.com/B_Maroc/status/1015990908565819392)

"I’m sure these makes your site very secure but they’re an absolute pain. Could you please turn them off? Three rounds of CAPTCHAS to read a blog is a bit disproportionate." by @[Aeyoun](https://twitter.com/Aeyoun/status/1012399198124347392)

"This is clearly indicate, these websites track us." by @[muththukrishna](https://twitter.com/muththukrishna/status/982786376235126786)

"The stupidest thing now a days #cloudflare asking #captcha everytime. #cloudflare i use Mac please dont give shitty explanation of #malware" by @[_danielchris](https://twitter.com/_danielchris/status/725628300253515776)

"Thanx 4 making it impossible 2 use your site because I use a vpn-connection. Please go away from cloudflare because they suck" by @[Curiosity63](https://twitter.com/Curiosity63/status/814219352824250368)

"You know what, dear Cloudflare? Go away. Please. Now. #DontBlockTOR" by @[ChristianTanner](https://twitter.com/ChristianTanner/status/900654039440003072)

"Nima (mrphs) talks about Tor's community team, noting: If you are accidentally blocking Tor via #CloudFlare, don't!" by @[torproject](https://twitter.com/torproject/status/756588578206081024)

"In my experience, there are a lot of CloudFlare users who are unwittingly blocking Tor." by @[georgemaschke](https://twitter.com/georgemaschke/status/756589185377632258)

"would you want a company like cloudflare tracking you using Tor? Avoid sites using cloudflare before you see the tracking" by @[FeelitWorking](https://twitter.com/FeelitWorking/status/756797304267472896)

"cloudflare also starting to block VPNs. They will be paid well one day for the user tracking database. LEO love it." by @[FeelitWorking](https://twitter.com/FeelitWorking/status/756797778630676480)

"Whoever wrote this summary of outstanding questions for #CloudFlare by the #Tor community is great" by @[ioerror](https://twitter.com/ioerror/status/702899307683442688)

"One person called it a MITM and it essentially is." by @[SPC_Bitcoin](https://twitter.com/SPC_Bitcoin/status/702939944633774081)

"Cloudflare is shit, dont use it" by @[raz_c](https://twitter.com/raz_c/status/436534703873884160)

"Oh shit guys, the Internet's here. CloudFlare #dontblocktor" by @[abditum](https://twitter.com/abditum/status/715353710780489728)

""protecting" your exchange with CloudFlare = permanent #MITM" by @[Bitcoin_Central](https://twitter.com/Bitcoin_Central/status/398741889681084416)

""This site has banned your access based on your browser's signature" I'm trying to acces my own site from my PC. Total #fail" by @[CdLCreative](https://twitter.com/CdLCreative/status/15538210176241665)

"Why is cloudflare blocking my access to a range of sites based on my “browser signature”?" by @[jasonwryan](https://twitter.com/jasonwryan/status/370716484537835520)

"jobscore is blocking me. Maybe an advanced AI to screen candidates by browser signature ;)" by @[utsengar](https://twitter.com/utsengar/status/636684259529388032)

"I wanted to tweet a pic about how overwhelmed with mail I am right now but it is #Cloudflare blocked so I'll just tweet #dontblocktor again." by @[shiromarieke](https://twitter.com/shiromarieke/status/707509515260973057)

"why does a 403 response state we banned access because of browser signature?" by @[friedcell](https://twitter.com/friedcell/status/562634060057821185)

"cloudflare dox's people who report illegal customer activity. Silicon Valley morality" by @[TinyPirate](https://twitter.com/TinyPirate/status/554718958176067584)

"I would have to whitelist pretty much my entire readership. It is banning based on "browser signature"" by @[skinnypupp](https://twitter.com/skinnypupp/status/113637917741678592)

"You can't contact the host. Cloudflare proxy servers hide the IP of the website making it impossible to determine who's hosting. You can report to their abuse, but I reported 100s of scammers sites and they did nothing" by @[phyzonloop](https://twitter.com/phyzonloop/status/1071901410307031040)

"CAPTCHA served to torproject users is even worse if you reject all cookies…" by @[ecodissident](https://twitter.com/ecodissident/status/587372365924925440)

"Companies (eg CF) can get bigger than some governments, and have a more meaningful reach of censorship than them" by @[jeffcliff1](https://twitter.com/jeffcliff1/status/1108144214976667648)

"Is #CloudFlare IP Blocking Causing More Harm than Good?" by @[torproject](https://twitter.com/torproject/status/731154593493716992)

"Trying to read an article online using a mobile. How much more human can it be?" by @[JKirstaetter](https://twitter.com/JKirstaetter/status/759615382659432448)

"I wonder if any of the CloudFlare's engineers are human enough to solve this captcha coz apparently I'm not." by @[mrphs](https://twitter.com/mrphs/status/581685782043209728)

"next-level annoying captcha from CloudFlare they don't even bother challenging me being a human anymore" by @[mrphs](https://twitter.com/mrphs/status/630770566023249920)

"What did I do to make you believe I'm not human enough to surf the web?" by @[mrphs](https://twitter.com/mrphs/status/588370301911695360)

"Irony trying to read Register article re:#Tor users & #CloudFlare: no HTTPS, CF/Google collusion & a broken Captcha!" by @[ioerror](https://twitter.com/ioerror/status/702467168064151552)

"Effects _everybody_ using Tor. I just hate the way it fools users into thinking it actually works. I close page immediately now." by @[strumpanio](https://twitter.com/strumpanio/status/702470236247818240)

"The #Register says sorry when you get a CAPTCHA. Do they understand that they place all their users under #CloudFlare surveillance?" by @[ioerror](https://twitter.com/ioerror/status/702470400404422657)

"I saw #cloudflare described as the "stop and Frisk" of the net recently." by @[toni_gon](https://twitter.com/toni_gon/status/702470660409454592)

"I never want to see “Attention Required!” #PITA" by @[kencf0618](https://twitter.com/kencf0618/status/702474459286863872)

"Watch out for Cloudflare, its controlled by the governments so they can follow each step you take on the internet. They are settled almost in the whole world even the poor countries." by @[EarthAngel_Lex](https://twitter.com/EarthAngel_Lex/status/1128723834071396352)

"Why are CloudFlare captchas for Tor-users so much harder when NoScript is enabled?" by @[thetorist](https://twitter.com/thetorist/status/586542477789769728)

"cloudflare has shared information from abuse form to site owner. They have retaliated and doxxed filer." by @[3bagsfull](https://twitter.com/3bagsfull/status/896985805817876480)

"Fuck CloudFlare. They chose to pass on people's contact details to 8chan when those people report" by @[ChrisWarcraft](https://twitter.com/ChrisWarcraft/status/886297432493092864)

"CloudFlare is a MITM for half the web. CloudFlare sniffs -all- traffic to 1.1.1.1. CloudFlare resolves your DNS queries. Thats a lot of freakin DATA. It's a freakin' GOLD MINE!" by @[uid_](https://twitter.com/uid_/status/980834950650957825)

"Cloudflare won't help future me dox and harass complainants! Whatever will I do?!" by @[yaelwrites](https://twitter.com/yaelwrites/status/860518424144887809)

"What the fuck happened why does cloudflare have to ask me if I'm a robot every single time I want to browse the web." by @[Oosran_ebooks](https://twitter.com/Oosran_ebooks/status/588615228067164160)

"Why am I unable to get past your home page? Few attempts, not even captcha loads...says 'attention required Cloudflare'" by @[uxgravy](https://twitter.com/uxgravy/status/1117395094041243651)

"If i ever meet a cloudflare employee in real life, they are going to get a finger in the eye" by @[dcuthbert](https://twitter.com/dcuthbert/status/685127882776129537)

"Hey i'm seeing this attention required again and again on the same website after I do the captcha." by @[rambokkisa](https://twitter.com/rambokkisa/status/725945637070495745)

"torproject users in SF should organize a protest outside cloudflare's office to demand its employees solve a few captchas before entering" by @[wiretapped](https://twitter.com/wiretapped/status/512988526544424960)

"Why is the CloudFlare memory leak bug not getting more attention?! It's potentially worse than HeartBleed." by @[withgallantry](https://twitter.com/withgallantry/status/836182424522539009)

"Isn't it time to #BoycottCloudFlare? Fuck CloudFlare" by @[YourAnonNews](https://twitter.com/YourAnonNews/status/585293954939232256)

"Stay the Fuck Away from CloudFlare" by @[AaronM](https://twitter.com/AaronM/status/554699462853947392)

"Why do I see a captcha or challenge (Attention Required) trying to visit a site protected by CloudFlare" by @[backtoschoolart](https://twitter.com/backtoschoolart/status/581161064542400513)

"Why does this notification ("ATTENTION REQUIRED/CLOUDFLARE") pop up instead of the site I want to go to? Annoying." by @[jovefrancisco](https://twitter.com/jovefrancisco/status/520550545652609024)

"Why do I get CloudFlare ""Attention Required" CAPTCHA interstitial pages for several websites" by @[pforret](https://twitter.com/pforret/status/450948617768292352)

"Everytime I go to a website I get attention required and it has me do a captcha, why are you blocking me from sites?" by @[syzransport](https://twitter.com/syzransport/status/413243634905911296)

"Everytime I go to a website I get attention required and it has me do a captcha, why are you blocking me from sites?" by @[geevaveeri](https://twitter.com/geevaveeri/status/372325413738934272)

"Dear #Cloudflare users: YOU ARE IDIOTS and I'm not a FUCKING ROBOT." by @[AnalSystemist](https://twitter.com/AnalSystemist/status/804437882240901121)

"Your captcha currently appears to be blocking Tor users. After clicking 'Im not a robot' it always says 'Try again later. Your sending automated queries'" by @[JungleBoy03](https://twitter.com/JungleBoy03/status/978860372760449025)

"can you please tell CloudFlare your users are not robots?" by @[vrde](https://twitter.com/vrde/status/744821179697901568)

</details>

------

<details>
<summary>_click me_

## Mastodon
</summary>


> Too many to list here. It is IMPOSSIBLE to list them all! [See for yourself](https://mastodon.social/tags/cloudflare).


"Save the Internet, boycott #Cloudflare" by @[Ajz](https://mastodon.nl/@Ajz/103712485183183547)

"I used the "shocking" fact that I noticed that mastodon.social is using "bad-cookie" #Cloudflare, to once again remind people that mastodon.social is overloaded." by @[Ajz](https://mastodon.nl/@Ajz/103709379040402180)

"Cloudflare silently deleted my DNS records" by @[hackeroo](https://botsin.space/@hackeroo/103715714895415788)

"NPM’s outage and Cloudflare’s communication... what a mess" by @[zerok](https://chaos.social/@zerok/103681659450407389)

"It is quite apparent that the internet needs to move away from #cloudflare 
I ran an #tor exitnode for a few hours (mostly for testing combined with a miss in config) and now does cloudflare prompt me with a #captcha every, single, minute. Do youself and your user a favour, and move away from cloudflare" by @[selea](https://social.linux.pizza/@selea/103692664037914594)

"It seems to me #tusky still try to connect to #cloudflare despite using http proxy." by @[Br0m3x](https://social.weho.st/@Br0m3x/103674328334042541)

"Sehe gerade, ein Moodle, in das ich mich einlogge, nutzt Cloudflare. Wie ist das denn aus Datenschutzsicht zu bewerten?
Kennt sich da jemand aus? In der Datenschutzerklärung des Moodles findet sich da nichts drüber." by @[D22](https://social.tchncs.de/@D22/103634742838191448)

"You oppose #Google & #Amazon at least in part due to their attacks on #privacy.  Yet, you're in #CloudFlare's privacy-abusing #walledgarden yourself. You should ditch mastodon.social and find a node that respects your values." by @[aktivismoEstasMiaLuo](https://activism.openworlds.info/@aktivismoEstasMiaLuo/103591534339447704)

"I'm tired of people going on about Cloudflare. It's not a normal tired either. It's a "please get the fuck out of my face if you're going to say one more word because I am just done." A friend of mine said I was a shit service provider because I would rather stop running everything than put my applications behind a MITM and compromise user privacy." by @[amolith](https://masto.nixnet.xyz/@amolith/103650166871589343)

"Würde ich gerne, aber change.org lässt mich nicht auf die Webseite, weil ich mich momentan im Netz der "Leaseweb Deutschland GmbH" befinde (screenshot:  ASN  28753). change.org sollte dringend ihren Server konfigurieren oder besser noch den Hoster "Clownflare" wechseln." by @[Wolfram](https://norden.social/@Wolfram/103608344157032468)

"Today first I successfully did a search on opensubtitles.org with Firefox and noticed that this site uses CloudFlare. Then later on I tried to do a search on opensubtitles.org with the #Tor browser. I got the Google ReCaptcha, and after solving that for more than 10 to 15 times, where it seemed to give me access a few times, I was still not able to perform a search at all (meh) :_catsad: When will people realize that the "free of charge" for 1 website DDOS protection by #CloudFlare is not at all a cool and "free" option to choose for. It is more like selling your soul and poisoning the Internet. Also : A few months ago I had to use the CloudFlare web admin interface for a friend. I added a subdomain for an admin login page for the website of my friend. Then I continued working on the site and suddenly noticed in the logs that the brand new subdomain was visited by several different ones, including a tor node. At first I got confused by it, and I found it very unpleasant, but in the end my conclusion is that CloudFlare is just dodgy. I hope that affordable CDN and DDOS protection alternatives will grow and vanish CloudFlare in the end." by @[Ajz](https://mastodon.host/@Ajz)

"If you're at a small scale, why would you pretend you need the services that CloudFlare provides in the first place? I understand that people watching MSM and have little knowledge of IT will think that everyone in the world is constantly being DDoSed every time they make a post on their blog, but that's just not the case. " by @[fglt](https://soc.fglt.nl/objects/0e1fe9a1-1099-4091-899b-69130f309a6e)

" I understand the marketing team has a lot of other "functionalities" that you can list in order to make it look less bad, but you're still doing all your users a massive disservice by employing CloudFlare inbetween them and your service. No matter how many cool features you think you're getting for gratis, you're still handing all your customer data to them, essentially through having them become the "middle man" of the "man in the middle" scheme. Paying for that "service" is even more ridiculous from any sane perspective. " by @[fglt](https://soc.fglt.nl/objects/e3a57b9a-a80d-4d2b-a44f-de8b166dba00)

"If you had ever trusted CloudFlare to work in the interests of it's "customers", you were a fool to begin with. " by @[fglt](https://soc.fglt.nl/objects/c91371f7-2a56-4c5f-b5b1-1e1942a6e953)

"Cloudflare and Google captcha are the worst enemies of tor." by @[lexa](https://mstdn.io/@lexa/103356277680824722)

"They may trust you, but do they trust Cloudflare?" by @[IceWolf](https://meow.social/@IceWolf/103358288466314935)

"BTW, it doesn't matter who your CA is, the tunnel between outsiders & CF's site always terminates at CF.  So, for example, if you were a mastodon.social user, CF would see your username & unhashed pw full stop." by @[resist1984](https://social.privacytools.io/@resist1984/103359038780017782)

"#CloudFlare blocks from the reader's side, not the author.  If Alice blogs on a CF site then Bob is blocked from reading it if CF objects to Bob's IP address-browser pair.  And that's not theoretical -- it's certain. CF dictates who can reach who.  CF also pushes #surveillanceCapitalism via a forced #Google-served #CAPTCHA (depending on IP-browser pair).  Using a CF service gives them power.  Twentyfivebux unwittingly supports #centralization & empowers CF by using mastodon.social." by @[resist1984](https://social.privacytools.io/@resist1984/103358978513641848)

"Sure there are many instances.  Some are respecting of #freedom & #privacy, and some are not.  #CloudFlare is an abuser of privacy & #netneutrality.  You've specifically endorsed a CF node (mastodon.social). As a "privacy foundation", you should know better and you should set a better example." by @[resist1984](https://social.privacytools.io/@resist1984/103359630910309671)

" #CloudFlare sites like mastodon.social are profoundly stupid choices to endorse. Even if you neglect the #Tor hostility & #VPN weakness, CF still sees all traffic.  Also, using CF feeds a #privacy abuser financially, which works against your alleged cause. " by @[resist1984](https://social.privacytools.io/@resist1984/103359717636003316)

"I'm more concerned with Clouflare's dominance (~75% of high-traffic websites on internet) regarding caching HTTPS in exchange for website owners private keys (in effect silently compromising  all visitors to said sites as well). After digging into Cloudbleed and the technical solution at Cloudflare making this security bug possible, along with their transparency  (www.cloudflare.com/transparency/), it became apparent that this is the biggest orchestrated HTTPS weakness out there serving US government." by @[modrobert](https://qoto.org/@modrobert/103349511813674970)

"They removed "Cloudflare has never terminated a customer due to political pressure"" by @[amolith](https://masto.nixnet.xyz/@amolith/103353026740986457)

"Wenn futurzone.at jetzt auch noch damit aufhört, ihren kompletten Traffic durch #Cloudflare zu jagen, kann man sie bei solchen Artikeln auch ernst nehmen!" by @[kromonos](https://metalhead.club/@kromonos/103347438033060779)

"In der Tat. #Cloudflare scheint wohl Verbindungen zur #NSA zu haben?" by @[display](https://f.haeder.net/display/adbba6c6-945d-fe77-7156-80f533821135)

"You should switch to a #decentralized node before building followers" by @[resist1984](https://social.privacytools.io/@resist1984/103342936171766997)

"I am depressed. I'm seeing so many fantastic open source projects using subpar services like Cloudflare and Digital Ocean bEcAuSe MaRkEtInG and they don't realise what kind of ecosystem they're buying into." by @[amolith](https://masto.nixnet.xyz/@amolith/103343274100985996)

"On a more serious note, I know a lot of people believe using Cloudflare automagically makes their site more secure because of the green lock indicating that "fact". I can understand it more for people using crappy webhosts that you can't get certs from Let's Encrypt with. For people who run their own servers, there is no excuse for putting CF in the middle and giving them access to all their users' traffic. It's just stupid." by @[amolith](https://masto.nixnet.xyz/@amolith/103315936594282075)

"This repo outlines all the reasons I would give in greater detail than I have time to go into. I've contributed a bit but the maintainers do a fantastic of keeping it very up-to-date with the current situation" by @[amolith](https://masto.nixnet.xyz/@amolith/103315947408760507)

"Fantastic. I'm connected to a friend's VPN (all of two users) while downloading some movies and I went to view someone's profile but the instance seems to be behind #Cloudflare cloudflare_red They've been getting more and more aggressive lately" by @[amolith](https://masto.nixnet.xyz/@amolith/103315876773459246)

"Why the hell do people use Cloudflare on their Mastodon instances anyway...? It makes even less sense to me than other usecases." by @[polarisfm](https://www.librepunk.club/@polarisfm/103315896439633426)

""just solve the captcha, it works, we promise!" cloudflare are liars and censors " by @[opal](https://anime.website/objects/cc7f93e8-9114-4a11-97e8-a92f75c280fe)

"I swear it seems like half the sites on the web are having a Cloudflare related outage today.  Who woulda thought trying to route half the web through a single point of failure wouldn't ever be a problem?" by @[BalooUriza](https://meow.social/@BalooUriza/103330872309479681)

"Congress has put publically funded literature under  CreativeCommons licensing (when CC's license text sends ppl to CloudFlare's walledgarden).  Then he reports this using sparcopen.org, a CloudFlare site. It should have been publicDomain.  Public funding = public domain. This is just layers of hypocrisy." by @[batalanto](https://todon.nl/@batalanto/103326880978571285)

"Having CloudFlare MITM your website is like shutting the door in my face. No, I will not "Select all images with street signs." But I will blacklist your site in my browser." by @[notice](https://shitposter.club/notice/949325)

"Point is: no, you don't need #CloudFlare." by @[rysiek](https://mastodon.social/@rysiek/103240081915169502)

"#cloudflare is having problems again, hm?" by @[musicmatze](https://mastodon.technology/@musicmatze/103239327000559082)

"Anyone marginally concerned about #privacy needs to take a massive step back at this point. Bitchute will not load videos for #Tor Privacy #Browser users." by @[dsfgs](https://activism.openworlds.info/@dsfgs/103232653381567023)

"Sorry, i'll never sign a change.org #petition - it's a #privacy-abusing #CloudFlare site.  Use openpetition.org for petitions important enough for everyone to access." by @[resist1984](https://social.privacytools.io/@resist1984/103204933856595091)

"Tails is still useful but devs are not trustworthy." by @[bojkotiMalbona](https://infosec.exchange/@bojkotiMalbona/103188285582213699)

"Can you trust CloudFlare with your personal data?" by @[Edent](https://mastodon.social/@Edent/103175943251084170)

"wenn sich der Tagesspiegel nicht hinter #cloudflare verstecken würde, würde ich ihn wahrscheinlich auch lesen" by @[kromonos](https://metalhead.club/@kromonos/103136278965173255)

"Not that I expect Cloudflare to be doing anything malicious, I'm just a little uncomfortable where passwords or anything sensitive are involved." by @[IceWolf](https://meow.social/@IceWolf/103116651269844076)

"To MITM an entire domain with CF you give Cloudflare authority over the entire domain’s DNS resolution. This is the best indicator of a full domain MITMing." by @[Trysdyn Black](https://marf.space/objects/6a07884a-cfe8-4eeb-9486-63ffe791259f)

"The corp., project + devs lost my trust once they pushed EME, Pocket, Studies, censorship and #Cloudflare crap." by @[NullWhereMan](https://social.bobcall.me/@NullWhereMan/103115226792755635)

"It's a bit of a #hypocrisy for #privacytoolsIO to subject their donors to a #Cloudflare site. No warning is given to tell their pro-privacy supporters that their name, address, and sensitive financial data will be shared with a #privacy abuser." by @[batalanto](https://todon.nl/@batalanto/103097101716393570)

"interessant, eine Website a la github, die die Umtriebe von #cloudflare dokumentiert" by @[alm10965](https://social.bau-ha.us/@alm10965/102670940812325433)

"plz avoid imgur.com - It's #CloudFlare'd." by @[aktivismoEstasMiaLuo](https://activism.openworlds.info/@aktivismoEstasMiaLuo/103131170879379868)

"The bad side-effect is that *many*  are registering on mastodon.social, which is centralized on #Cloudflare & ultimately detrimental." by @[batalanto](https://todon.nl/@batalanto/103127670786580635)

"I suggest leaving mastodon.social & choosing a different #Mastodon node.  Mastodon is about #decentralization, and the mastodon.social node is #centralized on #CloudFlare" by @[aktivismoEstasMiaLuo](https://activism.openworlds.info/@aktivismoEstasMiaLuo/103127507035816683)

"it's despicable that some mastodon & #gnusocial nodes appear on CF.  Those admins either don't grasp the #netneutrality purpose of federating or they fail to realize that CF is entirely counter to net-neutrality." by @[resist1984](https://social.privacytools.io/@resist1984/102598306130420507)

"99%+ have no clue why the #greatcloudwall is dangerous, probably 33%+ don't even know what it is." by @[jeffcliff](https://niu.moe/@jeffcliff/102599290674949891)

"OK #cloudflare hat mich blacklisted. Hmm, von cloudflare habe ich schon so manch schlechtes gehört ! Irgendwie ahnte ich doch so etwas, daß ich XXX.com nicht mehr nutzen kann." by @[alm10965](https://social.bau-ha.us/@alm10965/102670832938054598)

"GitLab sits behind Cloudflare and is hosted by Google – while Codeberg runs on Codeberg hardware and has no MITM. Much valued, especially for privacy reasons. Want more reasons against Cloudflare?" by @[IzzyOnDroid](https://mastodon.technology/@IzzyOnDroid/102662955566633818)

"Cloudflare is a great big "man in the middle" ifyouknowwhatimean" by @[shitposter](https://shitposter.club/notice/35788)

"OH GOD THAT'S CLOUDFLARE" by @[shitposter](https://shitposter.club/notice/1026961)

"Security: every site that uses it, Cloudflare is a proxy that intercepts all the traffic. It can therefore correlate individual user traffic across every site that uses/embeds cloudflare site/resources. Single point of security failure, from a global surveillance point of view. Usability: If you're a Tor user, it makes a huge part of the Internet unbearable because it constantly injects captchas to view sites every couple minutes, to "block abuse"." by @[shitposter](https://shitposter.club/notice/389221)

"Ha, and the instance which you use, serves images through Cloudflare, so I can't see your profile picture." by @[rmw](https://welovela.in/rmw)

"Now, whenever that pops up, I just close the tab – and ask someone else to update. I've got no time to play memory puzzles. Got more important stuff to do." by @[IzzyOnDroid](https://mastodon.technology/@IzzyOnDroid/102667841048225494)

"Good starting point: no fears of anything being blocked (so doesn't hurt to try), while great for "showing off". Like with that tooth paste ad: "Ugh, it's all red!"  Might get some people start thinking. Hopefully." by @[IzzyOnDroid](https://mastodon.technology/@IzzyOnDroid/102667729196429726)

"#Cloudflare is a #censorship company; it's all about money and profits, not #freespeech" by @[Dr. Roy](https://pleroma.site/objects/7b613725-4178-4e6b-881b-3cb080de0ecc)

"Only pedophiles use #Cloudflare" by @[yuni](https://patch.cx/objects/3de4abbb-6ff0-4014-b4df-e6e6cc7a407e)

"Please do reconsider when possible, I find Cloudflare to be dangrous." by @[utf8equalsX](https://fosstodon.org/@utf8equalsX/102582059701778332)

"destroy cloudflare" by @[ReplyGal](https://pleroma.soykaf.com/notice/9m549hhM8c2YwYhxfU)

"The timeouts it's been causing, apparently, seem to be worse than the DoS protection and such it's supposed to provide." by @[KatieBrink](https://pleroma.soykaf.com/notice/9m557HJJNNiWThrv2u)

"I don't get why none of these people understand how Cloudflare works and doesn't work." by @[zoe](https://sleeping.town/@zoe)

"PSA: mastohost.com, where for some reason – and I don't really know how this works – most images that are posted here on mastodon are hosted, is #cloudflared. That's very bad." by @[utf8equalsX](https://fosstodon.org/@utf8equalsX/102580647437254254)

"i do care about cloudflare not taking over the fediverse. im also weird about the masto.host instances (which incidentally use cf for media content)" by @[unknown](https://anime.website/objects/68c10712-6d65-43a0-aeb8-502dab87c730)

"Also mit nginx eine eigene Firewall bauen, anstelle, wie es andere machen, #Cloudflare zu nutzen." by @[kromonos](https://metalhead.club/@kromonos/102592478151563288)

"How to shut down the #bullshitweb? Switch off #Cloudflare & AWS, approx. 80% done. There's a Firefox (and Chrome) addon available at codeberg helping you decide what links not to click, as Cloudflare does a #MITM" by @[IzzyOnDroid](https://mastodon.technology/@IzzyOnDroid/102583975445779208)

"DDoS protection is good – but the price it comes at… They're said to play MITM in breaking up your encryption. And as I indicated: whenever they have an outage (and they had some), about 1/3 of "the internet" is unreachable…" by @[IzzyOnDroid](https://mastodon.technology/@IzzyOnDroid/102585623297173423)

"The PINE64 website is also #cloudflared. This means that #cloudflare terminates the secure connection, decrypts the traffic, and encrypts it again. That is terrible." by @[utf8equalsX](https://fosstodon.org/@utf8equalsX/102581004076074151)

"Everyone should boycott censorship-loving @Cloudflare and any groups associated with it that they are able. (Dailymotion) #Cloudflare #8chan #Censorship " by @[Styx666Official](https://mstdn.foxfam.club/@Styx666Official/102570516495655146)

"I found that over half the sites I tried to visit would not load at all unless I added exceptions, and some loaded but with broken elements. It seems to me it's not healthy for the #internet to have a single company controlling this much. They may be benevolent now, but what about the future?" by @[kezzbracey](https://mastodon.gamedev.place/@kezzbracey/102574455814372037)

"Well done, #Cloudflare. And thank you. Thank you for proving me right. You're a #surveillance and #censorship company. You start with nazis and you move on closer from that edge" by @[Dr. Roy](https://pleroma.site/objects/0f476d6e-aeef-4043-89c1-5db998b69497)

"And now #bigTech #cloudflare is deciding they need to be the gatekeeper of more speech. Because a crazy happened to post on one of their hosted sites???" by @[poncho524](https://mastodon.host/@poncho524/102567778634669022)

"Ich betreibe gleich seit fast 14 Jahren eigene #Server und hab noch keinen Grund dafür finden können, sie komplett hinter einen #Proxy von #Cloudflare zu stellen. Ganz ehrlich... Wer Cloudflare wegen "Sicherheit" nimmt, der sollte aufgrund mangelnder #Kenntnisse über #Serversicherheit seinen Server besser kündigen und sich einfachen Webspace zu legen!" by @[kromonos](https://metalhead.club/@kromonos/102562981524139639)

"Services like #CloudFlare are really nice with #PatriotAct and #NSA. #USHackers #whatawonderfulworld. CloudFlare create SSL certificates  for sites they cache. So have the wholme traffic unciphered locally, and send it to the remote server to be cached. So, they have clear and unciphered access to the whole content :  usernames   passwords  web content..." by @[popolon](https://mastodon.social/@popolon/102546838067285665)

"Google reCaptcha nicht erreichbar? ... Na dann viel Spaß in der #Hölle, freunde von #Cloudflare" by @[kromonos](https://metalhead.club/@kromonos/102543497886371728)

"the internet is a huge place even once you disable javascript and many websites stop working, and you use tor so the #greatcloudwall auto-blocks you." by @[jeffcliff](https://niu.moe/@jeffcliff/101894104472506869)

" Ich surf inzwischen fast ausschließlich über #TOR. Außer bei Betreibern, denen ic traue. Deswegen hab ich so oft ärger mit #CloudFlare. Nur wenige machen es z.B. wie nipos und deaktivieren den "TOR Schutz"." by @[kromonos](https://metalhead.club/@kromonos/102501955975549073)

"na klasse. Noch so eine deutsche Firma, die glaubt, der Datenschutz sei gewährleistet, wenn man den kompletten Traffic durch einen proxy in den USA leitet. Also das Linux Magazin, nicht tuxedocomputers
Man könnte echt meinen, die deutschen könnten ohne #Cloudflare nicht mehr leben " by @[kromonos](https://metalhead.club/@kromonos/102501662983987631)

"This is a good example why it is bad to rely on cloudflare in masses. It felt like the internet was dying for a minute there." by @[SolSoCoG](https://ieji.de/@SolSoCoG/102372262422393175)

"kann mir sicher beantworten, in wie weit #Cloudflare mit dem deutschen #Datenschutz vereinbar ist, wenn man eine #Homepage hinter deren #Proxy Server versteckt, deren Standorte in den #USA liegen? Und wie muss Cloudflare in dem Fall in der #Datenschutzerklärung angegeben sein?" by @[kromonos](https://metalhead.club/@kromonos/102497051015883486)

"Wir haben also eine „ethical hacker“ #Zertifizierung von der #NSA beworben und CloudFlare auf einer Seite. Was sagt uns das also über #CloudFlare aus? Für mich persönlich sieht das so aus, als würde CloudFlare die Daten direkt zur NSA weiter leiten" by @[kromonos](https://metalhead.club/@kromonos/102462408957007477)

"Das #Linux #Magazin nennt die Nutzung der #CloudFlare Reverse-Proxy Server als #CDN ... Wenn das CDN ist, was ist dann das Ausliefern einzelner Dateien über einen extra Server?
CloudFlare kann echt bald von jedem ein Bewegungsprofil erstellen, dagegen sind die Fahrzeuge von #Google Maps ein scheiß" by @[kromonos](https://metalhead.club/@kromonos/102475706977537396)

"Lustig, wie selbst #INWX in ihren Datenschutzerklärungen in keinster Weise #CloudFlare erwähnt." by @[kromonos](https://metalhead.club/@kromonos/102474883800543138)

"It's because cloudflare actively spies on traffic going through it, so any service connected to it is compromised by default. Even if the service itself is well-meaning and wants to respect privacy, just having cloudflare there ruins all of it." by @[OTheB](https://mastodon.technology/@OTheB/102457243116505761)

"Cloudflare had a unchecked regex expression they rolled out to live service without testing and it broke half the Internet." by @[Bluedepth](https://mastodon.technology/@Bluedepth/102457244936687401)

"Wenn ich mir die Möglichkeiten ansehe, die ich mit #nginx habe, Log Dateien zu schreiben, läuft mir echt ein eiskalter Schauer über den Rücken,weil ich mir ausmalen kann,welchen Preis Besucher von #Webseiten hinter #Cloudflare #Proxy Servern bezahlen. Wenn ich das ganze dann hoch rechne...Die könnten ein komplettes bewegungsprofil eines jeden erstellen. Und Google wird durch das Captcha direkt mit gefüttert.
Ich als Anbieter könnte echt nicht mit dem Gewissen leben,meine Besucher so zu verkaufen" by @[kromonos](https://metalhead.club/@kromonos/102458133671163498)

"Mit "Serverstandort Deutschland" zu werben, nutzt einen scheißdreck, wenn man den Server hinter den USA Proxys von #CloudFlare versteckt." by @[kromonos](https://metalhead.club/@kromonos/102457750251926436)

"Really disappointed and a little disgusted by the #Matrix team. Putting #Cloudflare spyware on their main instance, forcing every user to either go through the malicious wall or to use a homeserver that doesn't federate with matrix.org thus cutting itself off from just about everything. Will be giving #XMPP a go today. Wonder if that community has even the tiniest bit of respect for users." by @[OTheB](https://mastodon.technology/@OTheB/102456355379841384)

"Even the main instance uses Cloudflare. New users will probably just use the main instance for convenience." by @[RMW](https://linuxrocks.online/@RMW)

"I use to think it was cool that you can pay a little bit and have your own instance but it sucks that they put every instance behind Cloudflare." by @[amolith](https://masto.nixnet.xyz/@amolith/101918685210269597)

"Don't laugh when #cloudflare says “the network is the computer.” They mean #surveillance and they mean the surveillance goes all the way to the endpoints. Boycott them and their nefarious agenda." by @[roy](https://pleroma.site/objects/1ddf444d-0f2c-4d0d-bc11-e06f28efd5bc)

"I think I've found an alternative to #Cloudflare Liberapay. I'm going to set #Fosspay up this evening and start replacing Liberapay links on my website" by @[amolith](https://masto.nixnet.xyz/@amolith/102436150784521976)

"I'm sad that the #Crimeflare got deleted because they'd be having a field day today. #cloudflare" by @[DashEquals](https://ap.torlipen.net/objects/5f1b65fc-499d-4312-b20e-7b0cb9dd7ee2)

"All I want is a #privacy respecting messaging platform that does group conversations and voice chat, does that *really* have to be a big ask? Been really put off #Matrix with the #Cloudflare bullshit and their attitude towards it. There are whole papers on the issues with it. When will this fucking nightmare end? I need suggestions. What things do other people use for this stuff?" by @[OTheB](https://mastodon.technology/@OTheB/102456866393573493)

"Hi pixelfed! You just added a mandatory #Cloudflare ajax service to pixelfed.social? The page is empty now..." by @[antonlopez](https://mastodon.social/@antonlopez/102451687366539901)

"Ich nutz hauptsächlich #Tor. Zwar #heuchelt #Cloudflare Tor-Freundlich zu sein, z.B. mit eigener Onion Adresse. Aber standardmäßig blocken die Tor Verbindungen, sofern man sie nicht freigibt.
Zudem empfinde ich es nicht gerade als Datenschutzfreundlich, die Cloudflare Proxy Server zu nutzen. Vor allem, da Cloudflare dem Patriot Act untersteht." by @[kromonos](https://metalhead.club/@kromonos/102440347862158580)

"außerdem empfinde ich persönlich es als Wohltuend, wenn die #CloudFlare #Fanboys mal genauso auf ein Forbidden stoßen, wie #TOR User all zu gern bei CloudFlare" by @[kromonos](https://metalhead.club/@kromonos/102447153687932571)

"I think that I will stop using it, until they remove it from Cloudflare. I don't see it as something that is made for privacy if it decides to use Cloudflare for their main instance." by @[Robin](https://welovela.in/objects/456f3bba-ee24-48c5-92a3-7f80349dfbc3)

"angesprochen sollten auch nur jene werden, die keine Ahnung haben, aber dann so tun, als hätten sie sie. IT Blogger, die Anleitungen dazu bringen, wie man die #Tor Exit Knoten Liste herunter lädt um die IP Adressen dann zu blockieren. Weil #CloudFlare ja gesagt hat.
Und jene, die keine Ahnung haben ... denen ist die Meinung der wirklichen Experten scheiß egal. Die lauschen lieber auf jene, die mit ihrem gefährlichen Halbwissen das Internet verpesten." by @[kromonos](https://metalhead.club/@kromonos/102445550843368442)

"Hyperbola members who don't care about #Cloudflare, which disrepect the privacy over violation of rule 3 of the social contract" by @[HarvettFox96](https://hostux.social/@HarvettFox96/102414243375270600)

"Remember how the liars at #cloudflare pretended that had come under attack (causing major downtime), only later to admit that they broke their own service because they're technically incompetent. Avoid them." by @[schestowitz](https://pleroma.site/objects/a5533839-4678-4c06-975f-5ceceeaa793d)

"is this instance using #Cloudflare somehow? Was getting errors from it when trying to access Mstdn yesterday." by @[orbifx](https://mstdn.io/@orbifx/102399186438337881)

"They use #cloudflare for their Matrix.org and other servers they operate." by @[eo](https://masto.nixnet.xyz/@eo/102396215079147841)

"They talk about "#decentralization", while being behind #cloudflare. awesome" by @[selea](https://social.linux.pizza/@selea/102387933088472002)

"It's been estimated that today's #Cloudflare outage is effecting about 10% of all websites world-wide. If a lot of your online world is down right now, your word and research topic for the day is #decentralization" by @[raye](https://sunbeam.city/@raye/102372447641525998)

"Seems like the world's biggest MiTM is down and took a rather large number of websites with it" by @[amolith](https://masto.nixnet.xyz/@amolith/102372539142565301)

"So I guess #Liberapay is behind #TheGreatCloudwall now" by @[amolith](https://masto.nixnet.xyz/@amolith/102207994379806531)

"I want to thank #Cloudflare for removing themselves from the internet." by @[Chuculate](https://niu.moe/@Chuculate/102378374675476776)

"Yesterday it was #cloudflare today it's #facebook static content (images etc) don't load, seems like CDN problems too. Welcome to the centralized Internet..." by @[kravietz](https://social.privacytools.io/@kravietz/102378733712987218)

"Qué es Cloudflare, cómo funciona y por qué cuando "se cae" parte de #internet se viene abajo" by @[GeekTale](https://mastodon.social/@GeekTale/102376704390517130)

"Вчера упал #CloudFlare, из-за чего нарушилась работа крупнейших сервисов Интернета. Разработчики уже выпустили пост-мортем, из которого следует, что причиной падения стало – внимание – регулярное выражение, вызвавшее 100% нагрузку на CPU и, вследствие чего, множество 502-х ошибок в ответ на запросы." by @[Groosha](https://mastodon.social/@Groosha/102376274244244420)

"Why you shouldn’t use services like #Cloudflare to enable „high availability“ " by @[f2k1de](https://chaos.social/@f2k1de/102375564327003058)

"LMAO #cloudflare is down AGAIN?" by @[DashEquals](https://ap.torlipen.net/objects/4236a828-7ba9-4c2c-9c40-c217c1f1d4e3)

"Natürlich bezahlst du nichts, dafür aber deine Besucher, die all ihre Daten und #Metadaten einem in den #USA durch den #PatriotAct verpflichteten Dienst in den Rachen werfen. Noch dazu ein Dienst, der auf den meisten Seiten #Tor blockiert. Fehler hin oder her ... Wer Datenschutz ernst nimmt, verzichtet auf #Cloudflare und ähnliche Dienste. Und wer doch einen Proxy vor seinem eigentlichen Dienst brauch, kann ihn ja selbst aufsetzen." by @[kromonos](https://metalhead.club/@kromonos/102372968538354130)

"Remember #Cloudflare? The people #Mozilla wanted to send all your DNS traffic to?" by @[Sturmflut](https://mastodon.technology/@Sturmflut/102373000030566755)

"That's when you understand single points of failures" by @[docbray](https://social.supernerdland.com/display/f7679b53-135d-1b7f-8446-841362829595)

"Cloudflare a rapidement confirmé l’existence d’une vaste panne." by @[leshoshin](https://framapiaf.org/@leshoshin/102372515752847465)

"How does an outfit that specializes in redundancy fail so bad in times like this? #Cloudflare Attributed to CPU spike. Huh? Like are they running on one machine in some dude's basement?" by @[cs](https://mastodon.sdf.org/@cs/102372444006745058)

"I don't use #Cloudflare." by @[veer66](https://mstdn.io/@veer66/102372440964130575)

"Cloudflare outage: this is the second time within the last few weeks, isn't it?" by @[fitheach](https://mstdn.io/@fitheach/102372404619232953)

"So apparently a bunch of sites are down because #cloudflare is down?? Cool. coolcool." by @[fayelyon](https://switter.at/@fayelyon/102372397761279375)

"#clowncomputing means you're not in control of your 'own' stuff. A guest in your own house." by @[schestowitz](https://pleroma.site/objects/19fa0942-f26c-4cd0-a6dd-79f8368bd014)

"When your friends/geek mates/colleagues shout that "OMG!! #Cloudflare is down!" and you don't give a damn because you told them not to touch this" by @[schestowitz](https://pleroma.site/objects/8618055f-d96f-437b-a24e-01a4ed1140b3)

"Yay, #cloudflare is down!" by @[DylanTheThomas](https://swingset.social/@DylanTheThomas/102372246983164931)

"#cloudflare is down ,... is that's why I cannot reach my #matrix homeserver anymore?" by @[musicmatze](https://mastodon.technology/@musicmatze/102372217168515090)

"Hahaha... Sehr geil  kaum kackt #krautflare ab, kann die Hälfte unserer #Frontend #Entwickler nicht mehr arbeiten, weil sie assets entweder vom #CDN bei #Cloudflare laden, oder von einer #Seite, die sich hinter deren proxy versteckt..." by @[kromonos](https://metalhead.club/@kromonos/102372240879706715)

"cloudflare outage again and the irony is D E L I C I O U S " by @[gingervitis](https://meme.garden/@gingervitis/102372210875463531)

"Allo Houston ? #Cloudflare est encore planté et a cassé une bonne partie du Web !" by @[DarvenDissek](https://mammouth.inframed.net/@DarvenDissek/102372195499429073)

"When you can't rant about cloudflare being down because the downtime complaint site is using #cloudflare." by @[lerk](https://comm.network/@lerk/102372193410432044)

"Bei den ganze #Cloudflare Problemen gerade freue ich mich richtig, dass es noch das Fediverse gibt" by @[clerie](https://fem.social/@clerie/102328746169300742)

"kuketzblog lang lebe #Cloudflare... Nicht!" by @[kromonos](https://metalhead.club/@kromonos/102334756147882328)

"they are too powerful, too big already. we dont' need to use them for time." by @[jeffcliff](https://niu.moe/@jeffcliff/102310896326359164)

"#cloudflare is #proprietarysoftware. To matter matters even worse, it's centralised and attached to a political system which subjugates 100+ governments." by @[schestowitz](https://pleroma.site/objects/ec8ac84d-60c2-4001-900a-85938c18abda)

"#itwire has self-immolated with #cloudflare Self-harming stupidity. Whose decision?" by @[schestowitz](https://pleroma.site/objects/e3d865f7-f161-4eae-a9d6-0764395af5bd)

"oh, eine #Cloudflare erinnerung, dass ich die #DNS Server für kromonos.net auf deren einstellen soll ... What about .. NO!" by @[kromonos](https://metalhead.club/@kromonos/102297020699575415)

"Ich wollte mal wieder bei LiberaPay vorbeischauen, dann springt mir #cloudflare entgegen. PayPal, Stripe und nun Cloudflare. Ein einst vielversprechendes Projekt schafft sich selbst ab." by @[kuketzblog](https://social.tchncs.de/@kuketzblog/102228512812286306)

"It makes it really hard for us Tor-users to use liberapay actually. We do know about "Under Attack" mode already, and I would like you to reconsider and not use cloudflare :)" by @[selea](https://social.linux.pizza/@selea/102208159022447946)

"What the fuck lowtechmagazine, you pull your fancy schmancy "oh were all low-bandwith and solar powered", but then you're behind FUCKING Cloudflare, banning ips (not even a captcha). Where's the low tech solarpunk shit huh, liberal hippie garbage" by @[f0x](https://social.pixie.town/@f0x/102204802211287233)

"I'm pretty sure just throwing up your statically generated site on a random provider would be more energy efficient because it doesn't have to go through #Cloudflare's surveillance apparatus." by @[f0x](https://social.pixie.town/@f0x/102204938741162533)

"No deja de llamar la atencion, que la inmensa mayoria de las instancias del #fediverso usen #cloudflare, el PEOR enemigo de la #privacidad y el #anonimato. ¿Sabian que cloudflare considera a #Tor una importante fuente de ataques DDoS, cuando no existen estudios que lo demuestren?" by @[rtfm](https://ieji.de/@rtfm/102177593596150087)

"So that's the new level of #Cloudflare blocks on torproject? I thought things were supposedly being fixed. Get your shit together, Cloudflare" by @[shiro](https://chaos.social/@shiro/102169210470724880)

"someone is trolling by trying to sign me up to cloudflare with my contact mails, thats creative?" by @[SolSoCoG](https://ieji.de/@SolSoCoG/102149695419969529)

"The person who created an account with my email address is actually trying to get my site on #Cloudflare." by @[amolith](https://masto.nixnet.xyz/@amolith/102137632049061724)

"Some delusional person out there must be madly in love with Cloudflare to pull this type of..." by @[caltlgin](https://masto.nixnet.xyz/@caltlgin/102129682739643046)

"I opened my mailbox this morning to find someone had also tried to register a Cloudflare account with my email address." by @[sean](https://social.sdoconnell.net/@sean/102134290535440645)

"I bet somebody who knows you hate Cloudflare is trolling you." by @[ataraxia937](https://fosstodon.org/@ataraxia937/102129849213530607)

"It sabotaged users over serious concerns of non-privacy, censorship, unethical, deception and unfair experiences." by @[HarvettFox96](https://hostux.social/@HarvettFox96/102097087292348123)

"Firefox is going to ship with DNS over HTTPS (DoH) by default soon. It will still use Cloudflare by default." by @[amolith](https://masto.nixnet.xyz/@amolith/101939482525255808)

"Oh, #Cloudflare hat Konkurrenz bekommen?
Jede Seite, die mich dazu drängt, ein #Google #Captcha zu lösen, damit ich mir die ansehen kann, schließe ich einfach direkt wieder" by @[kromonos](https://metalhead.club/@kromonos/102180075921056116)

"How pathetic is life when becoming absolutely enraged after solving a #captcha 3 times but not being permitted to move forward." by @[hrthu](https://fosstodon.org/@hrthu/101987545210858236)

"When #Cloudflare and #Google prevent you from watching a government's parliamentary debates. Does that count as interfering with democratic processes?" by @[MacLemon](https://chaos.social/@MacLemon/101862184608187348)

"You definitely put yourself in big danger by using it. Cloudflare has an own CA that is able to issue certificates for ANY website." by @[sheogorath](https://microblog.shivering-isles.com/@sheogorath/102056520473398252)

"Cloudflare is among the worst organisations I've seen from a privacy and security standpoint, same as Google, Facebook, et al." by @[amolith](https://masto.nixnet.xyz/@amolith/101919558389894089)

"sometimes it feels like I'm the only one pushing against the #greatcloudwall" by @[jeffcliff](https://niu.moe/@jeffcliff/101117752770134814)

"I liked them until I saw #Cloudflare." by @[amolith](https://masto.nixnet.xyz/@amolith/101588202702599511)

"#Cloudflare #surveillance marketed as "fast"" by @[Dr. Roy](https://pleroma.site/objects/a24125f1-c3c3-4138-ad22-1fb9327b80c8)

"Nothing says "our web developers are very competent" like putting your RSS feed URL behind a Cloudflare "please turn on javascript and reload the page" notice." by @[paul](https://social.device5.co.uk/@paul/101697354752109299)

"I wonder if the implied subtext is that YOU ARE THE FOOL for using these services. :-) " by @[MateJ](https://social.matej-lach.me/@MatejLach/101851464970639410)

"I dream of blocking #Cloudflare and #ReCaptcha" by @[Chuculate](https://niu.moe/@Chuculate/101834414084670347)

"you can't solve a puzzle to prove that you're a human in the new version anymore. if google says you're a bot, then you're a bot." by @[leip4Ier](https://infosec.exchange/@leip4Ier/100983839919407633)

"am i the only one being blocked by #Cloudflare before reaching Falkvinge's  article relating #NSA , and #GNU / #Linux systems... i'm puzzled, what am i missing ? " by @[hellekin](https://quitter.es/notice/4918810)

</details>

------

<details>
<summary>_click me_

# Telegram
</summary>


Screenshot provided by [@NoGoolag](https://t.me/NoGoolag), [@privacytoday](https://t.me/privacytoday), etc

![](image/telegram/4b644ffa2bfe836565dec686fb81238f.jpg)
![](image/telegram/adf85b43581e1f68d4466c28e2c5c5fc.jpg)

![](image/telegram/8e3379b250ea970c8d59eba1d154b560.jpg)
![](image/telegram/12134212fc9821e514a94888f3e2c902.jpg)

![](image/telegram/0be13101e79fcb5e90c7e949c234040e.jpg)
![](image/telegram/f029fcf244af884f6628decb7b15a8a9.jpg)

![](image/telegram/fe185b082ea67a734859b4ece650c4a5.jpg)
![](image/telegram/df2dc65af0ac66dcc68b3bfa9338bde3.jpg)

![](image/telegram/f6144c62db17a84c3bbd4d4f3eda8067.jpg)
![](image/telegram/cade80ec63cf119d8052cd5a8def2b3a.jpg)

![](image/telegram/79faadc7398477a7e1623af99a6ec9ae.jpg)
![](image/telegram/29b88092ed1f0091796d76baf07a7fbd.jpg)

![](image/telegram/0ff5d6441c53720c194af7e61fbd89ac.jpg)
![](image/telegram/4cdf036c1e45f1e943dda3e26d4cffb9.jpg)

![](image/telegram/67a77bc9b1e1a741496716482f7f6322.jpg)
![](image/telegram/320b8067457ce8c47838c4a07fad670b.jpg)

![](image/telegram/003adf34b034f1eb38e83fcc41b045ab.jpg)
![](image/telegram/a1e84595157d8ddc3985536878f53877.jpg)

![](image/telegram/8f3ef93452628296440814c81b75bfeb.jpg)

![](image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)
![](image/telegram/668c1ba0df11d5d8ef81b24e767ea3f7.jpg)

</details>

---

!["Cloudflare is not an option."](image/cfisnotanoption.jpg)